package com.praya.dreamfish.listener.support;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.gamingmesh.jobs.api.JobsPaymentEvent;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.agarthalib.utility.MetadataUtil;

public class ListenerJobsPayment extends HandlerEvent implements Listener {

	public ListenerJobsPayment(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void jobsPaymentEvent(JobsPaymentEvent event) {
		if (!event.isCancelled()) {
			final OfflinePlayer offlinePlayer = event.getPlayer();
			
			if (offlinePlayer.isOnline()) {
				final Player player = offlinePlayer.getPlayer();
				final String metadataFailID = "DreamFish Catch Failed";
				
				if (MetadataUtil.isCooldown(player, metadataFailID)) {
					event.setCancelled(true);
				}
			}
		}
	}
}
