package com.praya.dreamfish.listener.support;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.agarthalib.utility.MetadataUtil;

public class ListenerMcMMOPlayerXpGain extends HandlerEvent implements Listener {

	public ListenerMcMMOPlayerXpGain(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void xpGainEvent(McMMOPlayerXpGainEvent event) {
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			
			final String metadataFailID = "DreamFish Catch Failed";
			
			if (MetadataUtil.isCooldown(player, metadataFailID)) {
				event.setCancelled(true);
			}
		}
	}
}
