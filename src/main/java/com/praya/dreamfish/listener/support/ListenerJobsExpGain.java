package com.praya.dreamfish.listener.support;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.gamingmesh.jobs.api.JobsExpGainEvent;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.agarthalib.utility.MetadataUtil;

public class ListenerJobsExpGain extends HandlerEvent implements Listener {

	public ListenerJobsExpGain(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void jobsExpGainEvent(JobsExpGainEvent event) {
		if (!event.isCancelled()) {
			final OfflinePlayer offlinePlayer = event.getPlayer();
			
			if (offlinePlayer.isOnline()) {
				final Player player = offlinePlayer.getPlayer();
				final String metadataFailID = "DreamFish Catch Failed";
				
				if (MetadataUtil.isCooldown(player, metadataFailID)) {
					event.setCancelled(true);
				}
			}
		}
	}
}
