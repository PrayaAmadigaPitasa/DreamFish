package com.praya.dreamfish.listener.custom;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.event.PlayerExtendLuresEvent;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.dreamfish.manager.player.PlayerFishingModeManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerFishingMode;
import com.praya.dreamfish.player.PlayerFishingModeAction;

import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.SenderUtil;

public class ListenerPlayerExtendLures extends HandlerEvent implements Listener {

	public ListenerPlayerExtendLures(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void eventPlayerLures(PlayerExtendLuresEvent event) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingModeManager playerFishingModeManager = playerManager.getPlayerFishingModeManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final PlayerFishingMode playerFishingMode = playerFishingModeManager.getPlayerFishingMode(player);
			
			if (playerFishingMode != null) {
				final PlayerFishingModeAction playerFishingModeAction = playerFishingMode.getPlayerFishingModeAction();
				final Entity fishEntity = playerFishingMode.getFishEntity();
				
				playerFishingModeAction.setActionCooldown();
				
				if (fishEntity.isDead()) {
					playerFishingMode.unregister();
					return;
				} else {
					if (playerFishingMode.getFishPower() > 0) {
						final double pullPower = playerFishingModeManager.getPlayerFishingPower(player);
						final double pullTension = mainConfig.getRodDefaultPullTension();
						final double valRandom = -0.5 * (Math.random() + 0.5);
						final double valPower = valRandom * -(pullPower);
						final double valTension = valRandom * pullTension;
						
						playerFishingMode.addFishPower(valPower);
						playerFishingMode.addTension(valTension);
						SenderUtil.playSound(player, SoundEnum.BLOCK_CLOTH_FALL);
						return;
					}
				}
			}
		}
	}
}
