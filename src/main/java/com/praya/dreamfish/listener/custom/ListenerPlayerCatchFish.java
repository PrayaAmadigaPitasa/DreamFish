package com.praya.dreamfish.listener.custom;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.inventory.ItemStack;

import com.praya.agarthalib.utility.CommandUtil;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.event.PlayerCatchFishEvent;
import com.praya.dreamfish.event.PlayerFishingExpChangeEvent.ExpChangeReason;
import com.praya.dreamfish.fish.FishDrop;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.game.EventManager;
import com.praya.dreamfish.manager.game.FishManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerFishingModeManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerFishing;
import com.praya.dreamfish.player.PlayerFishingMode;

import api.praya.agarthalib.builder.support.SupportJobsReborn;
import api.praya.agarthalib.builder.support.SupportMcMMO;
import api.praya.agarthalib.builder.support.SupportSkillAPI;
import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;

public class ListenerPlayerCatchFish extends HandlerEvent implements Listener {

	public ListenerPlayerCatchFish(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void eventPlayerCatchFish(PlayerCatchFishEvent event) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		final PlayerFishingModeManager playerFishingModeManager = playerManager.getPlayerFishingModeManager();
		final FishManager fishManager = gameManager.getFishManager();
		final EventManager eventManager = gameManager.getEventManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		final SupportMcMMO supportMcMMO = supportManagerAPI.getSupportMcMMO();
		final SupportJobsReborn supportJobsReborn = supportManagerAPI.getSupportJobsReborn();
		final SupportSkillAPI supportSkillAPI = supportManagerAPI.getSupportSkillAPI();
		final Player player = event.getPlayer();
		final PlayerFishing playerFishing = playerFishingManager.getPlayerFishing(player);
		final PlayerFishingMode playerFishingMode = playerFishingModeManager.getPlayerFishingMode(player);
		
		if (playerFishingMode != null) {
			playerFishingMode.unregister();
		}
		
		if (!event.isCancelled()) {
			final String fish = event.getFish();
			final FishProperties fishProperties = fishManager.getFishProperties(fish);
			final FishDrop fishDrop = fishProperties.getFishDrop();
			final double length = event.getLength();
			final double weight = event.getWeight();
			final double exp = event.getExp();
			final double multiplierVanilla = mainConfig.getSupportExpMultiplierVanilla();
			final int expGainVanilla = (int) (exp * multiplierVanilla);
			final boolean enableRealisticMode = mainConfig.isFishingEnableRealistic();
			final boolean enableSupportMcMMO = mainConfig.isSupportEnableMcMMO();
			final boolean enableSupportJobsReborn = mainConfig.isSupportEnableJobsReborn();
			final boolean enableSupportSkillAPI = mainConfig.isSupportEnableSkillAPI();
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			mapPlaceholder.put("player", player.getName());
			mapPlaceholder.put("fish", fish);
			mapPlaceholder.put("length", String.valueOf(MathUtil.roundNumber(length, 2)));
			mapPlaceholder.put("weight", String.valueOf(MathUtil.roundNumber(weight, 2)));
			
			playerFishing.addExp((float) exp, ExpChangeReason.FISHING);
			SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
			
			for (String message : fishDrop.getMessages()) {
				
				message = TextUtil.placeholder(mapPlaceholder, message);
				message = TextUtil.placeholder(mapPlaceholder, message, "<", ">");
				
				SenderUtil.sendMessage(player, message);
			}
			
			for (String commandOP : fishDrop.getCommandOP()) {
				
				commandOP = TextUtil.placeholder(mapPlaceholder, commandOP);
				commandOP = TextUtil.placeholder(mapPlaceholder, commandOP, "<", ">");
				
				CommandUtil.sudoCommand(player, commandOP, true);
			}
			
			for (String commandConsole : fishDrop.getCommandConsole()) {
				
				commandConsole = TextUtil.placeholder(mapPlaceholder, commandConsole);
				commandConsole = TextUtil.placeholder(mapPlaceholder, commandConsole, "<", ">");
				
				CommandUtil.consoleCommand(commandConsole, player);
			}
			
			if (expGainVanilla > 0) {
				player.giveExp(expGainVanilla);
			}
			
			if (enableSupportMcMMO) {
				if (supportMcMMO != null) {
					final String skill = "FISHING";
					final double multiplier = mainConfig.getSupportExpMultiplierMcMMO();
					final float expGain = (float) (exp*multiplier);
					
					supportMcMMO.addExp(player, skill, expGain);
				}
			}
			
			if (enableSupportJobsReborn) {
				if (supportJobsReborn != null) {
					final String jobs = "Fisherman";
					final double multiplier = mainConfig.getSupportExpMultiplierJobsReborn();
					final double expGain = exp*multiplier;
					
					supportJobsReborn.addExperience(player, jobs, expGain);
				}
			}
			
			if (enableSupportSkillAPI) {
				if (supportSkillAPI != null) {
					final double multiplier = mainConfig.getSupportExpMultiplierSkillAPI();
					final double expGain = exp*multiplier;
					
					supportSkillAPI.addPlayerExp(player, expGain);
				}
			}
			
			if (enableRealisticMode) {
				final Entity hookEntity = playerFishingMode.getHook();
				final World world = hookEntity.getWorld();
				final Location locationPlayer = player.getLocation();
				final ItemStack itemFish = fishProperties.getItem();
				final MaterialEnum materialEnum = getMaterialEnum(itemFish);
				final ItemStack drop = materialEnum.toItemStack();
				final Item fishEntity = world.dropItem(locationPlayer, drop);
				
				eventManager.callFakePlayerFishEvent(player, fishEntity, hookEntity, State.CAUGHT_FISH);
				
				if (supportMcMMO != null) {
					final boolean isTreasure = supportMcMMO.handleFishingItem(player, fishEntity);
					
					if (isTreasure) {
						final ItemStack itemTreasure = fishEntity.getItemStack();
						final String itemTreasureName = EquipmentUtil.getDisplayName(itemTreasure);
						final boolean isReplace = mainConfig.isTreasureEnableReplaceFish();
						final MessageBuild message = Language.FISHING_CATCH_TREASURE.getMessage(player);
						
						mapPlaceholder.put("treasure", itemTreasureName);
						
						message.sendMessage(player, mapPlaceholder);
						PlayerUtil.addItem(player, itemTreasure);
						
						if (isReplace) {
							return;
						}
					}
				}
				
				final MessageBuild message = Language.FISHING_CATCH_FISH.getMessage(player);
				
				message.sendMessage(player, mapPlaceholder);
				PlayerUtil.addItem(player, itemFish);
			}
		}
	}
	
	private final MaterialEnum getMaterialEnum(ItemStack item) {
		if (item != null) {
			final MaterialEnum materialEnum = MaterialEnum.getMaterialEnum(item);
			
			if (materialEnum != null) {
				switch(materialEnum) {
				case COD : return materialEnum;
				case SALMON : return materialEnum;
				case TROPICAL_FISH : return materialEnum;
				case PUFFERFISH : return materialEnum;
				default : return MaterialEnum.COD;
				}
			}
		}
		
		return null;
	}
}