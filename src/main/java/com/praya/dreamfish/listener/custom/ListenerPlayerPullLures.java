package com.praya.dreamfish.listener.custom;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.util.Vector;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.event.PlayerCatchFishEvent;
import com.praya.dreamfish.event.PlayerPullLuresEvent;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.game.FishManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerFishingModeManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerFishingMode;
import com.praya.dreamfish.player.PlayerFishingModeAction;
import com.praya.dreamfish.player.PlayerFishing;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.MetadataUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.ServerEventUtil;
import com.praya.agarthalib.utility.VectorUtil;

public class ListenerPlayerPullLures extends HandlerEvent implements Listener {

	private static final String METADATA_PULL = "DreamFish Pull";
	
	public ListenerPlayerPullLures(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void eventPlayerLures(PlayerPullLuresEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final FishManager fishManager = gameManager.getFishManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		final PlayerFishingModeManager playerFishingModeManager = playerManager.getPlayerFishingModeManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final PlayerFishingMode playerFishingMode = playerFishingModeManager.getPlayerFishingMode(player);
			
			if (playerFishingMode != null) {
				final PlayerFishingModeAction playerFishingModeAction = playerFishingMode.getPlayerFishingModeAction();
				final String fish = event.getFish();
				final Entity fishEntity = playerFishingMode.getFishEntity();
				final double fishPower = playerFishingMode.getFishPower();
				
				playerFishingModeAction.setActionCooldown();
				
				if (fishEntity.isDead()) {
					playerFishingMode.unregister();
					return;
				} else {
					if (fishPower > 0) {
						final FishProperties fishProperties = fishManager.getFishProperties(fish);
						final PlayerFishing playerFishing = playerFishingManager.getPlayerFishing(player);
						final EntityType entityType = fishEntity.getType();
						final Location locationPlayer = player.getLocation();
						final Location locationFish = fishEntity.getLocation();
						final Location locationFishAbove = locationFish.clone().add(0, 1, 0);
						final Block blockFish = locationFish.getBlock();
						final Block blockFishAbove = locationFishAbove.getBlock();
						final Material materialFish = blockFish.getType();
						final Material materialFishAbove = blockFishAbove.getType();
						final MaterialEnum materialEnumFish = MaterialEnum.getMaterialEnum(materialFish);
						final MaterialEnum materialEnumFishAbove = MaterialEnum.getMaterialEnum(materialFishAbove);
						final Vector vectorRelative = VectorUtil.getRelative(locationFish, locationPlayer);
						final double fishResistance = fishProperties.getResistance();
						final double entitySpeedMutiplier = getEntitySpeedMultiplier(entityType);
						final double effectivenessMultiplier = (100 + playerFishing.getBonusEffectiveness()) / 100;
						final double enduranceMultiplier = (100 + playerFishing.getBonusEndurance()) / 100;
						final double speedMultiplier = (100 + playerFishing.getBonusSpeed()) / 100;
						final double pullSpeedDefault = mainConfig.getRodDefaultPullSpeed();
						final double pullSpeedMax = mainConfig.getRodDefaultPullMaxSpeed() * entitySpeedMutiplier;
						final double pullSpeedRod = playerFishingModeManager.getPlayerFishingSpeedMultiplier(player) * speedMultiplier;
						final double pullPower = playerFishingModeManager.getPlayerFishingPower(player) * effectivenessMultiplier;
						final double pullEndurance = playerFishingModeManager.getPlayerLuresEndurance(player) * enduranceMultiplier;
						final double pullTension = mainConfig.getRodDefaultPullTension();
						final double valRandom = Math.random() + 0.5;
						final double valPower = (valRandom * -(pullPower)) * ((100 - fishResistance) / 100);
						final double valTension = valRandom * pullTension / (pullEndurance / 100);
						final double valSpeed = Math.min(pullSpeedMax, (pullSpeedDefault * entitySpeedMutiplier * (pullSpeedRod / 100)));
						
						if (!materialEnumFishAbove.isFluid()) {
							final double heightSurface = locationFish.getY() % 1;
							final double speedY = heightSurface > 0.5 ? -(heightSurface - 0.5) / 2.5 : 0;
							
							vectorRelative.setY(speedY/valSpeed);
						} else if (!materialEnumFish.isFluid()) {
							vectorRelative.setY(-0.2/valSpeed);
						}
						
						vectorRelative.multiply(valSpeed);
						fishEntity.setVelocity(vectorRelative);
						playerFishingMode.addFishPower(valPower);
						playerFishingMode.addTension(valTension);
						MetadataUtil.setCooldown(player, METADATA_PULL, 100);
						SenderUtil.playSound(player, SoundEnum.BLOCK_WATER_AMBIENT);
						
						if (playerFishingMode.getFishPower() <= 0) {
							final MessageBuild message = Language.FISHING_FISH_WEAKEN.getMessage(player);
							
							message.sendMessage(player);
							SenderUtil.playSound(player, SoundEnum.ENTITY_SQUID_HURT);
						}
					} else {
						final Location locationPlayer = player.getLocation();
						final Location locationFish = fishEntity.getLocation();
						final Vector vectorRelative = VectorUtil.getRelative(locationFish, locationPlayer);
						final Location locationTo = locationFish.clone().add(vectorRelative);
						final double distance = locationTo.distance(locationPlayer);
						
						if (distance > 2.5) {
							final EntityType entityType = fishEntity.getType();
							final Location locationPlayerHorizontal = locationPlayer.clone();
							final Location locationFishHorizontal = locationFish.clone();
							final double entitySpeedMutiplier = getEntitySpeedMultiplier(entityType);
							final double pullSpeedDefault = mainConfig.getRodDefaultPullSpeed();
							final double pullSpeedMax = mainConfig.getRodDefaultPullMaxSpeed() * entitySpeedMutiplier;
							final double pullSpeedRod = playerFishingModeManager.getPlayerFishingSpeedMultiplier(player);
							final double valSpeed = Math.min(pullSpeedMax, (pullSpeedDefault * entitySpeedMutiplier * (pullSpeedRod / 100)));
							
							locationPlayerHorizontal.setY(0);
							locationFishHorizontal.setY(0);
							
							final double distanceHorizontal = locationPlayerHorizontal.distance(locationFishHorizontal);
							
							if (distanceHorizontal > 2.5) {
								final Location locationFishAbove = locationFish.clone().add(0, 1, 0);
								final Block blockFish = locationFish.getBlock();
								final Block blockFishAbove = locationFishAbove.getBlock();
								final Material materialFish = blockFish.getType();
								final Material materialFishAbove = blockFishAbove.getType();
								final MaterialEnum materialEnumFish = MaterialEnum.getMaterialEnum(materialFish);
								final MaterialEnum materialEnumFishAbove = MaterialEnum.getMaterialEnum(materialFishAbove);
								
								if (!materialEnumFish.isFluid() || !materialEnumFishAbove.isFluid()) {
									vectorRelative.setY(0);
								}
							} else {
								vectorRelative.setY(0.2);
							}
							
							vectorRelative.multiply(valSpeed);
							fishEntity.setVelocity(vectorRelative);								
							SenderUtil.playSound(player, SoundEnum.BLOCK_CLOTH_BREAK);
						} else {
							final PlayerCatchFishEvent playerCatchFishEvent = new PlayerCatchFishEvent(player, fish);
							
							ServerEventUtil.callEvent(playerCatchFishEvent);
						}
					}
				}
			}
		}
	}
	
	private final double getEntitySpeedMultiplier(EntityType type) {
		if (type != null) {
			switch (type.toString()) {
			case "COD" : return 0.5;
			case "SALMON" : return 0.5;
			case "TROPICAL_FISH" : return 0.5;
			case "PUFFERFISH" : return 0.5;
			case "GUARDIAN" : return 0.5;
			case "ELDER_GUARDIAN" : return 0.5;
			case "SILVERFISH" : return 0.5;
			case "ENDERMITE" : return 0.7;
			case "PHANTOM" : return 0.8;
			default : return 1;
			}
		}
		
		return 1;
	}
}
