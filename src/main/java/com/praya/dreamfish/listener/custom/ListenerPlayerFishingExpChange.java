package com.praya.dreamfish.listener.custom;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.event.PlayerFishingExpChangeEvent;
import com.praya.dreamfish.event.PlayerFishingExpChangeEvent.ExpChangeReason;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerFishing;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

public class ListenerPlayerFishingExpChange extends HandlerEvent implements Listener {
	
	public ListenerPlayerFishingExpChange(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void playerHerbalismExpChangeEvent(PlayerFishingExpChangeEvent event) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		final ExpChangeReason reason = event.getReason();
		
		if (reason.equals(ExpChangeReason.FISHING)) {
			final Player player = event.getOnlinePlayer();
			
			if (player != null) {
				final PlayerFishing playerFishing = playerFishingManager.getPlayerFishing(player);
				final float expPlayer = playerFishing.getExp();
				final float exp = event.getExp();
				
				if (exp > expPlayer) {
					final float expGain = exp - expPlayer;
					final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						
					String message = Language.ACTIONBAR_PLAYER_EXP_GAIN.getText(player);
					
					mapPlaceholder.put("exp_gain", String.valueOf(MathUtil.roundNumber(expGain)));
					
					message = TextUtil.placeholder(mapPlaceholder, message);
					
					Bridge.getBridgeMessage().sendActionbar(player, message);
					SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
				}
			}
		}
	}
}