package com.praya.dreamfish.listener.custom;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.event.PlayerFishingLevelChangeEvent;
import com.praya.dreamfish.event.PlayerFishingLevelChangeEvent.LevelChangeReason;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerFishing;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

public class ListenerPlayerFishingLevelChange extends HandlerEvent implements Listener {
	
	public ListenerPlayerFishingLevelChange(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void playerHerbalismLevelChangeEvent(PlayerFishingLevelChangeEvent event) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		final LevelChangeReason reason = event.getReason();
		
		if (reason.equals(LevelChangeReason.EXP_UP)) {
			final Player player = event.getOnlinePlayer();
			
			if (player != null) {
				final PlayerFishing playerFishing = playerFishingManager.getPlayerFishing(player);
				final int levelPlayer = playerFishing.getLevel();
				final int level = event.getLevel();
				
				if (level > levelPlayer) {
					final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
					
					String messageTitle = Language.TITLE_PLAYER_LEVEL_UP.getText(player);
					String messageSubtitle = Language.SUBTITLE_PLAYER_LEVEL_UP.getText(player);
					
					mapPlaceholder.put("fishing_level", String.valueOf(level));
					
					messageTitle = TextUtil.placeholder(mapPlaceholder, messageTitle);
					messageSubtitle = TextUtil.placeholder(mapPlaceholder, messageSubtitle);
					messageTitle = TextUtil.colorful(messageTitle);
					messageSubtitle = TextUtil.colorful(messageSubtitle);
					
					Bridge.getBridgeMessage().sendTitle(player, messageTitle, 5, 40, 5);
					Bridge.getBridgeMessage().sendSubtitle(player, messageSubtitle, 5, 40, 5);
					SenderUtil.playSound(player, SoundEnum.ENTITY_PLAYER_LEVELUP);
				}
			}
		}
	}
}