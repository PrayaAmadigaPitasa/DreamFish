package com.praya.dreamfish.listener.custom;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerEvent;

import api.praya.agarthalib.builder.event.MenuOpenEvent;
import core.praya.agarthalib.builder.menu.Menu;
import core.praya.agarthalib.builder.menu.MenuGUI;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.SenderUtil;

public class ListenerMenuOpen extends HandlerEvent implements Listener {

	public ListenerMenuOpen(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
    public void menuOpenEvent(MenuOpenEvent event) {
		final Menu menu = event.getMenu();
		final Player player = event.getPlayer();
		final String id = menu.getID();
		
		if (menu instanceof MenuGUI) {
			if (id.startsWith("DreamFish")) {
				SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
			}
		}
	}
}
