package com.praya.dreamfish.listener.main;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.event.PlayerCatchFishEvent;
import com.praya.dreamfish.event.PlayerHookFishEvent;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.game.BaitManager;
import com.praya.dreamfish.manager.game.EventManager;
import com.praya.dreamfish.manager.game.FishManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.manager.player.PlayerBaitManager;
import com.praya.dreamfish.manager.player.PlayerFishingModeManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerBait;
import com.praya.dreamfish.player.PlayerFishingMode;
import com.praya.dreamfish.player.PlayerFishingModeAction;

import api.praya.agarthalib.builder.support.SupportMcMMO;
import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;
import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.VersionNMS;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.MetadataUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.ServerEventUtil;
import com.praya.agarthalib.utility.ServerUtil;

public class ListenerPlayerFish extends HandlerEvent implements Listener {
	
	private static Field hookEntityField;
	
	public ListenerPlayerFish(DreamFish plugin) {
		super(plugin);
	}
	
	static {
        try {
            hookEntityField = PlayerFishEvent.class.getDeclaredField("hookEntity");
            hookEntityField.setAccessible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void eventPlayerFish(PlayerFishEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingModeManager playerFishingModeManager = playerManager.getPlayerFishingModeManager();
		final PlayerBaitManager playerBaitManager = playerManager.getPlayerBaitManager();
		final FishManager fishManager = gameManager.getFishManager();
		final EventManager eventManager = gameManager.getEventManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		
		if (!event.isCancelled()) {
	 		final Player player = event.getPlayer();
			final State state = event.getState();
			final Entity hook = getHook(event);
			final boolean isEnableRealisticMode = mainConfig.isFishingEnableRealistic();
			final boolean isEnableAutoCatch = mainConfig.isFishingEnableAutoCatch();
			final boolean isEnableBaitCheckOnCatch = mainConfig.isBaitEnableCheckOnCatch();
			final boolean isVersion_1_9 = ServerUtil.isCompatible(VersionNMS.V1_9_R1);
			
			if (state.equals(State.FISHING)) {
				final PlayerBait playerBait = playerBaitManager.getPlayerBait(player);
				
				if (isVersion_1_9) {
					final PlayerFishingMode playerFishingMode = playerFishingModeManager.getPlayerFishingMode(player);
					
					if (playerFishingMode != null) {
						final PlayerFishingModeAction playerFishingModeAction = playerFishingMode.getPlayerFishingModeAction();
						
						if (playerFishingModeAction.isActionCooldown()) {
							event.setCancelled(true);
							hook.remove();
							return;
						}
					}
				}
					
				if (!playerBait.hasMarkBait()) {
					final boolean isEnableAutoHook = mainConfig.isBaitEnableAutoHook();
					
					if (isEnableAutoHook) {
						playerBaitManager.autoHookBait(player);
					}
				}
				
				if (!isEnableBaitCheckOnCatch && !checkMarkBait(event, player)) {
					event.setCancelled(true);
					hook.remove();
					return;
				} else {
					if (playerBait.hasMarkBait()) {
						final String markBait = playerBait.getMarkBait();
						final String hookBait = playerBait.getHookBait();
						
						if (hookBait == null || !hookBait.equalsIgnoreCase(markBait)) {
							if (!playerBaitManager.useBait(player, markBait) && !playerBaitManager.autoHookBait(player)) {
								final MessageBuild message = Language.FISHING_BAIT_RUN_OUT.getMessage(player);
								
								playerBait.removeMarkBait();
								message.sendMessage(player);
								SenderUtil.playSound(player, SoundEnum.BLOCK_CLOTH_FALL);
								return;
							}
						}
					}
				}
			} else {
				final PlayerFishingMode playerFishingMode = playerFishingModeManager.getPlayerFishingMode(player);
				
				if (playerFishingMode != null) {
					final PlayerFishingModeAction playerFishingModeAction = playerFishingMode.getPlayerFishingModeAction();
					
					if (playerFishingModeAction.hasAntiBug() && !isVersion_1_9) {
						handleAntiBug(event);
						playerFishingModeAction.setAntiBug(false);
						return;
					}
				}
				
				if (state.equals(State.CAUGHT_FISH)) {
					if (eventManager.isFakePlayerFishEvent(event)) {
						handleFakeFish(event, player);
						return;
					} else {
						final String fish = handleFishing(event, state, player, hook);
						final FishProperties fishProperties = fishManager.getFishProperties(fish);
						
						if (fishProperties == null) {
							if (!isVersion_1_9) {
								handleAntiBug(event);
								return;
							}
						} else {
							if (isEnableRealisticMode) {
								event.setCancelled(true);
								PlayerFishingMode.startFishingMode(player, hook, fishProperties);
								return;
							} else {
								handleCatchFish(event, state, player, hook, fish);
								return;
							}
						}
					}
				} else {
					if (isVersion_1_9) {
						if (state.equals(State.BITE)) {
							if (isEnableRealisticMode) {
								if (isEnableAutoCatch) {
									if (playerFishingModeManager.isFishing(player)) {
										final Location loc = hook.getLocation();
										final double effectRange = mainConfig.getEffectRange();
										final Collection<Player> players = PlayerUtil.getNearbyPlayers(hook.getLocation(), effectRange);
										
										event.setCancelled(true);
										Bridge.getBridgeParticle().playParticle(players, ParticleEnum.WATER_BUBBLE, loc, 40, 1.5, 2, 1.5, 0.5F);
										Bridge.getBridgeParticle().playParticle(players, ParticleEnum.WATER_WAKE, loc, 60, 1.5, 2, 1.5, 0.5F);
										Bridge.getBridgeParticle().playParticle(players, ParticleEnum.WATER_DROP, loc, 40, 1.5, 2, 1.5, 0.5F);
										SenderUtil.playSound(player, SoundEnum.ENTITY_SQUID_AMBIENT);
									} else {
										final String fish = handleFishing(event, state, player, hook);
										final FishProperties fishProperties = fishManager.getFishProperties(fish);
										
										if (fishProperties != null) {
											event.setCancelled(true);
											PlayerFishingMode.startFishingMode(player, hook, fishProperties);
											return;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	private final String handleFishing(PlayerFishEvent event, State state, Player player, Entity hook) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerBaitManager playerBaitManager = playerManager.getPlayerBaitManager();
		final PlayerFishingModeManager playerFishingModeManager = playerManager.getPlayerFishingModeManager();
		
		if (event != null && state != null && player != null && hook != null) {
			if (checkMarkBait(event, player)) {
				final PlayerBait playerBait = playerBaitManager.getPlayerBait(player);
				final String bait = playerBait.getHookBait();
				
				if (checkFishing(event, state, player, hook, null, CheckType.BAIT, bait)) {
					final String fish = playerFishingModeManager.getRandomFish(player, hook, bait); 
					
					if (checkFishing(event, state, player, hook, fish, CheckType.FISH, fish)) {
						final PlayerHookFishEvent eventPlayerHookFish = new PlayerHookFishEvent(player, fish, bait, hook);
						
						ServerEventUtil.callEvent(eventPlayerHookFish);
						
						if (checkFishing(event, state, player, hook, fish, CheckType.HOOK, eventPlayerHookFish)) {
							final double chance = eventPlayerHookFish.getChance();
							
							if (checkFishing(event, state, player, hook, fish, CheckType.CHANCE, chance)) {
								return fish;
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
	private final void handleCatchFish(PlayerFishEvent event, State state, Player player, Entity hook, String fish) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerBaitManager playerBaitManager = playerManager.getPlayerBaitManager();
		final FishManager fishManager = gameManager.getFishManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final SupportMcMMO supportMcMMO = supportManagerAPI.getSupportMcMMO();
		final PlayerBait playerBait = playerBaitManager.getPlayerBait(player);
		
		playerBait.removeHookBait();
		
		if (event != null && state != null && player != null && hook != null && fish != null) {
			final PlayerCatchFishEvent playerCatchFishEvent = new PlayerCatchFishEvent(player, fish);
			
			ServerEventUtil.callEvent(playerCatchFishEvent);
			
			if (checkFishing(event, state, player, hook, fish, CheckType.CATCH, playerCatchFishEvent)) {
				final Entity entity = event.getCaught();
				
				new BukkitRunnable() {
					
					@Override
					public void run() {
						if (!entity.isDead()) {
							final FishProperties fishProperties = fishManager.getFishProperties(fish);
							final Location location = entity.getLocation();
							final double y = location.getY();
							final double additionalY = 1 - (y  % 1);
							
							location.add(0, additionalY, 0);
							
							if (fishProperties != null) {
								final World world = entity.getWorld();
								final Vector vector = entity.getVelocity();
								final ItemStack itemFish = fishProperties.getItem().clone();
								final Item drop = world.dropItem(location, itemFish);
								final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
								final boolean isTreasure = supportMcMMO != null ? supportMcMMO.handleFishingItem(player, drop) : false;
								
								drop.setVelocity(vector);
								entity.remove();
								
								if (isTreasure) {
									final ItemStack itemTreasure = drop.getItemStack();
									final String itemTreasureName = EquipmentUtil.getDisplayName(itemTreasure);
									final boolean isReplace = mainConfig.isTreasureEnableReplaceFish();
									final MessageBuild message = Language.FISHING_CATCH_TREASURE.getMessage(player);
									
									mapPlaceholder.put("treasure", itemTreasureName);
									
									message.sendMessage(player, mapPlaceholder);
									
									if (isReplace) {
										return;
									} else {
										final Item dropFish = world.dropItem(location, itemFish);
										
										dropFish.setVelocity(vector);
									}
								}
								
								final double length = playerCatchFishEvent.getLength();
								final double weight = playerCatchFishEvent.getWeight();
								final MessageBuild message = Language.FISHING_CATCH_FISH.getMessage(player);
								
								mapPlaceholder.put("fish", fish);
								mapPlaceholder.put("length", String.valueOf(MathUtil.roundNumber(length, 2)));
								mapPlaceholder.put("weight", String.valueOf(MathUtil.roundNumber(weight, 2)));
								
								message.sendMessage(player, mapPlaceholder);
							}
						}
					}
				}.runTaskLater(plugin, 0);
			}
		}
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void removeFakeEvent(PlayerFishEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final EventManager eventManager = gameManager.getEventManager();
		
		if (eventManager.isFakePlayerFishEvent(event)) {
			eventManager.removeFakePlayerFishEvent(event);
		}
	}
	
	private final void handleFakeFish(PlayerFishEvent event, Player player) {
		final Entity entity = event.getCaught();
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if (!entity.isDead()) {
					entity.remove();
				}
			}
		}.runTaskLater(plugin, 0);
	}
	
	private final void handleAntiBug(PlayerFishEvent event) {
		if (event != null) {
			final Entity caught = event.getCaught();
			
			event.setCancelled(false);
			
			if (caught != null) {
				caught.remove();
			}
		}
	}
	
	private final boolean checkMarkBait(PlayerFishEvent event, Player player) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerBaitManager playerBaitManager = playerManager.getPlayerBaitManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		
		if (event != null && player != null) {
			final PlayerBait playerBait = playerBaitManager.getPlayerBait(player);
			final String markBait = playerBait.getMarkBait();
			final BaitProperties markBaitProperties = baitManager.getBaitProperties(markBait);
			final Entity entity = getHook(event);
			
			if (markBaitProperties == null) {
				final MessageBuild message = Language.FISHING_BAIT_EMPTY.getMessage(player);
				
				event.setCancelled(true);
				entity.remove();
				message.sendMessage(player);
				SenderUtil.playSound(player, SoundEnum.BLOCK_CLOTH_FALL);
				return false;
			} else {
				final String hookBait = playerBait.getHookBait();
				
				if (hookBait != null && hookBait.equalsIgnoreCase(markBait)) {
					return true;
				} else {
					final ItemStack itemBait = markBaitProperties.getItem();
					
					if (!PlayerUtil.hasItem(player, itemBait)) {
						final MessageBuild message = Language.FISHING_BAIT_RUN_OUT.getMessage(player);
						
						event.setCancelled(true);
						entity.remove();
						playerBait.removeMarkBait();
						message.sendMessage(player);
						SenderUtil.playSound(player, SoundEnum.BLOCK_CLOTH_FALL);
						return false;
					} else {
						if (hookBait != null) {
							final BaitProperties hookBaitProperties = baitManager.getBaitProperties(hookBait);
							
							if (hookBaitProperties != null) {
								final ItemStack itemRetrieval = hookBaitProperties.getItem().clone();
								
								PlayerUtil.addItem(player, itemRetrieval);
							}
						}
						
						playerBait.setHookBait(markBait);
						PlayerUtil.removeItem(player, itemBait);
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	private final boolean checkFishing(PlayerFishEvent event, State state, Player player, Entity hook, String fish, CheckType type, Object object) {
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingModeManager playerFishingModeManager = playerManager.getPlayerFishingModeManager();
		final PlayerBaitManager playerBaitManager = playerManager.getPlayerBaitManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		
		if (event != null && state != null && player != null && hook != null && type != null) {
			if (object != null) {
				if (type.equals(CheckType.FISH)) {
					return true;
				} else if (type.equals(CheckType.BAIT)) {
					final String bait = (String) object;
					
					if (bait != null && baitManager.isBaitExists(bait)) {
						return true;
					}
				} else if (type.equals(CheckType.CHANCE)) {
					final double chance = (double) object;
					final double multiplier = playerFishingModeManager.getPlayerFishingMultiplier(player);
					
					if (MathUtil.chanceOf(chance*multiplier)) {
						return true;
					} else {
						final PlayerBait playerBait = playerBaitManager.getPlayerBait(player);
						
						playerBait.removeHookBait();
					}
				} else if (type.equals(CheckType.HOOK)) {
					final PlayerHookFishEvent playerHookFishEvent = (PlayerHookFishEvent) object;
					
					if (!playerHookFishEvent.isCancelled()) {
						return true;
					}
				} else if (type.equals(CheckType.CATCH)) {
					final PlayerCatchFishEvent playerCatchFishEvent = (PlayerCatchFishEvent) object;
					
					if (!playerCatchFishEvent.isCancelled()) {
						return true;
					}
				}
			}
			
			if (state.equals(State.CAUGHT_FISH)) {
				if (ServerUtil.isCompatible(VersionNMS.V1_9_R1)) {
					hook.remove();
					event.setCancelled(true);
				} else {
					final Entity entity = event.getCaught();
					
					if (entity instanceof Item) {
						final Item itemDrop = (Item) entity;
						
						itemDrop.remove();
					}
				}
			} else {
				hook.remove();
			}
			
			final String metadataFailID = "DreamFish Catch Failed";
			final Language language = fish != null ? Language.FISHING_CATCH_FAILED_FISH : Language.FISHING_CATCH_FAILED_DEFAULT;
			final MessageBuild message = language.getMessage(player);
			
			event.setExpToDrop(0);
			MetadataUtil.setCooldown(player, metadataFailID, 50);
			
			message.sendMessage(player, "fish", fish != null ? fish : "fish");
			SenderUtil.playSound(player, SoundEnum.BLOCK_CLOTH_FALL);
			return false;
		}
		
		return false;
	}
	
	private final Entity getHook(PlayerFishEvent event) {
		if (event != null) {
			try {
				final Object object = hookEntityField.get(event);
				
				if (object != null) {
					final Entity hook = (Entity) object;
					
					return hook;
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	private enum CheckType {
		BAIT,
		FISH,
		CHANCE,
		HOOK,
		CATCH;
	}
}