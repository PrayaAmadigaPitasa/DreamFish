package com.praya.dreamfish.listener.main;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerFishing;

public class ListenerPlayerQuit extends HandlerEvent implements Listener {

	public ListenerPlayerQuit(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void playerQuitEvent(PlayerQuitEvent event) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		final Player player = event.getPlayer();
		final PlayerFishing playerFishing = playerFishingManager.getPlayerFishing(player, false);
		
		if (playerFishing != null) {
			playerFishing.save();
			playerFishingManager.removeFromCache(player);
		}		
	}
}