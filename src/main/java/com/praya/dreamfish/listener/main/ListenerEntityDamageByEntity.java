package com.praya.dreamfish.listener.main;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.agarthalib.utility.MetadataUtil;

public class ListenerEntityDamageByEntity extends HandlerEvent implements Listener {

	public ListenerEntityDamageByEntity(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void entityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		if (!event.isCancelled()) {
			final String metadataFishID = "DreamFish Fish";
			final Entity victims = event.getEntity();
			final Entity attacker = event.getDamager();
			final boolean isFishVictims = victims instanceof Player ? false : MetadataUtil.hasMetadata(victims, metadataFishID);
			final boolean isFishAttacker = attacker instanceof Player ? false : MetadataUtil.hasMetadata(attacker, metadataFishID);
			
			if (isFishVictims || isFishAttacker) {
				event.setCancelled(true);
			}
		}
	}
}
