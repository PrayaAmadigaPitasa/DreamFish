package com.praya.dreamfish.listener.main;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.agarthalib.utility.MetadataUtil;

public class ListenerEntityCombust extends HandlerEvent implements Listener {

	public ListenerEntityCombust(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void entityCombustEvent(EntityCombustEvent event) {
		if (!event.isCancelled()) {
			final String metadataFishID = "DreamFish Fish";
			final Entity victims = event.getEntity();
			final boolean isFishVictims = victims instanceof Player ? false : MetadataUtil.hasMetadata(victims, metadataFishID);
			
			if (isFishVictims) {
				event.setCancelled(true);
			}
		}
	}
}
