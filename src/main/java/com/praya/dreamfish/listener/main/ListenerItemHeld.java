package com.praya.dreamfish.listener.main;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.player.PlayerFishingModeManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerFishingMode;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.SenderUtil;

public class ListenerItemHeld extends HandlerEvent implements Listener {

	public ListenerItemHeld(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void playerItemHeldEvent(PlayerItemHeldEvent event) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingModeManager playerFishingModeManager = playerManager.getPlayerFishingModeManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final PlayerFishingMode playerFishingMode = playerFishingModeManager.getPlayerFishingMode(player);
			
			if (playerFishingMode != null) {
				final Inventory inventory = player.getInventory();
				final int slotPrevious = event.getPreviousSlot();
				final ItemStack itemPrevious = inventory.getItem(slotPrevious);
				final Material materialPrevious = EquipmentUtil.isSolid(itemPrevious) ? itemPrevious.getType() : null;
				
				if (materialPrevious != null && materialPrevious.equals(Material.FISHING_ROD)) {
					final int slotAfter = event.getNewSlot();
					final ItemStack itemAfter = inventory.getItem(slotAfter);
					final Material materialAfter = EquipmentUtil.isSolid(itemAfter) ? itemAfter.getType() : null;
					
					if (materialAfter == null || !materialAfter.equals(Material.FISHING_ROD)) {
						final String informationID = "DreamFish Fishing Mode";
						final int priorityActionbar = mainConfig.getPriorityActionbar();
						final MessageBuild message = Language.FISHING_LURE_BROKEN.getMessage(player);
						
						playerFishingMode.unregister();
						message.sendMessage(player);
						SenderUtil.playSound(player, SoundEnum.BLOCK_CLOTH_BREAK);
						Bridge.getBridgeMessage().sendProtectedActionbar(player, "", informationID, priorityActionbar);
						return;
					}
				}
			}
		}
	}
}