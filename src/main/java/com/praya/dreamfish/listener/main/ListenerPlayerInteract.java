package com.praya.dreamfish.listener.main;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.event.PlayerExtendLuresEvent;
import com.praya.dreamfish.event.PlayerPullLuresEvent;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.handler.HandlerEvent;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.game.BaitManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.manager.player.PlayerBaitManager;
import com.praya.dreamfish.manager.player.PlayerFishingModeManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerBait;
import com.praya.dreamfish.player.PlayerFishingMode;
import com.praya.dreamfish.player.PlayerFishingModeAction;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.ServerEventUtil;

public class ListenerPlayerInteract extends HandlerEvent implements Listener {
	
	public ListenerPlayerInteract(DreamFish plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.LOW)
	public void fishingMode(PlayerInteractEvent event) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingModeManager playerFishingModeManager = playerManager.getPlayerFishingModeManager();
		final Player player = event.getPlayer();
		final PlayerFishingMode playerFishingMode = playerFishingModeManager.getPlayerFishingMode(player);
		
		if (playerFishingMode != null) {
			final FishProperties fishProperties = playerFishingMode.getFishProperties();
			final PlayerFishingModeAction playerFishingModeAction = playerFishingMode.getPlayerFishingModeAction();
			final Action action = event.getAction();
			final String fish = fishProperties.getId();
			
			event.setCancelled(true);
			
			if (!playerFishingModeAction.isActionCooldown()) {
				if (action.equals(Action.RIGHT_CLICK_BLOCK) || action.equals(Action.RIGHT_CLICK_AIR)) {
					final PlayerPullLuresEvent pullLuresEvent = new PlayerPullLuresEvent(player, fish);
					
					ServerEventUtil.callEvent(pullLuresEvent); 
					return;
				} else if (action.equals(Action.LEFT_CLICK_BLOCK) || action.equals(Action.LEFT_CLICK_AIR)) {
					final PlayerExtendLuresEvent extendLuresEvent = new PlayerExtendLuresEvent(player, fish);
					
					ServerEventUtil.callEvent(extendLuresEvent);
					return;
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.LOW)
	public void markBait(PlayerInteractEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerBaitManager playerBaitManager = playerManager.getPlayerBaitManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		final Player player = event.getPlayer();
		final Action action = event.getAction();
		
		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			
			if (EquipmentUtil.isSolid(item)) {
				final BaitProperties baitProperties = baitManager.getBaitProperties(item);
				
				if (baitProperties != null) {
					final String bait = baitProperties.getId();
					final PlayerBait playerBait = playerBaitManager.getPlayerBait(player);
					final String hook = playerBait.getHookBait();
					
					if (hook != null && hook.equalsIgnoreCase(bait)) {
						final MessageBuild message = Language.COMMAND_BAIT_USE_DUPLICATE.getMessage(player);
						
						message.sendMessage(player);
						SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else if (playerBaitManager.useBait(player, bait)) {
						final MessageBuild message = Language.COMMAND_BAIT_USE_SUCCESS.getMessage(player);
						
						message.sendMessage(player, "bait", bait);
						SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						return;
					} else {
						final MessageBuild message = Language.COMMAND_BAIT_USE_FAILED.getMessage(player);
						
						message.sendMessage(player);
						SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					}
				}
			}
		}
	}
}