package com.praya.dreamfish.tabcompleter.dreamfish;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.manager.game.FishManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.tabcompleter.TabCompleterArgument;

public class TabCompleterDreamFishLoad extends TabCompleterArgument {

	private static final Command COMMAND = Command.DREAMFISH_LOAD;
	
	private TabCompleterDreamFishLoad(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterDreamFishLoadSingleton {
		private static final TabCompleterDreamFishLoad instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterDreamFishLoad(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterDreamFishLoad getInstance() {
		return TabCompleterDreamFishLoadSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final GameManager gameManager = plugin.getGameManager();
		final FishManager fishManager = gameManager.getFishManager();
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			final Collection<String> fishIds = fishManager.getFishPropertiesIds();
			
			if (!fishIds.isEmpty()) {
				tabList.addAll(fishIds);
			}
		} else if (args.length == 3) {
			return null;
		} else if (args.length == 4) {
			tabList.add("[<amount>]");
		}
		
		return tabList;
	}
}
