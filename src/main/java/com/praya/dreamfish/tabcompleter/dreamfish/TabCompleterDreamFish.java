package com.praya.dreamfish.tabcompleter.dreamfish;

import com.praya.dreamfish.tabcompleter.TabCompleterArgument;
import com.praya.dreamfish.tabcompleter.TabCompleterTree;

public class TabCompleterDreamFish extends TabCompleterTree {

	private static final String COMMAND = "DreamFish";
	
	private TabCompleterDreamFish() {
		super(COMMAND);
		
		final TabCompleterArgument tabCompleterArgumentExp = TabCompleterDreamFishExp.getInstance();
		final TabCompleterArgument tabCompleterArgumentHelp = TabCompleterDreamFishHelp.getInstance();
		final TabCompleterArgument tabCompleterArgumentLevel = TabCompleterDreamFishLevel.getInstance();
		final TabCompleterArgument tabCompleterArgumentList = TabCompleterDreamFishList.getInstance();
		final TabCompleterArgument tabCompleterArgumentLoad = TabCompleterDreamFishLoad.getInstance();
		final TabCompleterArgument tabCompleterArgumentMenu = TabCompleterDreamFishMenu.getInstance();
		
		register(tabCompleterArgumentExp);
		register(tabCompleterArgumentHelp);
		register(tabCompleterArgumentLevel);
		register(tabCompleterArgumentList);
		register(tabCompleterArgumentLoad);
		register(tabCompleterArgumentMenu);
	}
	
	private static class CommandDreamFishSingleton {
		private static final TabCompleterDreamFish instance = new TabCompleterDreamFish();
	}
	
	public static final TabCompleterDreamFish getInstance() {
		return CommandDreamFishSingleton.instance;
	}
}