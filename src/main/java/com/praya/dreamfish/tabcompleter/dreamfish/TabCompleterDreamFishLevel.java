package com.praya.dreamfish.tabcompleter.dreamfish;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.tabcompleter.TabCompleterArgument;

public class TabCompleterDreamFishLevel extends TabCompleterArgument {

	private static final Command COMMAND = Command.DREAMFISH_LEVEL;
	
	private TabCompleterDreamFishLevel(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterDreamFishLevelSingleton {
		private static final TabCompleterDreamFishLevel instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterDreamFishLevel(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterDreamFishLevel getInstance() {
		return TabCompleterDreamFishLevelSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			tabList.add("Set");
			tabList.add("Add");
			tabList.add("Take");
		} else if (args.length == 3) {
			tabList.add("<value>");
		} else if (args.length == 4) {
		}
		
		return tabList;
	}
}
