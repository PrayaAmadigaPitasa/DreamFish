package com.praya.dreamfish.tabcompleter.dreamfish;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.tabcompleter.TabCompleterArgument;

public class TabCompleterDreamFishExp extends TabCompleterArgument {

	private static final Command COMMAND = Command.DREAMFISH_EXP;
	
	private TabCompleterDreamFishExp(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterDreamFishExpSingleton {
		private static final TabCompleterDreamFishExp instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterDreamFishExp(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterDreamFishExp getInstance() {
		return TabCompleterDreamFishExpSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			tabList.add("Set");
			tabList.add("Add");
			tabList.add("Take");
		} else if (args.length == 3) {
			tabList.add("<value>");
		} else if (args.length == 4) {
			return null;
		}
		
		return tabList;
	}
}
