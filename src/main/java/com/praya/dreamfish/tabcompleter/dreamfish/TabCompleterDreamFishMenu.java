package com.praya.dreamfish.tabcompleter.dreamfish;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.tabcompleter.TabCompleterArgument;

public class TabCompleterDreamFishMenu extends TabCompleterArgument {

	private static final Command COMMAND = Command.DREAMFISH_MENU;
	
	private TabCompleterDreamFishMenu(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterDreamFishMenuSingleton {
		private static final TabCompleterDreamFishMenu instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterDreamFishMenu(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterDreamFishMenu getInstance() {
		return TabCompleterDreamFishMenuSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			final String permissionMenuBuyBait = mainConfig.getMenuPermissionBuyBait();
			final String permissionMenuSellBait = mainConfig.getMenuPermissionSellBait();
			final String permissionMenuSellFish = mainConfig.getMenuPermissionSellFish();
			final String permissionMenuGuide = mainConfig.getMenuPermissionGuide();
			final boolean enableMenuPageBuyBait = mainConfig.isMenuEnablePageBuyBait();
			final boolean enableMenuPageSellBait = mainConfig.isMenuEnablePageSellBait();
			final boolean enableMenuPageSellFish = mainConfig.isMenuEnablePageSellFish();
			
			tabList.add("Home");
			tabList.add("Player");
			tabList.add("Progress");
			tabList.add("Progress_Bait");
			tabList.add("Progress_Fish");
			tabList.add("Shop");
			
			if (SenderUtil.hasPermission(sender, permissionMenuBuyBait) && enableMenuPageBuyBait) {
				tabList.add("Shop_Buy_Bait");
			}
			if (SenderUtil.hasPermission(sender, permissionMenuSellBait) && enableMenuPageSellBait) {
				tabList.add("Shop_Sell_Bait");
			}
			if (SenderUtil.hasPermission(sender, permissionMenuSellFish) && enableMenuPageSellFish) {
				tabList.add("Shop_Sell_Fish");
			}
			if (SenderUtil.hasPermission(sender, permissionMenuGuide)) {
				tabList.add("Guide");
			}
		}
		
		return tabList;
	}
}
