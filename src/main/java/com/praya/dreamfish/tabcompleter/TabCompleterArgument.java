package com.praya.dreamfish.tabcompleter;

import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

public abstract class TabCompleterArgument {

	private final Plugin plugin;
	private final String mainArgument;
	
	public TabCompleterArgument(Plugin plugin, String mainArgument) {
		if (plugin == null || mainArgument == null) {
			throw new IllegalArgumentException();
		} else {
			this.plugin = plugin;
			this.mainArgument = mainArgument;
		}
	}
	
	public abstract List<String> execute(CommandSender sender, String[] args);
	
	public final Plugin getPlugin() {
		return this.plugin;
	}
	
	public final String getMainArgument() {
		return this.mainArgument;
	}
}