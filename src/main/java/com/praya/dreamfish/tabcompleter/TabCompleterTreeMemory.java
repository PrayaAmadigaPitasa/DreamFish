package com.praya.dreamfish.tabcompleter;

import java.util.Collection;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.game.TabCompleterTreeManager;
import com.praya.dreamfish.tabcompleter.bait.TabCompleterBait;
import com.praya.dreamfish.tabcompleter.dreamfish.TabCompleterDreamFish;

public class TabCompleterTreeMemory extends TabCompleterTreeManager {

	private final HashMap<String, TabCompleterTree> mapCommandTree = new HashMap<String, TabCompleterTree>();
	
	private TabCompleterTreeMemory(DreamFish plugin) {
		super(plugin);
		
		final TabCompleterTree tabCompleterDreamFish = TabCompleterDreamFish.getInstance();
		final TabCompleterTree tabCompleterBait = TabCompleterBait.getInstance();
		
		register(tabCompleterDreamFish);
		register(tabCompleterBait);
	}
	
	private static class CommandTreeMemorySingleton {
		private static final TabCompleterTreeMemory instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			instance = new TabCompleterTreeMemory(plugin);
		}
	}
	
	public static final TabCompleterTreeMemory getInstance() {
		return CommandTreeMemorySingleton.instance;
	}
	
	@Override
	public final Collection<String> getCommands() {
		return this.mapCommandTree.keySet();
	}
	
	@Override
	public final Collection<TabCompleterTree> getAllTabCompleterTree() {
		return this.mapCommandTree.values();
	}
	
	@Override
	public final TabCompleterTree getTabCompleterTree(String command) {
		if (command != null) {
			for (String key : getCommands()) {
				if (key.equalsIgnoreCase(command)) {
					return this.mapCommandTree.get(key);
				}
			}
		}
		
		return null;
	}
	
	protected final boolean register(TabCompleterTree commandTree) {
		if (commandTree != null && !getAllTabCompleterTree().contains(commandTree)) {
			final String command = commandTree.getCommand();
			final PluginCommand pluginCommand = Bukkit.getPluginCommand(command);
			
			if (pluginCommand != null) {
			
				this.mapCommandTree.put(command, commandTree);
				
				pluginCommand.setTabCompleter(commandTree);
				return true;
			}
		}
		
		return false;
	}
}
