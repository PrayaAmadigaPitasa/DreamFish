package com.praya.dreamfish.tabcompleter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TabCompleterUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.CommandArgument;
import com.praya.dreamfish.command.CommandTree;
import com.praya.dreamfish.manager.game.CommandTreeManager;
import com.praya.dreamfish.manager.game.GameManager;

public class TabCompleterTree implements TabCompleter {

	private final String command;
	private final HashMap<String, TabCompleterArgument> mapTabCompleterArgument; 
	
	protected TabCompleterTree(String command) {
		this(command, null);
	}
	
	protected TabCompleterTree(String command, HashMap<String, TabCompleterArgument> mapTabCompleterArgument) {
		if (command == null) {
			throw new IllegalArgumentException();
		} else {
			this.command = command;
			this.mapTabCompleterArgument = mapTabCompleterArgument != null ? mapTabCompleterArgument : new HashMap<String, TabCompleterArgument>();
		}
	}
	
	public final String getCommand() {
		return this.command;
	}
	
	public final Collection<String> getMainArguments() {
		return this.mapTabCompleterArgument.keySet();
	}
	
	public final Collection<TabCompleterArgument> getAllTabCompleterArgument() {
		return this.mapTabCompleterArgument.values();
	}
	
	public final TabCompleterArgument getTabCompleterArgument(String argument) {
		if (argument != null) {
			for (String key : getMainArguments()) {
				if (key.equalsIgnoreCase(argument)) {
					return this.mapTabCompleterArgument.get(key);
				}
			}
		}
		
		return null;
	}
	
	public final boolean isRegistered(String mainArgument) {
		return getTabCompleterArgument(mainArgument) != null;
	}
	
	public final boolean register(TabCompleterArgument tabCompleterArgument) {
		if (tabCompleterArgument != null && !getAllTabCompleterArgument().contains(tabCompleterArgument)) {
			final String mainArgument = tabCompleterArgument.getMainArgument();
			
			this.mapTabCompleterArgument.put(mainArgument, tabCompleterArgument);
			
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final GameManager gameManager = plugin.getGameManager();
		final CommandTreeManager commandTreeManager = gameManager.getCommandTreeManager();
		final List<String> tabList = new ArrayList<String>();
		
		if (command != null) {
			final String name = command.getName();
			final CommandTree commandTree = commandTreeManager.getCommandTree(name);
			
			if (commandTree != null) {
				if (args.length > 1) {
					final String argument = args[0];
					final CommandArgument commandArgument = commandTree.getCommandArgument(argument);
					
					if (commandArgument != null) {
						final String mainArgument = commandArgument.getMainArgument();
						final TabCompleterArgument tabCompleterArgument = getTabCompleterArgument(mainArgument);
						
						if (tabCompleterArgument != null) {
							final List<String> listTabArgument = tabCompleterArgument.execute(sender, args);
							
							if (listTabArgument != null) {
								if (!listTabArgument.isEmpty()) {
									tabList.addAll(listTabArgument);
								} else {
									tabList.add("");
								}
							} else {
								return null;
							}
						} else {
							tabList.add("");
						}
					}
				} else {
					final Collection<CommandArgument> allCommandArgument = commandTree.getAllCommandArgument();
					
					for (CommandArgument commandArgument : allCommandArgument) {
						final String permission = commandArgument.getPermission();
						
						if (SenderUtil.hasPermission(sender, permission)) {
							final String mainArgument = commandArgument.getMainArgument();
							
							tabList.add(mainArgument);
						}
					}
				}
			}
		}
		
		return TabCompleterUtil.returnList(tabList, args);
	}
}