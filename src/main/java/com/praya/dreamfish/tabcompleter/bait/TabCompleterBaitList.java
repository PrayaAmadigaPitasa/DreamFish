package com.praya.dreamfish.tabcompleter.bait;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.tabcompleter.TabCompleterArgument;

public class TabCompleterBaitList extends TabCompleterArgument {

	private static final Command COMMAND = Command.BAIT_LIST;
	
	private TabCompleterBaitList(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterBaitListSingleton {
		private static final TabCompleterBaitList instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterBaitList(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterBaitList getInstance() {
		return TabCompleterBaitListSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			tabList.add("[<page>]");
		}
		
		return tabList;
	}
}
