package com.praya.dreamfish.tabcompleter.bait;

import com.praya.dreamfish.tabcompleter.TabCompleterArgument;
import com.praya.dreamfish.tabcompleter.TabCompleterTree;

public class TabCompleterBait extends TabCompleterTree {

	private static final String COMMAND = "Bait";
	
	private TabCompleterBait() {
		super(COMMAND);
		
		final TabCompleterArgument tabCompleterArgumentList = TabCompleterBaitList.getInstance();
		final TabCompleterArgument tabCompleterArgumentLoad = TabCompleterBaitLoad.getInstance();
		
		register(tabCompleterArgumentList);
		register(tabCompleterArgumentLoad);
	}
	
	private static class CommandDreamFishSingleton {
		private static final TabCompleterBait instance = new TabCompleterBait();
	}
	
	public static final TabCompleterBait getInstance() {
		return CommandDreamFishSingleton.instance;
	}
}