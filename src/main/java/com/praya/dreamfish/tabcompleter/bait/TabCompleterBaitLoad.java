package com.praya.dreamfish.tabcompleter.bait;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.manager.game.BaitManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.tabcompleter.TabCompleterArgument;

public class TabCompleterBaitLoad extends TabCompleterArgument {

	private static final Command COMMAND = Command.BAIT_LOAD;
	
	private TabCompleterBaitLoad(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterBaitLoadSingleton {
		private static final TabCompleterBaitLoad instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterBaitLoad(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterBaitLoad getInstance() {
		return TabCompleterBaitLoadSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final GameManager gameManager = plugin.getGameManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			final Collection<String> baitIds = baitManager.getBaitPropertiesIds();
			
			if (!baitIds.isEmpty()) {
				tabList.addAll(baitIds);
			}
		} else if (args.length == 3) {
			return null;
		} else if (args.length == 4) {
			tabList.add("[<amount>]");
		}
		
		return tabList;
	}
}
