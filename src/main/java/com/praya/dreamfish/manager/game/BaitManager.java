package com.praya.dreamfish.manager.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.inventory.ItemStack;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.handler.HandlerManager;

public abstract class BaitManager extends HandlerManager {
	
	protected BaitManager(DreamFish plugin) {
		super(plugin);
	};
	
	public abstract Collection<String> getBaitPropertiesIds();
	public abstract Collection<BaitProperties> getAllBaitProperties();
	public abstract BaitProperties getBaitProperties(String bait);
	public abstract BaitProperties getBaitProperties(ItemStack item);
	
	public final boolean isBaitExists(String bait) {
		return getBaitProperties(bait) != null;
	}
	
	public final boolean isBaitExists(ItemStack item) {
		return getBaitProperties(item) != null;
	}
	
	public final List<String> getBuyableBaitList() {
		final List<String> list = new ArrayList<String>();

		for (BaitProperties baitProperties : getAllBaitProperties()) {
			if (baitProperties.isBuyable()) {
				final String bait = baitProperties.getId();
				
				list.add(bait);
			}
		}
		
		return list;
	}
}

