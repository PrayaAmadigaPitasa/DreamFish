package com.praya.dreamfish.manager.game;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;

public abstract class GameManager extends HandlerManager {
	
	protected GameManager(DreamFish plugin) {
		super(plugin);
	};
	
	public abstract BaitManager getBaitManager();
	public abstract FishManager getFishManager();
	public abstract EventManager getEventManager();
	public abstract MenuManager getMenuManager();
	public abstract CommandTreeManager getCommandTreeManager();
	public abstract TabCompleterTreeManager getTabCompleterTreeManager();
	public abstract TreasureManager getTreasureManager();
}
