package com.praya.dreamfish.manager.game;

import java.util.Collection;
import org.bukkit.inventory.ItemStack;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.handler.HandlerManager;

public abstract class FishManager extends HandlerManager {
	
	protected FishManager(DreamFish plugin) {
		super(plugin);
	};
	
	public abstract Collection<String> getFishPropertiesIds();
	public abstract Collection<FishProperties> getAllFishProperties();
	public abstract FishProperties getFishProperties(String fish);
	public abstract FishProperties getFishProperties(ItemStack item);
	
	public final boolean isFishExists(String fish) {
		return getFishProperties(fish) != null;
	}
	
	public final boolean isFishExists(ItemStack item) {
		return getFishProperties(item) != null;
	}
}
