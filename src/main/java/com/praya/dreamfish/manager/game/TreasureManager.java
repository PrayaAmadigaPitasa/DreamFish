package com.praya.dreamfish.manager.game;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;
import com.praya.dreamfish.treasure.TreasureItem;

import java.util.Collection;

public abstract class TreasureManager extends HandlerManager {

    protected TreasureManager(DreamFish plugin) {
        super(plugin);
    }

    public abstract Collection<String> getTreasureItemID();
    public abstract Collection<TreasureItem> getTreasureItems();
    public abstract TreasureItem getTreasureItem(String id);
}
