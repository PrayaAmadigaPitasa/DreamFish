package com.praya.dreamfish.manager.game;

import java.util.Set;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;

public abstract class EventManager extends HandlerManager {
	
	protected EventManager(DreamFish plugin) {
		super(plugin);
	}
	
	public abstract Set<PlayerFishEvent> getAllFakePlayerFishEvent();
	public abstract PlayerFishEvent callFakePlayerFishEvent(Player player, Entity entity, Object hookEntity, State state);
	public abstract boolean isFakePlayerFishEvent(PlayerFishEvent playerFishEvent);
	public abstract boolean removeFakePlayerFishEvent(PlayerFishEvent playerFishEvent);
}
