package com.praya.dreamfish.manager.game;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;
import com.praya.dreamfish.menu.MenuDreamFish;

public abstract class MenuManager extends HandlerManager {
	
	protected MenuManager(DreamFish plugin) {
		super(plugin);
	}
	
	public abstract MenuDreamFish getMenuDreamFish();
}