package com.praya.dreamfish.manager.game;

import java.util.Collection;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;
import com.praya.dreamfish.tabcompleter.TabCompleterTree;

public abstract class TabCompleterTreeManager extends HandlerManager {
	
	protected TabCompleterTreeManager(DreamFish plugin) {
		super(plugin);
	}
	
	public abstract Collection<String> getCommands();
	public abstract Collection<TabCompleterTree> getAllTabCompleterTree();
	public abstract TabCompleterTree getTabCompleterTree(String command);
}
