package com.praya.dreamfish.manager.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.CommandArgument;
import com.praya.dreamfish.command.CommandTree;
import com.praya.dreamfish.handler.HandlerManager;

public abstract class CommandTreeManager extends HandlerManager {
	
	protected CommandTreeManager(DreamFish plugin) {
		super(plugin);
	}
	
	public abstract Collection<String> getCommands();
	public abstract Collection<CommandTree> getAllCommandTree();
	public abstract CommandTree getCommandTree(String command);
	
	public final List<CommandArgument> getAllCommandArgument() {
		final List<CommandArgument> allCommandArgument = new ArrayList<CommandArgument>();
		
		for (CommandTree commandTree : getAllCommandTree()) {
			for (CommandArgument commandArgument : commandTree.getAllCommandArgument()) {
				allCommandArgument.add(commandArgument);
			}
		}
		
		return allCommandArgument;
	}
}
