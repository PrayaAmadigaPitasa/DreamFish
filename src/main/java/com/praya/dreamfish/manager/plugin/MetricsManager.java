package com.praya.dreamfish.manager.plugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;
import com.praya.dreamfish.metrics.service.BStats;

public abstract class MetricsManager extends HandlerManager {
	
	protected MetricsManager(DreamFish plugin) {
		super(plugin);
	}
	
	public abstract BStats getMetricsBStats();
}
