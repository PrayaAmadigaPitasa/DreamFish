package com.praya.dreamfish.manager.plugin;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.block.Biome;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import com.praya.agarthalib.utility.BiomeUtil;
import com.praya.agarthalib.utility.ListUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PluginUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.bait.BaitFishing;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.handler.HandlerManager;
import com.praya.dreamfish.item.ItemRequirement;
import com.praya.dreamfish.manager.game.BaitManager;
import com.praya.dreamfish.manager.game.FishManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.manager.player.PlayerBaitManager;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerFishingModeManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.placeholder.replacer.ReplacerMVDWPlaceholderAPI;
import com.praya.dreamfish.placeholder.replacer.ReplacerPlaceholderAPI;
import com.praya.dreamfish.player.PlayerBait;
import com.praya.dreamfish.player.PlayerFishing;
import com.praya.dreamfish.player.PlayerFishingMode;

public abstract class PlaceholderManager extends HandlerManager {
	
	protected PlaceholderManager(DreamFish plugin) {
		super(plugin);
	};
	
	protected abstract HashMap<String, String> getMapPlaceholder();
	public abstract Collection<String> getPlaceholderIds();
	public abstract Collection<String> getPlaceholders();
	public abstract String getPlaceholder(String id);
	
	public final boolean isPlaceholderExists(String id) {
		return getPlaceholder(id) != null;
	}
	
	public final void registerAll() {
		final String placeholder = plugin.getPluginPlaceholder();
		
		if (PluginUtil.isPluginInstalled("PlaceholderAPI")) {
			new ReplacerPlaceholderAPI(plugin, placeholder).hook();
		}
		
		if (PluginUtil.isPluginInstalled("MVdWPlaceholderAPI")) {
			new ReplacerMVDWPlaceholderAPI(plugin, placeholder).register();
		}
	}
	
	public final List<String> localPlaceholder(List<String> list) {
		final String divider = "\n";
		final String builder = TextUtil.convertListToString(list, divider);
		final String text = localPlaceholder(builder);
		
		return ListUtil.convertStringToList(text, divider);
	}
	
	public final String localPlaceholder(String text) {
		return TextUtil.placeholder(getMapPlaceholder(), text);
	}
	
	public final List<String> pluginPlaceholder(List<String> list, String... identifiers) {
		return pluginPlaceholder(list, null, identifiers);
	}
	
	public final List<String> pluginPlaceholder(List<String> list, Player player, String... identifiers) {
		final String divider = "\n";
		final String builder = TextUtil.convertListToString(list, divider);
		final String text = pluginPlaceholder(builder, player, identifiers);
		
		return ListUtil.convertStringToList(text, divider);
	}
	
	public final String pluginPlaceholder(String text, String... identifiers) {
		return pluginPlaceholder(text, null, identifiers);
	}
	
	public final String pluginPlaceholder(String text, Player player, String... identifiers) {
		final HashMap<String, String> map = getMapPluginPlaceholder(player, identifiers);
		
		return TextUtil.placeholder(map, text);
	}
	
	public final HashMap<String, String> getMapPluginPlaceholder(String... identifiers) {
		return getMapPluginPlaceholder(null, identifiers);
	}
	
	public final HashMap<String, String> getMapPluginPlaceholder(Player player, String... identifiers) {
		final String placeholder = plugin.getPluginPlaceholder();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		for (String identifier : identifiers) {
			final String replacement = getReplacement(player, identifier);
			
			if (replacement != null) {
				final String key = placeholder + "_" + identifier;
				
				map.put(key, replacement);
			}
		}
		
		return map;
	}
	
	public final String getReplacement(Player player, String identifier) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		final PlayerFishingModeManager playerFishingModeManager = playerManager.getPlayerFishingModeManager();
		final PlayerBaitManager playerBaitManager = playerManager.getPlayerBaitManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		final FishManager fishManager = gameManager.getFishManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final String[] parts = identifier.split(":");
        final int length = parts.length;
        
        if (length > 0) {
        	final String key = parts[0];
        	
        	if (key.equalsIgnoreCase("player_fishing")) {
        		final PlayerFishing playerFishing = playerFishingManager.getPlayerFishing(player);
        		
        		if (playerFishing != null && length > 1) {	
        			final String mainData = parts[1];
        			
        			if (mainData.equalsIgnoreCase("Level")) {
        				final int level = playerFishing.getLevel();
        				
        				return String.valueOf(level);
        			} else if (mainData.equalsIgnoreCase("Exp")) {
        				final double exp = MathUtil.roundNumber(playerFishing.getExp());
        				
        				return String.valueOf(exp);
        			} else if (mainData.equalsIgnoreCase("Exp_To_Up")) {
        				final double expToUp = MathUtil.roundNumber(playerFishing.getExpToUp());
        				
        				return String.valueOf(expToUp);
        			} else if (mainData.equalsIgnoreCase("Total_Unlocked_Bait")) {
        				final int totalUnlockedBait = playerFishing.getUnlockedBait().size();
        				
        				return String.valueOf(totalUnlockedBait);
        			} else if (mainData.equalsIgnoreCase("Total_Unlocked_Fish")) {
        				final int totalUnlockedFish = playerFishing.getUnlockedFish().size();
        				
        				return String.valueOf(totalUnlockedFish);
        			} else if (mainData.equalsIgnoreCase("Bonus_Effectiveness")) {
        				final double bonusEffectiveness = MathUtil.roundNumber(playerFishing.getBonusEffectiveness());
        				
        				return String.valueOf(bonusEffectiveness) + "%";
        			} else if (mainData.equalsIgnoreCase("Bonus_Endurance")) {
        				final double bonusEndurance = MathUtil.roundNumber(playerFishing.getBonusEndurance());
        				
        				return String.valueOf(bonusEndurance) + "%";
        			} else if (mainData.equalsIgnoreCase("Bonus_Speed")) {
        				final double bonusSpeed = MathUtil.roundNumber(playerFishing.getBonusSpeed());
        				
        				return String.valueOf(bonusSpeed) + "%";
        			}
        		}
        		
        		return "";
        	} else if (key.equalsIgnoreCase("player_fishing_mode")) {
        		final PlayerFishingMode playerFishingMode = playerFishingModeManager.getPlayerFishingMode(player);
        		
        		if (playerFishingMode != null && length > 1) {
        			final String mainData = parts[1];
        			
        			if (mainData.equalsIgnoreCase("fish")) {
        				final FishProperties fishProperties = playerFishingMode.getFishProperties();
        				final String fish = fishProperties.getId();
        				
        				return fish;
        			} else if (mainData.equalsIgnoreCase("fish_power")) {
        				final double fishPower = playerFishingMode.getFishPower();
        				
        				return String.valueOf(fishPower);
        			} else if (mainData.equalsIgnoreCase("tension")) {
        				final double tension = playerFishingMode.getTension();
        				
        				return String.valueOf(tension);
        			}
        		}
        		
        		return "";
        	} else if (key.equalsIgnoreCase("player_bait")) {
        		final PlayerBait playerBait = playerBaitManager.getPlayerBait(player);
        		
        		if (playerBait != null && length > 1) {
        			final String mainData = parts[1];
        			
        			if (mainData.equalsIgnoreCase("mark_bait") || mainData.equalsIgnoreCase("mark")) {
        				final String markBait = playerBait.getMarkBait();
        				
        				return markBait;
        			} else if (mainData.equalsIgnoreCase("hook_bait") || mainData.equalsIgnoreCase("hook")) {
        				final String hookBait = playerBait.getHookBait();
        				
        				return hookBait;
        			}
        		}
        		
        		return "";
        	} else if (key.equalsIgnoreCase("fish")) {
        		if (length > 1) {
        			final String fish = parts[1];
        			final FishProperties fishProperties = fishManager.getFishProperties(fish);
        			
        			if (fishProperties != null && length > 2) {
        				final String mainData = parts[2];
        				
        				if (mainData.equalsIgnoreCase("as_bait")) {
        					final boolean asBait = fishProperties.asBait();
        					
        					return String.valueOf(asBait);
        				} else if (mainData.equalsIgnoreCase("type")) {
        					final EntityType type = fishProperties.getType();
        					
        					return TextUtil.toTitleCase(type.toString().replace("_", " "));
        				} else if (mainData.equalsIgnoreCase("invisibile")) {
        					final boolean invisible = fishProperties.isInvisible();
        					
        					return String.valueOf(invisible);
        				} else if (mainData.equalsIgnoreCase("price")) {
        					final double price = fishProperties.getPrice();
        					
        					return String.valueOf(price);
        				} else if (mainData.equalsIgnoreCase("resistance")) {
        					final double resistance = fishProperties.getResistance();
        					
        					return String.valueOf(resistance);
        				} else if (mainData.equalsIgnoreCase("power")) {
        					final double power = fishProperties.getPower();
        					
        					return String.valueOf(power);
        				} else if (mainData.equalsIgnoreCase("max_power")) {
        					final double maxPower = fishProperties.getMaxPower();
        					
        					return String.valueOf(maxPower);
        				} else if (mainData.equalsIgnoreCase("max_speed")) {
        					final double maxSpeed = fishProperties.getMaxSpeed();
        					
        					return String.valueOf(maxSpeed);
        				} else if (mainData.equalsIgnoreCase("max_dive")) {
        					final double maxDive = fishProperties.getMaxDive();
        					
        					return String.valueOf(maxDive);
        				} else if (mainData.equalsIgnoreCase("average_length")) {
        					final double averageLength = fishProperties.getAverageLength();
        					
        					return String.valueOf(averageLength);
        				} else if (mainData.equalsIgnoreCase("average_weight")) {
        					final double averageWeight = fishProperties.getAverageWeight();
        					
        					return String.valueOf(averageWeight);
        				} else if (mainData.equalsIgnoreCase("requirement")) {
        					final ItemRequirement itemRequirement = fishProperties.getRequirement();
        					
        					if (itemRequirement != null && length > 3) {
        						final String requirement = parts[3];
        						
        						if (requirement.equalsIgnoreCase("level")) {
        							final int level = itemRequirement.getLevel();
        							
        							return String.valueOf(level);
        						} else if (requirement.equalsIgnoreCase("permission")) {
        							final String permission = itemRequirement.getPermission();
        							
        							return permission != null ? permission : placeholderManager.getPlaceholder("none");
        						}
        					}
        				} else if (mainData.equalsIgnoreCase("in_region")) {
        					if (length > 3) {
        						final String region = parts[3];
        						final boolean inRegion = fishProperties.inRegion(region);
        						
        						return String.valueOf(inRegion);
        					}
        				} else if (mainData.equalsIgnoreCase("in_biome")) {
        					if (length > 3) {
        						final String textBiome = parts[3];
        						final Biome biome = BiomeUtil.getBiome(textBiome);
        						final boolean inBiome = fishProperties.inBiome(biome);
        						
        						return String.valueOf(inBiome);
        					}
        				}
        			}
        		}
        		
        		return "";
        	} else if (key.equalsIgnoreCase("bait")) {
        		if (length > 1) {
        			final String bait = parts[1];
        			final BaitProperties baitProperties = baitManager.getBaitProperties(bait);
        			
        			if (baitProperties != null && length > 2) {
        				final String mainData = parts[2];
        				
        				if (mainData.equalsIgnoreCase("buyable")) {
        					final boolean buyable = baitProperties.isBuyable();
        					
        					return String.valueOf(buyable);
        				} else if (mainData.equalsIgnoreCase("price")) {
        					final double price = baitProperties.getPrice();
        					
        					return String.valueOf(price);
        				} else if (mainData.equalsIgnoreCase("requirement")) {
        					final ItemRequirement itemRequirement = baitProperties.getRequirement();
        					
        					if (itemRequirement != null && length > 3) {
        						final String requirement = parts[3];
        						
        						if (requirement.equalsIgnoreCase("level")) {
        							final int level = itemRequirement.getLevel();
        							
        							return String.valueOf(level);
        						} else if (requirement.equalsIgnoreCase("permission")) {
        							final String permission = itemRequirement.getPermission();
        							
        							return permission != null ? permission : "none";
        						}
        					}
        				} else if (mainData.equalsIgnoreCase("fishing")) {
        					final BaitFishing baitFishing = baitProperties.getBaitFishing();
        					
        					if (baitFishing != null && length > 3) {
        						final String subData = parts[3];
        						
        						if (subData.equalsIgnoreCase("total_possibility")) {
        							final double totalPossibility = baitFishing.getTotalPossibility();
        							
        							return String.valueOf(totalPossibility);
        						} else if (subData.equalsIgnoreCase("chance")) {
        							if (length > 4) {
        								final String fish = parts[4];
        								final double chance = baitFishing.getFishChance(fish);
        								
        								return String.valueOf(chance);
        							}
        						} else if (subData.equalsIgnoreCase("possibility")) {
        							if (length > 4) {
        								final String fish = parts[4];
        								final double possibility = baitFishing.getFishPossibility(fish);
        								
        								return String.valueOf(possibility);
        							}
        						}
        					}
        				}
        			}
        		}
        		
        		return "";
        	}
        }
        
        return null;
	}
}