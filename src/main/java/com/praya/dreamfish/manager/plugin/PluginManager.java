package com.praya.dreamfish.manager.plugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;

public abstract class PluginManager extends HandlerManager {
	
	protected PluginManager(DreamFish plugin) {
		super(plugin);
	}
	
	public abstract LanguageManager getLanguageManager();
	public abstract PlaceholderManager getPlaceholderManager();
	public abstract PluginPropertiesManager getPluginPropertiesManager();
	public abstract MetricsManager getMetricsManager();
	public abstract CommandManager getCommandManager();
}
