package com.praya.dreamfish.manager.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.plugin.PluginLoader;

import com.praya.agarthalib.utility.SystemUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;
import core.praya.agarthalib.builder.plugin.PluginPropertiesResourceBuild;
import core.praya.agarthalib.builder.plugin.PluginPropertiesStreamBuild;
import core.praya.agarthalib.builder.plugin.PluginTypePropertiesBuild;

public abstract class PluginPropertiesManager extends HandlerManager {
	
	protected PluginPropertiesManager(DreamFish plugin) {
		super(plugin);
	}
	
	protected abstract PluginPropertiesResourceBuild getPluginPropertiesResource();
	protected abstract PluginPropertiesStreamBuild getPluginPropertiesStream();
	public abstract Collection<String> getPluginIds();
	public abstract Collection<PluginPropertiesStreamBuild> getAllPluginProperties();
	public abstract PluginPropertiesStreamBuild getPluginProperties(String plugin);
	
	public final List<String> getAllPluginType() {
		final PluginPropertiesStreamBuild pluginPropertiesStream = getPluginPropertiesStream();
		final List<String> listType = new ArrayList<String>(pluginPropertiesStream.getTypeIDs());
		
		return listType;
	}
	
	public final PluginTypePropertiesBuild getPluginTypeProperties(String type) {
		if (type != null) {
			for (String key : getAllPluginType()) {
				if (key.equalsIgnoreCase(type)) {
					return getPluginPropertiesStream().getTypeProperties(type);
				}
			}
		}
		
		return null;
	}
	
	public final boolean isPluginTypeExists(String type) {
		return getPluginTypeProperties(type) != null;
	}
	
	public final String getPluginName() {
		return getPluginPropertiesStream().getName();
	}
	
	public final String getPluginVersion() {
		final String pluginType = plugin.getPluginType();
		
		return getPluginVersion(pluginType);
	}
	
	public final String getPluginVersion(String type) {
		final PluginTypePropertiesBuild pluginTypeProperties = getPluginTypeProperties(type);
		
		return pluginTypeProperties != null ? pluginTypeProperties.getVersion() : getPluginPropertiesResource().getVersion();
	}
	
	public final String getPluginCompany() {
		return getPluginPropertiesStream().getCompany();
	}
	
	public final String getPluginAuthor() {
		return getPluginPropertiesStream().getAuthor();
	}
	
	public final String getPluginWebsite() {
		final String pluginType = plugin.getPluginType();
		
		return getPluginWebsite(pluginType);
	}
	
	public final String getPluginWebsite(String type) {
		final PluginTypePropertiesBuild pluginTypeProperties = getPluginTypeProperties(type);
		
		return pluginTypeProperties != null ? pluginTypeProperties.getWebsite() : getPluginPropertiesResource().getWebsite();
	}
	
	public final List<String> getPluginDevelopers() {
		return getPluginPropertiesStream().getDevelopers();
	}
	
	public final boolean isLatestVersion() {
		final String type = plugin.getPluginType();
		final String versionLatest = getPluginVersion(type);
		final String versionCurrent = plugin.getPluginVersion();
		
		
		return versionLatest.equalsIgnoreCase(versionCurrent);
	}
	
	public final boolean isActivated() {
		return getPluginPropertiesStream().isActivated();
	}
	
	public final void check() {		
		final PluginManager pluginManager = plugin.getPluginManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		if (!isActivated()) {
			final String message = lang.getText("Plugin_Deactivated");
			
			disable(message);
			return;			
		} else if (!checkPluginName() || !checkPluginAuthor()) {
			final String message = lang.getText("Plugin_Information_Not_Match");
			
			disable(message);
			return;
		}
	}
	
	private final boolean checkPluginName() {
		final String pluginName = getPluginName();
		
		if (pluginName != null) {
			final String pluginNameResource = getPluginPropertiesResource().getName();
			
			if (!pluginName.equals(pluginNameResource)) {
				return false;
			}
		}
		
		return true;
	}
	
	private final boolean checkPluginAuthor() {
		final String pluginAuthor = getPluginAuthor();
		
		if (pluginAuthor != null) {
			final String pluginAuthorResource = getPluginPropertiesResource().getAuthor();
			
			if (!pluginAuthor.equals(pluginAuthorResource)) {
				return false;
			}
		}
		
		return true;
	}
	
	private final void disable(String message) {
		final PluginLoader pluginLoader = plugin.getPluginLoader();
		
		if (message != null) {
			SystemUtil.sendMessage(message);
		}
		
		pluginLoader.disablePlugin(plugin);
	}
}
