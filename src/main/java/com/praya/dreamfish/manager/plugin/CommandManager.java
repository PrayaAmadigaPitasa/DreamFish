package com.praya.dreamfish.manager.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.command.CommandSender;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;

import core.praya.agarthalib.builder.command.CommandBuild;

public abstract class CommandManager extends HandlerManager {
	
	protected CommandManager(DreamFish plugin) {
		super(plugin);
	};
	
	public abstract Collection<String> getCommandIds();
	public abstract Collection<CommandBuild> getAllCommandBuild();
	public abstract CommandBuild getCommandBuild(String id);
	
	protected final boolean isCommandExists(String id) {
		return getCommandBuild(id) != null;
	}
	
	protected final String getMain(String id) {
		final CommandBuild commandBuild = getCommandBuild(id);
		
		return commandBuild != null ? commandBuild.getMain() : null;
	}
	
	protected final String getPermission(String id) {
		final CommandBuild commandBuild = getCommandBuild(id);
		
		return commandBuild != null ? commandBuild.getPermission() : null;
	}
	
	protected final List<String> getAliases(String id) {
		final CommandBuild commandBuild = getCommandBuild(id);
		
		return commandBuild != null ? commandBuild.getAliases() : new ArrayList<String>();
	}
	
	protected final boolean checkPermission(CommandSender sender, String id) {
		final CommandBuild commandBuild = getCommandBuild(id);
		
		if (commandBuild != null) {
			final String permission = commandBuild.getPermission();
			
			return SenderUtil.hasPermission(sender, permission);
		} else {
			return false;
		}
	}
	
	protected final boolean checkCommand(String arg, String id) {
		final CommandBuild commandBuild = getCommandBuild(id);
		
		if (commandBuild != null) {
			final String main = commandBuild.getMain();
			
			if (main.equalsIgnoreCase(arg)) {
				return true;
			} else {
				for (String aliases : commandBuild.getAliases()) {
					if (aliases.equalsIgnoreCase(arg)) {
						return true;
					}
				}
			}
		} 
		
		return false;
	}
}