package com.praya.dreamfish.manager.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.handler.HandlerManager;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.main.LanguageBuild;
import core.praya.agarthalib.builder.message.MessageBuild;

public abstract class LanguageManager extends HandlerManager {
	
	protected LanguageManager(DreamFish plugin) {
		super(plugin);
	};
	
	public abstract Collection<String> getLanguageIds();
	public abstract Collection<LanguageBuild> getAllLanguageBuild();
	public abstract LanguageBuild getLocaleLanguage(String id);
	public abstract MessageBuild getLocaleMessage(String id, String key);
	
	public final LanguageBuild getLanguage(String id) {
		if (id != null) {
			final String[] parts = id.split("_");
			final int length = parts.length;
			
			for (int size = length; size > 0; size--) {
				final StringBuilder builder = new StringBuilder();
				
				for (int index = 0; index < size; index++) {
					final String component = parts[index];
					
					if (index > 0) {
						builder.append("_");
					}
					
					builder.append(component);
					
				}
					
				final String identifier = builder.toString();
				final LanguageBuild languageBuild = getLocaleLanguage(identifier);
				
				if (languageBuild != null) {
					return languageBuild;
				}
			}
		}
		
		return getLocaleLanguage("en");
	}
	
	public final boolean isLanguageExists(String id) {
		return getLocaleLanguage(id) != null;
	}
	
	public final MessageBuild getMessage(LivingEntity entity, String key) {
		if (entity != null && entity instanceof Player) {
			final Player player = (Player) entity;
			final String locale = Bridge.getBridgePlayer().getLocale(player);
			
			return getMessage(locale, key);
		} else {
			return getMessage(key);
		}
	}
	
	public final MessageBuild getMessage(CommandSender sender, String key) {
		if (sender != null && sender instanceof Player) {
			final Player player = (Player) sender;
			final String locale = Bridge.getBridgePlayer().getLocale(player);
			
			return getMessage(locale, key);
		} else {
			return getMessage(key);
		}
	}
	
	public final MessageBuild getMessage(String key) {
		final DreamFishConfig  mainConfig = plugin.getMainConfig();
		final String locale = mainConfig.getGeneralLocale();
		
		return getMessage(locale, key);
	}
	
	public final MessageBuild getMessage(String id, String key) {
		if (id != null) {
			final String[] parts = id.split("_");
			final int length = parts.length;
			
			for (int size = length; size > 0; size--) {
				final StringBuilder builder = new StringBuilder();
				
				for (int index = 0; index < size; index++) {
					final String component = parts[index];
					
					if (index > 0) {
						builder.append("_");
					}
					
					builder.append(component);
				}
				
				final String identifier = builder.toString();
				final LanguageBuild languageBuild = getLocaleLanguage(identifier);
				
				if (languageBuild != null) {
					final MessageBuild message = languageBuild.getMessage(key);
					
					if (message.isSet()) {
						return message;
					}
				}
			}
		}
		
		return getLocaleMessage("en", key);
	}
	
	public final List<String> getListText(LivingEntity entity, String key) {
		if (entity != null && entity instanceof Player) {
			final Player player = (Player) entity;
			final String locale = Bridge.getBridgePlayer().getLocale(player);
			
			return getListText(locale, key);
		} else {
			return getListText(key);
		}
	}
	
	public final List<String> getListText(CommandSender sender, String key) {
		if (sender != null && sender instanceof Player) {
			final Player player = (Player) sender;
			final String locale = Bridge.getBridgePlayer().getLocale(player);
			
			return getListText(locale, key);
		} else {
			return getListText(key);
		}
	}
	
	public final List<String> getListText(String key) {
		final DreamFishConfig  mainConfig = plugin.getMainConfig();
		final String locale = mainConfig.getGeneralLocale();
		
		return getListText(locale, key);
	}
	
	public final List<String> getListText(String id, String key) {
		final MessageBuild message = getMessage(id, key);
		
		return message != null ? message.getListText() : new ArrayList<String>();
	}
	
	public final String getText(LivingEntity entity, String key) {
		if (entity != null && entity instanceof Player) {
			final Player player = (Player) entity;
			final String locale = Bridge.getBridgePlayer().getLocale(player);
			
			return getText(locale, key);
		} else {
			return getText(key);
		}
	}
	
	public final String getText(CommandSender sender, String key) {
		if (sender != null && sender instanceof Player) {
			final Player player = (Player) sender;
			final String locale = Bridge.getBridgePlayer().getLocale(player);
			
			return getText(locale, key);
		} else {
			return getText(key);
		}
	}
	
	public final String getText(String key) {
		final DreamFishConfig  mainConfig = plugin.getMainConfig();
		final String locale = mainConfig.getGeneralLocale();
		
		return getText(locale, key);
	}
	
	public final String getText(String id, String key) {
		final MessageBuild message = getMessage(id, key);
		
		return message != null ? message.getText() : "";
	}
}