package com.praya.dreamfish.manager.player;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.handler.HandlerManager;
import com.praya.dreamfish.item.ItemRequirement;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.game.BaitManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.player.PlayerBait;

import core.praya.agarthalib.builder.message.MessageBuild;

import com.praya.agarthalib.utility.PlayerUtil;

public abstract class PlayerBaitManager extends HandlerManager {
	
	protected PlayerBaitManager(DreamFish plugin) {
		super(plugin);
	};
	
	public abstract PlayerBait getPlayerBait(OfflinePlayer player);
	
	public final boolean useBait(Player player, String bait) {
		final GameManager gameManager = plugin.getGameManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		
		if (player != null && bait != null) {	
			final BaitProperties baitProperties = baitManager.getBaitProperties(bait);
			
			if (baitProperties != null) {
				final ItemRequirement requirement = baitProperties.getRequirement();
				
				if (requirement.isAllowed(player)) {
					final ItemStack item = baitProperties.getItem();
					
					if (PlayerUtil.hasItem(player, item)) {
						final PlayerBait playerBait = getPlayerBait(player);
						final String hook = playerBait.getHookBait();
						
						if (hook == null || !hook.equalsIgnoreCase(bait)) {
							if (hook != null && !hook.equalsIgnoreCase(bait)) {
								final BaitProperties baitPropertiesHook = baitManager.getBaitProperties(hook);
								
								if (baitPropertiesHook != null) {
									final ItemStack itemRetrieval = baitPropertiesHook.getItem().clone();
									
									PlayerUtil.addItem(player, itemRetrieval);
								}
							}
							
							playerBait.setMarkBait(bait);
							playerBait.setHookBait(bait);
							PlayerUtil.removeItem(player, item);
							return true;
						}
					}
				}
			}
		}
		
		return false;
	}
	
	public final boolean autoHookBait(Player player) {
		final GameManager gameManager = plugin.getGameManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		
		if (player != null) {
			final PlayerBait playerBait = getPlayerBait(player);
			final String hook = playerBait.getHookBait();
			
			if (hook == null) {
				for (BaitProperties baitProperties : baitManager.getAllBaitProperties()) {
					final ItemStack item = baitProperties.getItem();
					
					if (PlayerUtil.hasItem(player, item)) {
						final String bait = baitProperties.getId();
						
						if (useBait(player, bait)) {
							final MessageBuild message = Language.FISHING_BAIT_AUTO_HOOK.getMessage(player);
							
							message.sendMessage(player, "bait", bait);
							return true;
						}
					}
				}
			}
		}
		
		return false;
	}
}
