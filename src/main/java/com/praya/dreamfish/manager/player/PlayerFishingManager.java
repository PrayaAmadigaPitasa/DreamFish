package com.praya.dreamfish.manager.player;

import java.util.Collection;
import java.util.UUID;

import org.bukkit.OfflinePlayer;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;
import com.praya.dreamfish.player.PlayerFishing;

public abstract class PlayerFishingManager extends HandlerManager {
	
	protected PlayerFishingManager(DreamFish plugin) {
		super(plugin);
	}
	
	public abstract void setupPlayerFishingDatabase();
	public abstract Collection<UUID> getPlayerIds();
	public abstract Collection<PlayerFishing> getAllPlayerFishing();
	public abstract PlayerFishing getPlayerFishing(OfflinePlayer player);
	public abstract PlayerFishing getPlayerFishing(OfflinePlayer player, boolean generate);
	public abstract boolean removeFromCache(OfflinePlayer player);	
}