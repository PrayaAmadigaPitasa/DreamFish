package com.praya.dreamfish.manager.player;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.bait.BaitFishing;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.handler.HandlerManager;
import com.praya.dreamfish.item.ItemRequirement;
import com.praya.dreamfish.manager.game.BaitManager;
import com.praya.dreamfish.manager.game.FishManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.player.PlayerFishingMode;

import api.praya.agarthalib.builder.support.SupportMyItems;
import api.praya.agarthalib.builder.support.SupportWorldGuard;
import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;
import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.Slot;

public abstract class PlayerFishingModeManager extends HandlerManager {
	
	protected PlayerFishingModeManager(DreamFish plugin) {
		super(plugin);
	};
	
	public abstract Collection<UUID> getPlayerIds();
	public abstract Collection<PlayerFishingMode> getAllPlayerFishingMode();
	public abstract PlayerFishingMode getPlayerFishingMode(Player player);
	
	public final boolean isFishing(Player player) {
		return getPlayerFishingMode(player) != null;
	}
	
	public final String getRandomFish(Player player, Entity hook, String bait) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManager = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final GameManager gameManager = plugin.getGameManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		final FishManager fishManager = gameManager.getFishManager();
		
		if (player != null && hook != null && bait != null) {
			final Biome biome = hook.getLocation().getBlock().getBiome();
			final Location location = hook.getLocation();
			final World world = location.getWorld();
			final SupportWorldGuard supportWorldGuard = supportManager.getSupportWorldGuard();
			final BaitProperties baitProperties = baitManager.getBaitProperties(bait);
			final BaitFishing baitFishing = baitProperties.getBaitFishing();
			final HashMap<String, Double> minPossibility = new HashMap<String, Double>();
			final HashMap<String, Double> maxPossibility = new HashMap<String, Double>();
			
			double sum = 0;
			
			for (String fish : baitFishing.getFishes()) {
				final FishProperties fishProperties = fishManager.getFishProperties(fish);
				
				if (fishProperties != null) {
					final ItemRequirement requirement = fishProperties.getRequirement();
					
					if (requirement.isAllowed(player)) {
						final List<Biome> biomes = fishProperties.getBiomes();
						final List<String> regions = fishProperties.getRegions();
						
						boolean allowRegion = supportWorldGuard != null ? false : true;
						boolean allowBiome = biomes.isEmpty() || biomes.contains(biome);
						
						if (supportWorldGuard != null) {
							if (regions.isEmpty()) {
								allowRegion = true;
							} else {
								for (String region : regions) {
									if (supportWorldGuard.isLocationInsideRegion(world, region, location)) {
										allowRegion = true;
									}
								}
							}
						}
						
						if (allowRegion && allowBiome) {
							final double fishPossibility = baitFishing.getFishPossibility(fish);
							
							minPossibility.put(fish, sum);
							
							sum = sum + fishPossibility;
							
							maxPossibility.put(fish, sum);
						}
					}
				}
			}
			
			final double number = Math.random() * sum;
			
			for (String key : baitFishing.getFishes()) {
				if (minPossibility.containsKey(key)) {
					if (number >= minPossibility.get(key) && number <= maxPossibility.get(key)){
						return key;
					}
				}
			}
		}
		
		return null;
	}
	
	public final Slot getActiveSlot(Player player) {
		if (player != null) {
			final ItemStack mainHand = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			final ItemStack offHand = Bridge.getBridgeEquipment().getEquipment(player, Slot.OFFHAND);
			
			if (mainHand != null && mainHand.getType().equals(Material.FISHING_ROD)) {
				return Slot.MAINHAND;
			} else if (offHand != null && offHand.getType().equals(Material.FISHING_ROD)) {
				return Slot.OFFHAND;
			} else {
				return null;
			}
		}
		
		return null;
	}
	
	public final double getPlayerFishingMultiplier(Player player) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final SupportMyItems supportMyItems = supportManagerAPI.getSupportMyItems();
		
		try {
			if (supportMyItems != null && player != null) {
				final Slot activeSlot = getActiveSlot(player);
				
				if (activeSlot != null) {
					final String loreStats = "FISHING_CHANCE";
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, activeSlot);
					
					if (supportMyItems.hasLoreStats(item, loreStats)) {
						final double multiplier = supportMyItems.getStatsValue(item, loreStats);
						
						return multiplier;
					}
				}
			}
		} catch (NoClassDefFoundError exception) {
			// Silent
		}
		
		return 1;
	}
	
	public final double getPlayerFishingPower(Player player) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final SupportMyItems supportMyItems = supportManagerAPI.getSupportMyItems();
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		final double pullBasePower = mainConfig.getRodDefaultPullPower();
		
		try {
			if (supportMyItems != null && player != null) {
				final Slot activeSlot = getActiveSlot(player);
				
				if (activeSlot != null) {
					final String loreStats = "FISHING_POWER";
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, activeSlot);
					
					if (supportMyItems.hasLoreStats(item, loreStats)) {
						final double pullRodPower = supportMyItems.getStatsValue(item, loreStats);
						
						return pullBasePower + pullRodPower;
					}
				}
			} 
		} catch (NoClassDefFoundError exception) {
			// Silent
		}
		
		return pullBasePower;
	}
	
	public final double getPlayerFishingSpeedMultiplier(Player player) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final SupportMyItems supportMyItems = supportManagerAPI.getSupportMyItems();
		
		try {
			if (supportMyItems != null && player != null) {
				final Slot activeSlot = getActiveSlot(player);
				
				if (activeSlot != null) {
					final String loreStats = "FISHING_SPEED";
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, activeSlot);
					
					if (supportMyItems.hasLoreStats(item, loreStats)) {
						final double rodFishingSpeed = supportMyItems.getStatsValue(item, loreStats);
						
						return 100 + rodFishingSpeed;
					}
				}
			} 
		} catch (NoClassDefFoundError exception) {
			// Silent
		}
		
		return 100;
	}
	
	public final double getPlayerLuresMaxTension(Player player) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final SupportMyItems supportMyItems = supportManagerAPI.getSupportMyItems();
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		final double baseMaxTension = mainConfig.getRodDefaultLuresMaxTension();
		
		try {
			if (supportMyItems != null && player != null) {
				final Slot activeSlot = getActiveSlot(player);
				
				if (activeSlot != null) {
					final String loreStats = "LURES_MAX_TENSION";
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, activeSlot);
					
					if (supportMyItems.hasLoreStats(item, loreStats)) {
						final double rodMaxTension = supportMyItems.getStatsValue(item, loreStats);
						
						return baseMaxTension + rodMaxTension;
					}
				}
			}
		} catch (NoClassDefFoundError exception) {
			// Silent
		}
		
		return baseMaxTension;
	}
	
	public final double getPlayerLuresEndurance(Player player) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final SupportMyItems supportMyItems = supportManagerAPI.getSupportMyItems();
		
		try {
			if (supportMyItems != null && player != null) {
				final Slot activeSlot = getActiveSlot(player);
				
				if (activeSlot != null) {
					final String loreStats = "LURES_ENDURANCE";
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, activeSlot);
					
					if (supportMyItems.hasLoreStats(item, loreStats)) {
						final double rodEndurance = supportMyItems.getStatsValue(item, loreStats);
						
						return 100 + rodEndurance;
					}
				}
			}
		} catch (NoClassDefFoundError exception) {
			// Silent
		}
		
		return 100;
	}
}
