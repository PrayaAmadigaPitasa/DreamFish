package com.praya.dreamfish.manager.player;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;

public abstract class PlayerManager extends HandlerManager {
	
	protected PlayerManager(DreamFish plugin) {
		super(plugin);
		
	}
	
	public abstract PlayerBaitManager getPlayerBaitManager();
	public abstract PlayerFishingManager getPlayerFishingManager();
	public abstract PlayerFishingModeManager getPlayerFishingModeManager();
}
