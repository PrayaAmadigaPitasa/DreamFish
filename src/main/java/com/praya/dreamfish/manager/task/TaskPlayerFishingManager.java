package com.praya.dreamfish.manager.task;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;

public abstract class TaskPlayerFishingManager extends HandlerManager {
	
	protected TaskPlayerFishingManager(DreamFish plugin) {
		super(plugin);
	}

	public abstract void reloadTaskPlayerFishingDatabase();
}