package com.praya.dreamfish.manager.task;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerManager;

public abstract class TaskManager extends HandlerManager {
	
	protected TaskManager(DreamFish plugin) {
		super(plugin);
	}
	
	public abstract TaskPlayerFishingManager getTaskPlayerFishingManager();
}