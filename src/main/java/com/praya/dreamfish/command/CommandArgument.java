package com.praya.dreamfish.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.plugin.PlaceholderManager;
import com.praya.dreamfish.manager.plugin.PluginManager;

public abstract class CommandArgument {

	private final Plugin plugin;
	private final String mainArgument;
	private final String permission;
	private final List<String> aliases;
	
	protected CommandArgument(Plugin plugin, String mainArgument) {
		this(plugin, mainArgument, null);
	}
	
	protected CommandArgument(Plugin plugin, String mainArgument, String permission) {
		this(plugin, mainArgument, permission, null);
	}
	
	protected CommandArgument(Plugin plugin, String mainArgument, String permission, List<String> aliases) {
		if (plugin == null || mainArgument == null) {
			throw new IllegalArgumentException();
		} else {
			this.plugin = plugin;
			this.mainArgument = mainArgument;
			this.permission = permission;
			this.aliases = aliases != null ? aliases : new ArrayList<String>();
		}
	}
	
	public abstract void execute(CommandSender sender, String[] args);
	
	public final Plugin getPlugin() {
		return this.plugin;
	}
	
	public final String getMainArgument() {
		return this.mainArgument;
	}
	
	public final String getPermission() {
		return this.permission;
	}
	
	public final List<String> getAliases() {
		return this.aliases;
	}
	
	public final CommandTree getCommandTree() {
		final CommandTreeMemory commandTreeMemory = CommandTreeMemory.getInstance();
		
		for (CommandTree commandTree : commandTreeMemory.getAllCommandTree()) {
			if (commandTree.getAllCommandArgument().contains(this)) {
				return commandTree;
			}
		}
		
		return null;
	}
	
	public String getDescription(CommandSender sender) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final String description = placeholderManager.getPlaceholder("none");
		
		return description;
	}
}