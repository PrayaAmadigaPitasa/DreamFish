package com.praya.dreamfish.command.dreamfish;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.command.CommandArgument;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.manager.game.MenuManager;
import com.praya.dreamfish.menu.MenuDreamFish;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;

public final class CommandDreamFishMenu extends CommandArgument {

	private static final Command COMMAND = Command.DREAMFISH_MENU;
	
	protected CommandDreamFishMenu(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_DREAMFISH_MENU.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final GameManager gameManager = plugin.getGameManager();
		final MenuManager menuManager = gameManager.getMenuManager();
		
		if (!SenderUtil.isPlayer(sender)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player player = PlayerUtil.parse(sender);
			final MenuDreamFish menuDreamFish = menuManager.getMenuDreamFish();
			final String menu = args.length > 1 ? args[1] : "Home";
			
			if (menu.equalsIgnoreCase("Home")) {
				menuDreamFish.openMenuHome(player);
				return;
			} else if (menu.equalsIgnoreCase("Player")) {
				menuDreamFish.openMenuPlayer(player);
				return;
			} else if (menu.equalsIgnoreCase("Progress")) {
				menuDreamFish.openMenuProgress(player);
				return;
			} else if (menu.equalsIgnoreCase("Progress_Bait")) {
				menuDreamFish.openMenuProgressBait(player);
				return;
			} else if (menu.equalsIgnoreCase("Progress_Fish")) {
				menuDreamFish.openMenuProgressFish(player);
				return;
			} else if (menu.equalsIgnoreCase("Shop")) {
				menuDreamFish.openMenuProgressFish(player);
				return;
			} else if (menu.equalsIgnoreCase("Shop_Buy_Bait")) {
				menuDreamFish.openMenuBaitBuy(player);
				return;
			} else if (menu.equalsIgnoreCase("Shop_Sell_Bait")) {
				menuDreamFish.openMenuBaitSell(player);
				return;
			} else if (menu.equalsIgnoreCase("Shop_Sell_Fish")) {
				menuDreamFish.openMenuFishSell(player);
				return;
			} else if (menu.equalsIgnoreCase("Guide")) {
				menuDreamFish.openMenuGuide(player);
				return;
			} else {
				final MessageBuild message = Language.ARGUMENT_MENU_NOT_EXIST.getMessage(sender);
				
				message.sendMessage(sender, "menu", menu);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			}
		}
	}
}
