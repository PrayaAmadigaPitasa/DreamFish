package com.praya.dreamfish.command.dreamfish;

import java.util.HashMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.command.CommandArgument;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.game.FishManager;
import com.praya.dreamfish.manager.game.GameManager;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;

public final class CommandDreamFishLoad extends CommandArgument {

	private static final Command COMMAND = Command.DREAMFISH_LOAD;
	
	protected CommandDreamFishLoad(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_DREAMFISH_LOAD.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final GameManager gameManager = plugin.getGameManager();
		final FishManager fishManager = gameManager.getFishManager();
		
		if (args.length < (SenderUtil.isPlayer(sender) ? 2 : 3)) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_DREAMFISH_LOAD.getText(sender));
			final MessageBuild message = Language.ARGUMENT_DREAMFISH_LOAD.getMessage(sender);
			
			message.sendMessage(sender, "tooltip_load", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final FishProperties fishProperties = fishManager.getFishProperties(args[1]);
			
			if (fishProperties == null) {
				final MessageBuild message = Language.ITEM_NOT_EXIST.getMessage(sender);
				
				message.sendMessage(sender, "nameid", args[1]);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final String fish = fishProperties.getId();
				final ItemStack item = fishProperties.getItem().clone();
				
				Player target;
				int amount;
				
				if (args.length > 2) {
					final String nameTarget = args[2];
					
					if (!PlayerUtil.isOnline(nameTarget)) {
						final MessageBuild message = Language.PLAYER_TARGET_OFFLINE.getMessage(sender);
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else {
						target = PlayerUtil.getOnlinePlayer(nameTarget);
					}
				} else {
					target = PlayerUtil.parse(sender);
				}
				
				if (args.length > 3) {
					final String textAmount = args[3];
					
					if (!MathUtil.isNumber(textAmount)) {
						final MessageBuild message = Language.ARGUMENT_INVALID_VALUE.getMessage(sender);
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else {
						amount = MathUtil.parseInteger(textAmount);
						amount = MathUtil.limitInteger(amount, 1, amount);
					}
				} else {
					amount = 1;
				}
				
				if (target.equals(sender)) {
					final MessageBuild message = Language.COMMAND_DREAMFISH_LOAD_SUCCESS_SELF.getMessage(sender);
					final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
											
					mapPlaceholder.put("amount", String.valueOf(amount));
					mapPlaceholder.put("nameid", fish);
					
					item.setAmount(amount);
					target.getInventory().addItem(item);
					message.sendMessage(sender, mapPlaceholder);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
					target.updateInventory();
					return;
				} else {
					final MessageBuild messageToSender = Language.COMMAND_DREAMFISH_LOAD_SUCCESS_TO_SENDER.getMessage(sender);
					final MessageBuild messageToTarget = Language.COMMAND_DREAMFISH_LOAD_SUCCESS_TO_TARGET.getMessage(sender);
					final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
					
					mapPlaceholder.put("nameid", fish);
					mapPlaceholder.put("amount", String.valueOf(amount));
					mapPlaceholder.put("target", target.getName());
					mapPlaceholder.put("sender", sender.getName());
					
					item.setAmount(amount);
					target.getInventory().addItem(item);
					messageToSender.sendMessage(sender, mapPlaceholder);
					messageToTarget.sendMessage(target, mapPlaceholder);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
					SenderUtil.playSound(target, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);	
					target.updateInventory();
					return;
				}
			}
		}
	}
}
