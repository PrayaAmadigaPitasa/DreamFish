package com.praya.dreamfish.command.dreamfish;

import java.util.HashMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.command.CommandArgument;
import com.praya.dreamfish.event.PlayerFishingExpChangeEvent.ExpChangeReason;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerFishing;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;

public final class CommandDreamFishExp extends CommandArgument {

	private static final Command COMMAND = Command.DREAMFISH_EXP;
	
	protected CommandDreamFishExp(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_DREAMFISH_EXP.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		
		if (args.length < (sender instanceof Player ? 3 : 4)) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_DREAMFISH_EXP.getText(sender));
			final MessageBuild message = Language.ARGUMENT_DREAMFISH_EXP.getMessage(sender);
			
			message.sendMessage(sender, "tooltip_exp", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player target;
			
			if (args.length > 3) {
				final String textTarget = args[3];
				
				target = PlayerUtil.getOnlinePlayer(textTarget);
			} else {
				target = PlayerUtil.parse(sender);
			}
			
			if (target == null) {
				final MessageBuild message = Language.PLAYER_TARGET_OFFLINE.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final PlayerFishing playerFishing = playerFishingManager.getPlayerFishing(target);
				
				if (playerFishing == null) {
					final MessageBuild message = Language.ARGUMENT_DATABASE_PLAYER_NOT_REGISTERED.getMessage(sender);
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final String textValue = args[2];
					
					if (!MathUtil.isNumber(textValue)) {
						final MessageBuild message = Language.ARGUMENT_INVALID_VALUE.getMessage(sender);
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else {
						final String action = args[1];
						final double value = MathUtil.parseDouble(textValue);
						final double playerExp = playerFishing.getExp();
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						final MessageBuild messageTarget;
						final MessageBuild messageSender;
						final double exp;
						
						if (action.equalsIgnoreCase("Set")) {
							exp = Math.max(0, value);
							messageTarget = Language.COMMAND_DREAMFISH_EXP_SET_SUCCESS_TO_TARGET.getMessage(sender);
							messageSender = Language.COMMAND_DREAMFISH_EXP_SET_SUCCESS_TO_SENDER.getMessage(sender);
							
							playerFishing.setExp((float) exp, ExpChangeReason.COMMAND);
						} else if (action.equalsIgnoreCase("Add")) {
							exp = Math.max(0, value);
							messageTarget = Language.COMMAND_DREAMFISH_EXP_ADD_SUCCESS_TO_TARGET.getMessage(sender);
							messageSender = Language.COMMAND_DREAMFISH_EXP_ADD_SUCCESS_TO_SENDER.getMessage(sender);
							
							playerFishing.addExp((float) exp, ExpChangeReason.COMMAND);
						}  else if (action.equalsIgnoreCase("Take")) {
							exp = MathUtil.limitDouble(value, 0, playerExp);
							messageTarget = Language.COMMAND_DREAMFISH_EXP_TAKE_SUCCESS_TO_TARGET.getMessage(sender);
							messageSender = Language.COMMAND_DREAMFISH_EXP_TAKE_SUCCESS_TO_SENDER.getMessage(sender);
							
							playerFishing.takeExp((float) exp, ExpChangeReason.COMMAND);
						} else {
							final MessageBuild message = Language.ARGUMENT_INVALID_COMMAND.getMessage(sender);
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return;
						}
						
						mapPlaceholder.put("sender", sender.getName());
						mapPlaceholder.put("target", target.getName());
						mapPlaceholder.put("exp", String.valueOf(exp));
						
						if (!sender.equals(target)) {
							messageSender.sendMessage(sender, mapPlaceholder);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						}
						
						playerFishing.save();
						messageTarget.sendMessage(target, mapPlaceholder);
						SenderUtil.playSound(target, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						return;
					}
				}
			}
		}
	}
}
