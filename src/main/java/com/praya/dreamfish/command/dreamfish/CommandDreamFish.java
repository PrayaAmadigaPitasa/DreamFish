package com.praya.dreamfish.command.dreamfish;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.CommandArgument;
import com.praya.dreamfish.command.CommandTree;

public final class CommandDreamFish extends CommandTree {

	private static final String COMMAND = "DreamFish";
	private static final String DEFAULT_ARGUMENT = "Menu";
	
	private CommandDreamFish(DreamFish plugin) {
		super(COMMAND, DEFAULT_ARGUMENT);
		
		final CommandArgument commandArgumentAbout = new CommandDreamFishAbout(plugin);
		final CommandArgument commandArgumentExp = new CommandDreamFishExp(plugin);
		final CommandArgument commandArgumentHelp = new CommandDreamFishHelp(plugin);
		final CommandArgument commandArgumentLevel = new CommandDreamFishLevel(plugin);
		final CommandArgument commandArgumentList = new CommandDreamFishList(plugin);
		final CommandArgument commandArgumentLoad = new CommandDreamFishLoad(plugin);
		final CommandArgument commandArgumentMenu = new CommandDreamFishMenu(plugin);
		final CommandArgument commandArgumentReload = new CommandDreamFishReload(plugin);
		final CommandArgument commandArgumentStats = new CommandDreamFishStats(plugin);
		
		register(commandArgumentAbout);
		register(commandArgumentExp);
		register(commandArgumentHelp);
		register(commandArgumentLevel);
		register(commandArgumentList);
		register(commandArgumentLoad);
		register(commandArgumentMenu);
		register(commandArgumentReload);
		register(commandArgumentStats);
	}
	
	private static class CommandDreamFishSingleton {
		private static final CommandDreamFish INSTANCE;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			INSTANCE = new CommandDreamFish(plugin);
		}
	}
	
	public static final CommandDreamFish getInstance() {
		return CommandDreamFishSingleton.INSTANCE;
	}
}