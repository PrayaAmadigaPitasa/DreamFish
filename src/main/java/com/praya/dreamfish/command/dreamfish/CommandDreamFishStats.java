package com.praya.dreamfish.command.dreamfish;

import java.util.HashMap;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.command.CommandArgument;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerFishing;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;

public class CommandDreamFishStats extends CommandArgument {

	private static final Command COMMAND = Command.DREAMFISH_STATS;
	
	protected CommandDreamFishStats(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_DREAMFISH_STATS.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		
		if (!(sender instanceof Player) && args.length < 2) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_DREAMFISH_STATS.getText(sender));
			final MessageBuild message = Language.ARGUMENT_DREAMFISH_STATS.getMessage(sender);
			
			message.sendMessage(sender, "tooltip_stats", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final OfflinePlayer player;
			
			if (args.length > 1) {
				final String textPlayer = args[1];
				
				player = PlayerUtil.getPlayer(textPlayer);
				
				if (player == null) {
					final MessageBuild message = Language.ARGUMENT_CATEGORY_NOT_EXISTS.getMessage(sender);
					
					message.sendMessage(sender, "player", textPlayer);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				}
			} else {
				player = (OfflinePlayer) sender; 
			}
			
			final PlayerFishing playerFishing = playerFishingManager.getPlayerFishing(player);
			
			if (playerFishing == null) {
				final MessageBuild message = Language.ARGUMENT_DATABASE_PLAYER_NOT_REGISTERED.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final MessageBuild message = Language.FISHING_STATS.getMessage(sender);
				final String playerName = player.getName();
				final int playerLevel = playerFishing.getLevel();
				final double bonusEffectiveness = playerFishing.getBonusEffectiveness();
				final double bonusEndurance = playerFishing.getBonusEndurance();
				final double bonusSpeed = playerFishing.getBonusSpeed();
				final float playerExp = playerFishing.getExp();
				final float playerExpUp = playerFishing.getExpToUp();
				final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
				
				mapPlaceholder.put("player_name", playerName);
				mapPlaceholder.put("player_level", String.valueOf(playerLevel));
				mapPlaceholder.put("player_fishing_bonus_effectiveness", String.valueOf(MathUtil.roundNumber(bonusEffectiveness)));
				mapPlaceholder.put("player_fishing_bonus_endurance", String.valueOf(MathUtil.roundNumber(bonusEndurance)));
				mapPlaceholder.put("player_fishing_bonus_speed", String.valueOf(MathUtil.roundNumber(bonusSpeed)));
				mapPlaceholder.put("player_exp", String.valueOf(MathUtil.roundNumber(playerExp, 1)));
				mapPlaceholder.put("player_exp_up", String.valueOf(MathUtil.roundNumber(playerExpUp, 1)));
				
				message.sendMessage(sender, mapPlaceholder);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
				return;
			}
		}
	}
}
