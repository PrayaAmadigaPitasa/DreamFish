package com.praya.dreamfish.command.dreamfish;

import com.praya.dreamfish.treasure.TreasureConfig;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.bait.BaitConfig;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.command.CommandArgument;
import com.praya.dreamfish.command.CommandConfig;
import com.praya.dreamfish.fish.FishConfig;
import com.praya.dreamfish.handler.Handler;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.language.LanguageConfig;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.manager.task.TaskManager;
import com.praya.dreamfish.placeholder.PlaceholderConfig;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;

public final class CommandDreamFishReload extends CommandArgument {

	private static final Command COMMAND = Command.DREAMFISH_RELOAD;
	
	protected CommandDreamFishReload(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_DREAMFISH_RELOAD.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final TaskManager taskManager = plugin.getTaskManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final MessageBuild message = Language.COMMAND_RELOAD_SUCCESS.getMessage(sender);
		final PlaceholderConfig placeholderConfig = Handler.getHandler(PlaceholderConfig.class);
		final LanguageConfig languageConfig = Handler.getHandler(LanguageConfig.class);
		final CommandConfig commandConfig = Handler.getHandler(CommandConfig.class);
		final BaitConfig baitConfig = Handler.getHandler(BaitConfig.class);
		final FishConfig fishConfig = Handler.getHandler(FishConfig.class);
		final TreasureConfig treasureConfig = Handler.getHandler(TreasureConfig.class);
		
		mainConfig.setup();
		placeholderConfig.setup();
		languageConfig.setup();
		commandConfig.setup();
		
		baitConfig.setup();
		fishConfig.setup();
		treasureConfig.setup();

		playerFishingManager.setupPlayerFishingDatabase();
		
		taskManager.getTaskPlayerFishingManager().reloadTaskPlayerFishingDatabase();
		
		message.sendMessage(sender);
		SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
		return;
	}
}
