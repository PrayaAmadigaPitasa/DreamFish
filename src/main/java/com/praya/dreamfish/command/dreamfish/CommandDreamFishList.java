package com.praya.dreamfish.command.dreamfish;

import java.util.HashMap;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.JsonUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.SortUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.command.CommandArgument;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.game.FishManager;
import com.praya.dreamfish.manager.game.GameManager;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;

public final class CommandDreamFishList extends CommandArgument {

	private static final Command COMMAND = Command.DREAMFISH_LIST;
	
	protected CommandDreamFishList(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_DREAMFISH_LIST.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final GameManager gameManager = plugin.getGameManager();
		final FishManager fishManager = gameManager.getFishManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		
		if (fishManager.getFishPropertiesIds().isEmpty()) {
			final MessageBuild message = Language.ITEM_DATABASE_EMPTY.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
			return;
		} else {
			final List<String> keyList = SortUtil.toList(fishManager.getFishPropertiesIds());
			final int size = keyList.size();
			final int maxRow = mainConfig.getListContent();
			final int maxPage = size % maxRow == 0 ? size / maxRow : (size / maxRow) + 1;
			
			int page = 1;
			
			if (args.length > 1) {
				final String textPage = args[1];
				
				if (MathUtil.isNumber(textPage)) {
					page = MathUtil.parseInteger(textPage);
					page = MathUtil.limitInteger(page, 1, maxPage);
				}
			}
			
			final String codeTooltip = mainConfig.getUtilityTooltip();
			final MessageBuild messageHeader = Language.LIST_HEADER.getMessage(sender);
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			mapPlaceholder.put("page", String.valueOf(page));
			mapPlaceholder.put("maxpage", String.valueOf(maxPage));
			
			messageHeader.sendMessage(sender, mapPlaceholder);

			final int addNum = (page-1)*maxRow;
			
			for (int t = 0; t < maxRow && (t+addNum) < size; t++) {
				final int index = t + addNum;
				final String fish = keyList.get(index);
				final FishProperties fishProperties = fishManager.getFishProperties(fish);
				final ItemStack item = fishProperties.getItem();
				final MessageBuild messageItem = Language.LIST_ITEM.getMessage(sender);
				
				mapPlaceholder.clear();
				mapPlaceholder.put("index", String.valueOf(index+1));
				mapPlaceholder.put("item", fish);
				mapPlaceholder.put("maxpage", String.valueOf(page));
				mapPlaceholder.put("tooltip", JsonUtil.generateJsonItem(codeTooltip, item));

				messageItem.sendMessage(sender, mapPlaceholder);
			}
			
			SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
			return;
		}
	}
}
