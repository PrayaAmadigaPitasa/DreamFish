package com.praya.dreamfish.command.bait;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.CommandArgument;
import com.praya.dreamfish.command.CommandTree;

public final class CommandBait extends CommandTree {

	private static final String COMMAND = "Bait";
	private static final String DEFAULT_ARGUMENT = "Use";
	
	private CommandBait(DreamFish plugin) {
		super(COMMAND, DEFAULT_ARGUMENT);
		
		final CommandArgument commandArgumentList = new CommandBaitList(plugin);
		final CommandArgument commandArgumentLoad = new CommandBaitLoad(plugin);
		final CommandArgument commandArgumentUse = new CommandBaitUse(plugin);
		
		register(commandArgumentList);
		register(commandArgumentLoad);
		register(commandArgumentUse);
	}
	
	private static class CommandDreamFishSingleton {
		private static final CommandBait INSTANCE;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			INSTANCE = new CommandBait(plugin);
		}
	}
	
	public static final CommandBait getInstance() {
		return CommandDreamFishSingleton.INSTANCE;
	}
}