package com.praya.dreamfish.command.bait;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.command.Command;
import com.praya.dreamfish.command.CommandArgument;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.game.BaitManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.manager.player.PlayerBaitManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerBait;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;

public final class CommandBaitUse extends CommandArgument {

	private static final Command COMMAND = Command.BAIT_USE;
	
	protected CommandBaitUse(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_BAIT_USE.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerBaitManager playerBaitManager = playerManager.getPlayerBaitManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		
		if (!SenderUtil.isPlayer(sender)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player player = PlayerUtil.parse(sender);
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			
			if (!EquipmentUtil.isSolid(item)) {
				final MessageBuild message = Language.ITEM_MAINHAND_EMPTY.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final BaitProperties baitProperties = baitManager.getBaitProperties(item);
				
				if (baitProperties == null) {
					final MessageBuild message = Language.COMMAND_BAIT_NOT_EXISTS.getMessage(sender);
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final String bait = baitProperties.getId();
					final PlayerBait playerBait = playerBaitManager.getPlayerBait(player);
					final String hook = playerBait.getHookBait();
					
					if (hook != null && hook.equalsIgnoreCase(bait)) {
						final MessageBuild message = Language.COMMAND_BAIT_USE_DUPLICATE.getMessage(sender);
						
						message.sendMessage(player);
						SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else if (playerBaitManager.useBait(player, bait)) {
						final MessageBuild message = Language.COMMAND_BAIT_USE_SUCCESS.getMessage(sender);
						
						message.sendMessage(player, "bait", bait);
						SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						return;
					} else {
						final MessageBuild message = Language.COMMAND_BAIT_USE_FAILED.getMessage(sender);
						
						message.sendMessage(player);
						SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					}
				}
			}
		}
	}
}
