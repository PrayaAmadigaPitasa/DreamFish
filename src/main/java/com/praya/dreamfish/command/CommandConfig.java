package com.praya.dreamfish.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import com.praya.agarthalib.utility.FileUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerConfig;

import core.praya.agarthalib.builder.command.CommandBuild;

public final class CommandConfig extends HandlerConfig {
	
	private static final String PATH_FILE = "command.yml";
	
	protected final HashMap<String, CommandBuild> mapCommandBuild = new HashMap<String, CommandBuild>(); 
	
	protected CommandConfig(DreamFish plugin) {
		super(plugin);
		
		setup();
	};
	
	@Override
	public final void setup() {
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapCommandBuild.clear();
	}
	
	private final void loadConfig() {
		final FileConfiguration config = FileUtil.getFileConfigurationResource(plugin, PATH_FILE);
		
		for (String key : config.getKeys(false)) {
			final ConfigurationSection keyDataSection = config.getConfigurationSection(key);
			
			for (String keyData : keyDataSection.getKeys(false)) {
				if (keyDataSection.isConfigurationSection(keyData)) {
					final ConfigurationSection mainDataSection = keyDataSection.getConfigurationSection(keyData);
					final String id = key + "_" + keyData;
					final List<String> aliases = new ArrayList<String>();
					
					String main = null;
					String permission = null;
					
					for (String mainData : mainDataSection.getKeys(false)) {
						if (mainData.equalsIgnoreCase("Main")) {
							main = mainDataSection.getString(mainData);
						} else if (mainData.equalsIgnoreCase("Permission")) {
							permission = mainDataSection.getString(mainData);
						} else if (mainData.equalsIgnoreCase("Aliases")) {
							if (mainDataSection.isString(mainData)) {
								aliases.add(mainDataSection.getString(mainData));
							} else if (mainDataSection.isList(mainData)) {
								aliases.addAll(mainDataSection.getStringList(mainData));
							}
						}
					}
					
					if (main != null) {
						final CommandBuild commandBuild = new CommandBuild(id, main, permission, aliases);
						
						this.mapCommandBuild.put(id, commandBuild);
					}
				}
			}
		}
	}
}
