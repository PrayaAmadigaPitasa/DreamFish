package com.praya.dreamfish;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.command.CommandMemory;
import com.praya.dreamfish.language.LanguageMemory;
import com.praya.dreamfish.manager.plugin.CommandManager;
import com.praya.dreamfish.manager.plugin.LanguageManager;
import com.praya.dreamfish.manager.plugin.MetricsManager;
import com.praya.dreamfish.manager.plugin.PlaceholderManager;
import com.praya.dreamfish.manager.plugin.PluginManager;
import com.praya.dreamfish.manager.plugin.PluginPropertiesManager;
import com.praya.dreamfish.metrics.MetricsMemory;
import com.praya.dreamfish.placeholder.PlaceholderMemory;
import com.praya.dreamfish.plugin.PluginPropertiesMemory;

public final class DreamFishPluginMemory extends PluginManager {

	private final CommandManager commandManager;
	private final LanguageManager languageManager;
	private final PlaceholderManager placeholderManager;
	private final PluginPropertiesManager pluginPropertiesManager;
	private final MetricsManager metricsManager;
	
	protected DreamFishPluginMemory(DreamFish plugin) {
		super(plugin);
		
		this.commandManager = CommandMemory.getInstance();
		this.placeholderManager = PlaceholderMemory.getInstance();
		this.languageManager = LanguageMemory.getInstance();
		this.pluginPropertiesManager = PluginPropertiesMemory.getInstance();
		this.metricsManager = MetricsMemory.getInstance();
	}
	
	public final LanguageManager getLanguageManager() {
		return this.languageManager;
	}
	
	public final PlaceholderManager getPlaceholderManager() {
		return this.placeholderManager;
	}
	
	public final PluginPropertiesManager getPluginPropertiesManager() {
		return this.pluginPropertiesManager;
	}
	
	public final MetricsManager getMetricsManager() {
		return this.metricsManager;
	}
	
	public final CommandManager getCommandManager() {
		return this.commandManager;
	}
}
