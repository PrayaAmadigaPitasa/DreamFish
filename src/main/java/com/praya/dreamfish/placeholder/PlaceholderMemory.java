package com.praya.dreamfish.placeholder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.plugin.PlaceholderManager;

public final class PlaceholderMemory extends PlaceholderManager {
	
	private final PlaceholderConfig placeholderConfig;
	
	private PlaceholderMemory(DreamFish plugin) {
		super(plugin);
		
		this.placeholderConfig = new PlaceholderConfig(plugin);
	};
	
	private static class PlaceholderMemorySingleton {
		private static final PlaceholderMemory instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			instance = new PlaceholderMemory(plugin);
		}
	}
	
	public static final PlaceholderMemory getInstance() {
		return PlaceholderMemorySingleton.instance;
	}
	
	public final PlaceholderConfig getPlaceholderConfig() {
		return this.placeholderConfig;
	}
	
	@Override
	protected final HashMap<String, String> getMapPlaceholder() {
		return getPlaceholderConfig().mapPlaceholder;
	}
	
	@Override
	public final Collection<String> getPlaceholderIds() {
		return getPlaceholders(false);
	}
	
	protected final Collection<String> getPlaceholderIds(boolean clone) {
		final Collection<String> placeholderIds = getPlaceholderConfig().mapPlaceholder.keySet();
		
		return clone ? new ArrayList<String>(placeholderIds) : placeholderIds;
	}
	
	@Override
	public final Collection<String> getPlaceholders() {
		return getPlaceholders(true);
	}
	
	protected final Collection<String> getPlaceholders(boolean clone) {
		final Collection<String> placeholders = getPlaceholderConfig().mapPlaceholder.values();
		
		return clone ? new ArrayList<String>(placeholders) : placeholders;
	}
	
	@Override
	public final String getPlaceholder(String id) {
		for (String key : getPlaceholderIds(false)) {
			if (key.equalsIgnoreCase(id)) {
				return getPlaceholderConfig().mapPlaceholder.get(id);
			}
		}
		
		return null;
	}
}
	