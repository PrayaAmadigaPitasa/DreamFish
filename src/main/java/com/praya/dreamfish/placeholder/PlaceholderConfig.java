package com.praya.dreamfish.placeholder;

import java.io.File;
import java.util.HashMap;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import com.praya.agarthalib.utility.FileUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerConfig;

public final class PlaceholderConfig extends HandlerConfig {
	
	private static final String PATH_FILE = "Configuration/placeholder.yml";
	
	protected final HashMap<String, String> mapPlaceholder = new HashMap<String, String>(); 
	
	protected PlaceholderConfig(DreamFish plugin) {
		super(plugin);
		
		setup();
	};
	
	public final void setup() {
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapPlaceholder.clear();
	}
	
	private final void loadConfig() {
		final File file = FileUtil.getFile(plugin, PATH_FILE);
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration config = FileUtil.getFileConfiguration(file);
		
		for (String key : config.getKeys(false)) {
			if (key.equalsIgnoreCase("Placeholder")) {
				final ConfigurationSection placeholderSection = config.getConfigurationSection(key);
				
				for (String placeholder : placeholderSection.getKeys(false)) {
					mapPlaceholder.put(placeholder, placeholderSection.getString(placeholder));
				}
			}
		}
	}
}
