package com.praya.dreamfish.placeholder.replacer;

import org.bukkit.entity.Player;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.plugin.PlaceholderManager;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import be.maximvdw.placeholderapi.PlaceholderReplaceEvent;
import be.maximvdw.placeholderapi.PlaceholderReplacer;

public class ReplacerMVDWPlaceholderAPI {

	private final DreamFish plugin;
	private final String placeholder;
	
	public ReplacerMVDWPlaceholderAPI(DreamFish plugin, String placeholder) {
		this.plugin = plugin;
		this.placeholder = placeholder;
	}
	
	public final String getPlaceholder() {
		return this.placeholder;
	}
	
	public final void register() {
		final String identifier = getPlaceholder() + "_*";
		final PlaceholderReplacer placeholderReplacer = new PlaceholderReplacerMVDW();
		
		PlaceholderAPI.registerPlaceholder(plugin, identifier, placeholderReplacer);
	}
	
	private class PlaceholderReplacerMVDW implements PlaceholderReplacer {

		@Override
		public String onPlaceholderReplace(PlaceholderReplaceEvent event) {
			final PlaceholderManager placeholderManager = plugin.getPluginManager().getPlaceholderManager();
			final Player player = event.getPlayer();
			final String placeholder = event.getPlaceholder();
			final String identifier = placeholder.split(getPlaceholder() + "_")[1];
			
			return placeholderManager.getReplacement(player, identifier);
		}
		
	}
}
