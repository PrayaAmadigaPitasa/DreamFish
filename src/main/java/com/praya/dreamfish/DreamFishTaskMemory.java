package com.praya.dreamfish;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.task.TaskManager;
import com.praya.dreamfish.manager.task.TaskPlayerFishingManager;
import com.praya.dreamfish.player.PlayerFishingTaskMemory;

public class DreamFishTaskMemory extends TaskManager {

	private final TaskPlayerFishingManager taskPlayerFishingManager;
	
	protected DreamFishTaskMemory(DreamFish plugin) {
		super(plugin);
		
		this.taskPlayerFishingManager = PlayerFishingTaskMemory.getInstance();
	}
	
	public final TaskPlayerFishingManager getTaskPlayerFishingManager() {
		return this.taskPlayerFishingManager;
	}
}