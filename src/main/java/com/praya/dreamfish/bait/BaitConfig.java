package com.praya.dreamfish.bait;

import java.io.File;
import java.util.HashMap;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import com.praya.agarthalib.utility.ConfigUtil;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.FileUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerConfig;
import com.praya.dreamfish.item.ItemRequirement;

public final class BaitConfig extends HandlerConfig {
	
	private static final String PATH_FILE = "Configuration/bait.yml";
	
	protected final HashMap<String, BaitProperties> mapBaitProperties = new HashMap<String, BaitProperties>();
	
	protected BaitConfig(DreamFish plugin) {
		super(plugin);
		
		moveOldFile();
		setup();
	}
	
	@Override
	public final void setup() {
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapBaitProperties.clear();
	}
	
	private final void loadConfig() {
		final File file = FileUtil.getFile(plugin, PATH_FILE);		
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration config = FileUtil.getFileConfiguration(file);
		
		for (String key : config.getKeys(false)) {
			final ConfigurationSection itemDataSection = config.getConfigurationSection(key);
			final ItemStack item = ConfigUtil.getItemStack(itemDataSection);
			final ItemRequirement requirement = new ItemRequirement();
			
			boolean buyable = true;
			double price = 0;
			
			for (String itemData : itemDataSection.getKeys(false)) {
				if (itemData.equalsIgnoreCase("Buyable")) {
					buyable =  itemDataSection.getBoolean(itemData);
				} else if (itemData.equalsIgnoreCase("Price")) {
					price = itemDataSection.getDouble(itemData);
				} else if (itemData.equalsIgnoreCase("Requirement")) {
					final ConfigurationSection requirementDataSection = itemDataSection.getConfigurationSection(itemData);
					
					for (String requirementData : requirementDataSection.getKeys(false)) {
						if (requirementData.equalsIgnoreCase("Permission")) {
							final String permission = requirementDataSection.getString(requirementData);
							
							requirement.setPermission(permission);
						} else if (requirementData.equalsIgnoreCase("Level")) {
							final int level = requirementDataSection.getInt(requirementData);
							
							requirement.setLevel(level);;
						}
					}
				}
			}
			
			if (EquipmentUtil.isSolid(item)) {
				
				MathUtil.limitDouble(price, 0, price);
				
				final BaitProperties buildProperties = new BaitProperties(key, item, buyable, price, requirement);
				
				this.mapBaitProperties.put(key, buildProperties);
			}
		}
	}
	
	private final void moveOldFile() {
		final String pathSource = "bait.yml";
		final String pathTarget = PATH_FILE;
		final File fileSource = FileUtil.getFile(plugin, pathSource);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSource.exists()) {
			FileUtil.moveFileSilent(fileSource, fileTarget);
		}
	}
}