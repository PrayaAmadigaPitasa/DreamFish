package com.praya.dreamfish.bait;

import java.util.Collection;
import java.util.HashMap;

public class BaitFishing {

	private HashMap<String, BaitFishingProperties> mapBaitFishingProperties;
	
	protected BaitFishing() {
		this(null);
	}
	
	protected BaitFishing(HashMap<String, BaitFishingProperties> mapBaitFishingProperties) {
		this.mapBaitFishingProperties = mapBaitFishingProperties != null ? mapBaitFishingProperties : new HashMap<String, BaitFishingProperties>();
	}
	
	public final Collection<String> getFishes() {
		return this.mapBaitFishingProperties.keySet();
	}
	
	public final Collection<BaitFishingProperties> getAllBaitFishingProperties() {
		return this.mapBaitFishingProperties.values();
	}
	
	public final BaitFishingProperties getBaitFishingProperties(String fish) {
		if (fish != null) {
			for (String key : getFishes()) {
				if (key.equalsIgnoreCase(fish)) {
					return this.mapBaitFishingProperties.get(key);
				}
			}
		}
		
		return null;
	}
	
	public final boolean isExists(String fish) {
		return getBaitFishingProperties(fish) != null;
	}
	
	public final void registerFish(BaitFishingProperties baitFishingProperties) {
		if (baitFishingProperties != null) {
			final String fish = baitFishingProperties.getFish();
			
			this.mapBaitFishingProperties.put(fish, baitFishingProperties);
		}
	}
	
	public final double getFishPossibility(String fish) {
		final BaitFishingProperties baitFishingProperties = getBaitFishingProperties(fish);
		
		return baitFishingProperties != null ? baitFishingProperties.getPossibility() : 0;
	}
	
	public final double getFishChance(String fish) {
		final BaitFishingProperties baitFishingProperties = getBaitFishingProperties(fish);
		
		return baitFishingProperties != null ? baitFishingProperties.getChance() : 0;
	}
	
	public final double getTotalPossibility() {
		
		double totalPossibility = 0;
		
		for (BaitFishingProperties baitFishingProperties : getAllBaitFishingProperties()) {
			final double possibility = baitFishingProperties.getPossibility();
			
			totalPossibility = totalPossibility + possibility;
		}
		
		return totalPossibility;
	}
}
