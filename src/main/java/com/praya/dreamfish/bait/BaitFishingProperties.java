package com.praya.dreamfish.bait;

public class BaitFishingProperties {

	private final String fish;
	private final double possibility;
	private final double chance;
	
	public BaitFishingProperties(String fish, double possibility, double chance) {
		if (fish == null) {
			throw new IllegalArgumentException();
		} else {
			this.fish = fish;
			this.possibility = possibility;
			this.chance = chance;
		}
	}
	
	public final String getFish() {
		return this.fish;
	}
	
	public final double getPossibility() {
		return this.possibility;
	}
	
	public final double getChance() {
		return this.chance;
	}
}
