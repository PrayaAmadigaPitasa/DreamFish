package com.praya.dreamfish.bait;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.bait.BaitConfig;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.manager.game.BaitManager;

public final class BaitMemory extends BaitManager {

	private final BaitConfig baitConfig;
	
	private BaitMemory(DreamFish plugin) {
		super(plugin);
		
		this.baitConfig = new BaitConfig(plugin);
	};
	
	private static class BaitMemorySingleton {
		private static final BaitMemory instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			instance = new BaitMemory(plugin);
		}
	}
	
	public static final BaitMemory getInstance() {
		return BaitMemorySingleton.instance;
	}
	
	protected final BaitConfig getBaitConfig() {
		return this.baitConfig;
	}
	
	@Override
	public final Collection<String> getBaitPropertiesIds() {
		return getBaitPropertiesIds(true);
	}
	
	protected final Collection<String> getBaitPropertiesIds(boolean clone) {
		final Collection<String> baitPropertiesIds = getBaitConfig().mapBaitProperties.keySet();
		
		return clone ? new ArrayList<String>(baitPropertiesIds) : baitPropertiesIds;
	}
	
	@Override
	public final Collection<BaitProperties> getAllBaitProperties() {
		return getAllBaitProperties(true);
	}
	
	protected final Collection<BaitProperties> getAllBaitProperties(boolean clone) {
		final Collection<BaitProperties> allBaitProperties = getBaitConfig().mapBaitProperties.values();
		
		return clone ? new ArrayList<BaitProperties>(allBaitProperties) : allBaitProperties;
	}
	
	@Override
	public final BaitProperties getBaitProperties(String bait) {
		if (bait != null) {
			for (String key : getBaitPropertiesIds(false)) {
				if (key.equalsIgnoreCase(bait)) {
					return getBaitConfig().mapBaitProperties.get(key);
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final BaitProperties getBaitProperties(ItemStack item) {
		if (item != null) {
			for (BaitProperties key : getAllBaitProperties(false)) {
				if (key.getItem().isSimilar(item)) {
					return key;
				}
			}
		}
		
		return null;
	}
}
