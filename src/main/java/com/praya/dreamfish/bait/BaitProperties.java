package com.praya.dreamfish.bait;

import org.bukkit.inventory.ItemStack;

import com.praya.dreamfish.item.ItemRequirement;

public class BaitProperties {

	private final String id;
	private final ItemStack item;
	private final boolean buyable;
	private final double price;
	private final ItemRequirement requirement;
	private final BaitFishing baitFishing;
	
	public BaitProperties(String id, ItemStack item, boolean buyable, double price) {
		this(id, item, buyable, price, null);
	}
	
	public BaitProperties(String id, ItemStack item, boolean buyable, double price, ItemRequirement requirement) {
		this(id, item, buyable, price, requirement, null);
	}
	
	public BaitProperties(String id, ItemStack item, boolean buyable, double price, ItemRequirement requirement, BaitFishing baitFishing) {
		if (id == null || item == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.item = item;
			this.buyable = buyable;
			this.price = price;
			this.requirement = requirement != null ? requirement : new ItemRequirement();
			this.baitFishing = baitFishing != null ? baitFishing : new BaitFishing();
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final ItemStack getItem() {
		return this.item;
	}
	
	public final boolean isBuyable() {
		return this.buyable;
	}
	
	public final double getPrice() {
		return this.price;
	}
	
	public final ItemRequirement getRequirement() {
		return this.requirement;
	}
	
	public final BaitFishing getBaitFishing() {
		return this.baitFishing;
	}
	
	public final void register() {
		final BaitMemory baitMemory = BaitMemory.getInstance();
		final BaitConfig baitConfig = baitMemory.getBaitConfig();
		final String id = getId();
		
		for (String key : baitMemory.getBaitPropertiesIds()) {
			if (key.equalsIgnoreCase(id)) {
				baitConfig.mapBaitProperties.put(key, this);
				return;
			}
		}
		
		baitConfig.mapBaitProperties.put(id, this);
	}
}
