package com.praya.dreamfish.fish;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;

public class FishDrop {

	private double exp;
	private List<String> messages;
	private List<String> commandOP;
	private List<String> commandConsole;
	
	protected FishDrop() {
		this(null);
	}
	
	protected FishDrop(Double exp) {
		this(exp, null, null, null);
	}
	
	protected FishDrop(Double exp, List<String> messages, List<String> commandOP, List<String> commandConsole) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		
		this.exp = exp != null ? exp : mainConfig.getFishDefaultExp();
		this.messages = messages != null ? messages : new ArrayList<String>();
		this.commandOP = commandOP != null ? commandOP : new ArrayList<String>();
		this.commandConsole = commandConsole != null ? commandConsole : new ArrayList<String>();
	}
	
	public final double getExp() {
		return this.exp;
	}
	
	public final List<String> getMessages() {
		return this.messages;
	}
	
	public final List<String> getCommandOP() {
		return this.commandOP;
	}
	
	public final List<String> getCommandConsole() {
		return this.commandConsole;
	}
	
	public final void setExp(double exp) {
		this.exp = Math.max(0, exp);
	}
	
	public final void setMessages(List<String> messages) {
		this.messages = messages != null ? messages : new ArrayList<String>();
	}
	
	public final void setCommandOP(List<String> commandOP) {
		this.commandOP = commandOP != null ? commandOP : new ArrayList<String>();
	}
	
	public final void setCommandConsole(List<String> commandConsole) {
		this.commandConsole = commandConsole != null ? commandConsole : new ArrayList<String>();
	}
}
