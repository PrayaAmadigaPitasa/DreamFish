package com.praya.dreamfish.fish;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.fish.FishConfig;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.manager.game.FishManager;

public final class FishMemory extends FishManager {

	private final FishConfig fishConfig;
	
	private FishMemory(DreamFish plugin) {
		super(plugin);
		
		this.fishConfig = new FishConfig(plugin);
	}
	
	private static class FishMemorySingleton {
		private static final FishMemory instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			instance = new FishMemory(plugin);
		}
	}
	
	public static final FishMemory getInstance() {
		return FishMemorySingleton.instance;
	}
	
	public final FishConfig getFishConfig() {
		return this.fishConfig;
	}
	
	@Override
	public final Collection<String> getFishPropertiesIds() {
		return getFishPropertiesIds(true);
	}
	
	protected final Collection<String> getFishPropertiesIds(boolean clone) {
		final Collection<String> fishPropertiesIds = getFishConfig().mapFishProperties.keySet();
		
		return clone ? new ArrayList<String>(fishPropertiesIds)	: fishPropertiesIds;
	}
	
	@Override
	public final Collection<FishProperties> getAllFishProperties() {
		return getFishConfig().mapFishProperties.values();
	}
	
	protected final Collection<FishProperties> getAllFishProperties(boolean clone) {
		final Collection<FishProperties> allFishProperties = getFishConfig().mapFishProperties.values();
		
		return clone ? new ArrayList<FishProperties>(allFishProperties) : allFishProperties;
	}
	
	@Override
	public final FishProperties getFishProperties(String fish) {
		if (fish != null) {
			for (String key : getFishPropertiesIds(false)) {
				if (key.equalsIgnoreCase(fish)) {
					return getFishConfig().mapFishProperties.get(key);
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final FishProperties getFishProperties(ItemStack item) {
		if (item != null) {
			for (FishProperties key : getAllFishProperties(false)) {
				if (key.getItem().isSimilar(item)) {
					return key;
				}
			}
		}
		
		return null;
	}
}
