package com.praya.dreamfish.fish;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.block.Biome;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.bait.BaitFishing;
import com.praya.dreamfish.bait.BaitFishingProperties;
import com.praya.dreamfish.bait.BaitMemory;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.handler.HandlerConfig;
import com.praya.dreamfish.item.ItemRequirement;
import com.praya.agarthalib.utility.BiomeUtil;
import com.praya.agarthalib.utility.ConfigUtil;
import com.praya.agarthalib.utility.EntityUtil;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.FileUtil;
import com.praya.agarthalib.utility.MathUtil;

public final class FishConfig extends HandlerConfig {
	
	private static final String PATH_FILE = "Configuration/fish.yml";
	
	protected final HashMap<String, FishProperties> mapFishProperties = new HashMap<String, FishProperties>();
	
	protected FishConfig(DreamFish plugin) {
		super(plugin);
		
		moveOldFile();
		setup();
	}
	
	@Override
	public final void setup() {
		clearCache();
		loadConfig();
	}
	
	private final void clearCache() {
		this.mapFishProperties.clear();
	}
	
	private final void loadConfig() {
		final BaitMemory baitMemory = BaitMemory.getInstance();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final File file = FileUtil.getFile(plugin, PATH_FILE);		
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration config = FileUtil.getFileConfiguration(file);
		
		for (String key : config.getKeys(false)) {
			final ConfigurationSection itemDataSection = config.getConfigurationSection(key);
			final ItemStack item = ConfigUtil.getItemStack(itemDataSection);
			final ItemRequirement requirement = new ItemRequirement();
			final FishDrop fishDrop = new FishDrop();
			final List<String> regions = new ArrayList<String>();
			final List<Biome> biomes = new ArrayList<Biome>();
			final HashMap<String, Double> mapBaitPossibility = new HashMap<String, Double>();
			final HashMap<String, Double> mapBaitSuccessRate = new HashMap<String, Double>();
			
			boolean asBait = false;
			boolean buyable = true;
			boolean invisible = false;
			EntityType type = mainConfig.getFishDefaultType();
			double resistance = mainConfig.getFishDefaultResistance();
			double power = mainConfig.getFishDefaultPower();
			double maxPower = mainConfig.getFishDefaultMaxPower();
			double maxSpeed = mainConfig.getFishDefaultMaxSpeed();
			double maxDive = mainConfig.getFishDefaultMaxDive();
			double averageLength = 0;
			double averageWeight = 0;
			double price = 0;
			
			for (String itemData : itemDataSection.getKeys(false)) {
				if (itemData.equalsIgnoreCase("Type")) {
					final String textType = itemDataSection.getString(itemData);
					
					type = EntityUtil.isEntityTypeExists(textType) ? EntityUtil.getEntityType(textType) : type;
				} else if (itemData.equalsIgnoreCase("As_Bait")) {
					asBait =  itemDataSection.getBoolean(itemData);
				} else if (itemData.equalsIgnoreCase("Invisible")) {
					invisible =  itemDataSection.getBoolean(itemData);
				} else if (itemData.equalsIgnoreCase("Buyable")) {
					buyable =  itemDataSection.getBoolean(itemData);
				} else if (itemData.equalsIgnoreCase("Resistance")) {
					resistance =  itemDataSection.getDouble(itemData);
				} else if (itemData.equalsIgnoreCase("Power")) {
					power =  itemDataSection.getDouble(itemData);
				} else if (itemData.equalsIgnoreCase("Max_Power")) {
					maxPower =  itemDataSection.getDouble(itemData);
				} else if (itemData.equalsIgnoreCase("Max_Speed")) {
					maxSpeed =  itemDataSection.getDouble(itemData);
				} else if (itemData.equalsIgnoreCase("Max_Dive")) {
					maxDive =  itemDataSection.getDouble(itemData);
				} else if (itemData.equalsIgnoreCase("Average_Length")) {
					averageLength =  itemDataSection.getDouble(itemData);
				} else if (itemData.equalsIgnoreCase("Average_Weight")) {
					averageWeight =  itemDataSection.getDouble(itemData);
				} else if (itemData.equalsIgnoreCase("Price")) {
					price = itemDataSection.getDouble(itemData);
				} else if (itemData.equalsIgnoreCase("Experience") || itemData.equalsIgnoreCase("Exp")) {
					final double exp = itemDataSection.getDouble(itemData); 
					
					fishDrop.setExp(exp);
				} else if (itemData.equalsIgnoreCase("Requirement")) {
					final ConfigurationSection requirementDataSection = itemDataSection.getConfigurationSection(itemData);
					
					for (String requirementData : requirementDataSection.getKeys(false)) {
						if (requirementData.equalsIgnoreCase("Permission")) {
							final String permission = requirementDataSection.getString(requirementData);
							
							requirement.setPermission(permission);
						} else if (requirementData.equalsIgnoreCase("Level")) {
							final int level = requirementDataSection.getInt(requirementData);
							
							requirement.setLevel(level);;
						}
					}
				} else if (itemData.equalsIgnoreCase("Drops") || itemData.equalsIgnoreCase("Drop")) {
					final ConfigurationSection dropDataSection = itemDataSection.getConfigurationSection(itemData);
					
					for (String dropData : dropDataSection.getKeys(false)) {
						if (dropData.equalsIgnoreCase("Experience") || dropData.equalsIgnoreCase("Exp")) {
							final double exp =  dropDataSection.getDouble(dropData);
							
							fishDrop.setExp(exp);
						} else if (dropData.equalsIgnoreCase("Messages") || dropData.equalsIgnoreCase("Message")) {
							final List<String> messages = dropDataSection.getStringList(dropData);
							
							fishDrop.setMessages(messages);
						} else if (dropData.equalsIgnoreCase("Commands") || dropData.equalsIgnoreCase("Command")) {
							final ConfigurationSection dropCommandDataSection = dropDataSection.getConfigurationSection(dropData);
							
							for (String dropCommandData : dropCommandDataSection.getKeys(false)) {
								if (dropCommandData.equalsIgnoreCase("OP")) {
									final List<String> commandOP = dropCommandDataSection.getStringList(dropCommandData);
									
									fishDrop.setCommandOP(commandOP);
								} else if (dropCommandData.equalsIgnoreCase("Console")) {
									final List<String> commandConsole = dropCommandDataSection.getStringList(dropCommandData);
									
									fishDrop.setCommandConsole(commandConsole);
								}
							}
						}
					}
				} else if (itemData.equalsIgnoreCase("Bait")) {
					final ConfigurationSection baitSection = itemDataSection.getConfigurationSection(itemData);
					
					for (String bait : baitSection.getKeys(false)) {
						final ConfigurationSection baitDataSection = baitSection.getConfigurationSection(bait);
						
						double baitPossibility = 1;
						double baitSuccessRate = 0;
						
						for (String baitData : baitDataSection.getKeys(false)) {
							if (baitData.equalsIgnoreCase("Possibility")) {
								baitPossibility = baitDataSection.getDouble(baitData);
							} else if (baitData.equalsIgnoreCase("Success_Rate")) {
								baitSuccessRate = baitDataSection.getDouble(baitData);
							}
						}
						
						MathUtil.limitDouble(baitPossibility, 1, baitPossibility);
						MathUtil.limitDouble(baitSuccessRate, 0, baitSuccessRate);
						
						mapBaitPossibility.put(bait, baitPossibility);
						mapBaitSuccessRate.put(bait, baitSuccessRate);
					}
				} else if (itemData.equalsIgnoreCase("Biome") || itemData.equalsIgnoreCase("Biomes")) {
					for (String biomeKey : itemDataSection.getStringList(itemData)) {
						final Biome biome = BiomeUtil.getBiome(biomeKey);
						
						if (biome != null) {
							biomes.add(biome);
						}
					}
				} else if (itemData.equalsIgnoreCase("Messages") || itemData.equalsIgnoreCase("Message")) {
					final List<String> messages = itemDataSection.getStringList(itemData);
					
					fishDrop.setMessages(messages);
				} else if (itemData.equalsIgnoreCase("Commands") || itemData.equalsIgnoreCase("Command")) {
					final List<String> commandOP = itemDataSection.getStringList(itemData);
					
					fishDrop.setCommandOP(commandOP);
				} else if (itemData.equalsIgnoreCase("Regions") || itemData.equalsIgnoreCase("Region")) {
					regions.addAll(itemDataSection.getStringList(itemData));
				}
			}
			
			if (EquipmentUtil.isSolid(item)) {
				
				MathUtil.limitDouble(resistance, 0, 100);
				MathUtil.limitDouble(power, 0, power);
				MathUtil.limitDouble(maxPower, 0, maxPower);
				MathUtil.limitDouble(maxSpeed, 0, 1);
				MathUtil.limitDouble(maxDive, 0, 1);
				MathUtil.limitDouble(averageLength, 1, averageLength);
				MathUtil.limitDouble(averageWeight, 1, averageWeight);
				MathUtil.limitDouble(price, 0, price);
				
				if (asBait) {
					final BaitProperties baitProperties = new BaitProperties(key, item, buyable, price, requirement);
					
					baitProperties.register();
				}
				
				for (String bait : mapBaitPossibility.keySet()) {
					final BaitProperties baitProperties = baitMemory.getBaitProperties(bait);
					
					if (baitProperties != null) {
						final BaitFishing baitFishing = baitProperties.getBaitFishing();
						final double possibility = mapBaitPossibility.get(bait);
						final double chance = mapBaitSuccessRate.get(bait);
						final BaitFishingProperties baitFishingProperties = new BaitFishingProperties(key, possibility, chance);
						
						baitFishing.registerFish(baitFishingProperties);
					}
				}
				
				final FishProperties fishProperties = new FishProperties(key, item, type, invisible, price, resistance, power, maxPower, maxSpeed, maxDive, averageLength, averageWeight, requirement, fishDrop, regions, biomes);
				
				this.mapFishProperties.put(key, fishProperties);
			}
		}
	}
	
	private final void moveOldFile() {
		final String pathSource = "fish.yml";
		final String pathTarget = PATH_FILE;
		final File fileSource = FileUtil.getFile(plugin, pathSource);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSource.exists()) {
			FileUtil.moveFileSilent(fileSource, fileTarget);
		}
	}
}