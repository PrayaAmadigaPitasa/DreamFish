package com.praya.dreamfish.fish;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.block.Biome;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.bait.BaitFishing;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.item.ItemRequirement;
import com.praya.dreamfish.manager.game.BaitManager;
import com.praya.dreamfish.manager.game.GameManager;

public class FishProperties {

	private final String id;
	private final ItemStack item;
	private final EntityType type;
	private final boolean invisible;
	private final double price;
	private final double resistance;
	private final double power;
	private final double maxPower;
	private final double maxSpeed;
	private final double maxDive;
	private final double averageLength;
	private final double averageWeight;
	private final ItemRequirement requirement;
	private final FishDrop fishDrop;
	private final List<String> regions;
	private final List<Biome> biomes;
	
	protected FishProperties(String id, ItemStack item, EntityType type, boolean invisible, double price, double resistance, double power, double maxPower, double maxSpeed, double maxDive, double averageLength, double averageWeight) {
		this(id, item, type, invisible, price, resistance, power, maxPower, maxSpeed, maxDive, averageLength, averageWeight, null);
	}
	
	protected FishProperties(String id, ItemStack item, EntityType type, boolean invisible, double price, double resistance, double power, double maxPower, double maxSpeed, double maxDive, double averageLength, double averageWeight, ItemRequirement requirement) {
		this(id, item, type, invisible, price, resistance, power, maxPower, maxSpeed, maxDive, averageLength, averageWeight, requirement, null);
	}
	
	protected FishProperties(String id, ItemStack item, EntityType type, boolean invisible, double price, double resistance, double power, double maxPower, double maxSpeed, double maxDive, double averageLength, double averageWeight, ItemRequirement requirement, FishDrop fishDrop) {
		this(id, item, type, invisible, price, resistance, power, maxPower, maxSpeed, maxDive, averageLength, averageWeight, requirement, fishDrop, null);
	}
	
	protected FishProperties(String id, ItemStack item, EntityType type, boolean invisible, double price, double resistance, double power, double maxPower, double maxSpeed, double maxDive, double averageLength, double averageWeight, ItemRequirement requirement, FishDrop fishDrop, List<String> regions) {
		this(id, item, type, invisible, price, resistance, power, maxPower, maxSpeed, maxDive, averageLength, averageWeight, requirement, fishDrop, regions, null);
	}
	
	protected FishProperties(String id, ItemStack item, EntityType type, boolean invisible, double price, double resistance, double power, double maxPower, double maxSpeed, double maxDive, double averageLength, double averageWeight, ItemRequirement requirement, FishDrop fishDrop, List<String> regions, List<Biome> biomes) {
		if (id == null || item == null || type == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.item = item;
			this.type = type;
			this.invisible = invisible;
			this.price = price;
			this.resistance = resistance;
			this.power = power;
			this.maxPower = maxPower;
			this.maxSpeed = maxSpeed;
			this.maxDive = maxDive;
			this.averageLength = averageLength;
			this.averageWeight = averageWeight;
			this.requirement = requirement != null ? requirement : new ItemRequirement();
			this.fishDrop = fishDrop != null ? fishDrop : new FishDrop();
			this.regions = regions != null ? regions : new ArrayList<String>();
			this.biomes = biomes != null ? biomes : new ArrayList<Biome>();
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final ItemStack getItem() {
		return this.item;
	}
	
	public final EntityType getType() {
		return this.type;
	}
	
	public final boolean isInvisible() {
		return this.invisible;
	}
	
	public final double getPrice() {
		return this.price;
	}
	
	public final double getResistance() {
		return this.resistance;
	}
	
	public final double getPower() {
		return this.power;
	}
	
	public final double getMaxPower() {
		return this.maxPower;
	}
	
	public final double getMaxSpeed() {
		return this.maxSpeed;
	}
	
	public final double getMaxDive() {
		return this.maxDive;
	}
	
	public final double getAverageLength() {
		return this.averageLength;
	}
	
	public final double getAverageWeight() {
		return this.averageWeight;
	}
	
	public final ItemRequirement getRequirement() {
		return this.requirement;
	}
	
	public final FishDrop getFishDrop() {
		return this.fishDrop;
	}
	
	public final List<String> getRegions() {
		return this.regions;
	}
	
	public final List<Biome> getBiomes() {
		return this.biomes;
	}
	
	public final BaitProperties getBaitProperties() {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final GameManager gameManager = plugin.getGameManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		final BaitProperties baitProperties = baitManager.getBaitProperties(getItem());
		
		return baitProperties;
	}
	
	public final boolean asBait() {
		return getBaitProperties() != null;
	}
	
	public final List<BaitProperties> getAllConnectBaitProperties() {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final GameManager gameManager = plugin.getGameManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		final List<BaitProperties> connectBaitProperties = new ArrayList<BaitProperties>();
		
		for (BaitProperties baitProperties : baitManager.getAllBaitProperties()) {
			final BaitFishing baitFishing = baitProperties.getBaitFishing();
			
			for (String fish : baitFishing.getFishes()) {
				if (fish.equalsIgnoreCase(getId())) {
					connectBaitProperties.add(baitProperties);
				}
			}
		}
		
		return connectBaitProperties;
	}
	
	public final BaitProperties getConnectBaitProperties(String bait) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final GameManager gameManager = plugin.getGameManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		
		if (bait != null) {
			final BaitProperties baitProperties = baitManager.getBaitProperties(bait);
			
			if (baitProperties != null) {
				final BaitFishing baitFishing = baitProperties.getBaitFishing();
				
				for (String fish : baitFishing.getFishes()) {
					if (fish.equalsIgnoreCase(getId())) {
						return baitProperties;
					}
				}
			}
		}
		
		return null;
	}
	
	public final boolean hasConnectBait(String bait) {
		return getConnectBaitProperties(bait) != null;
	}
	
	public final double getPossibility(String bait) {
		if (bait != null) {
			final BaitProperties baitProperties = getConnectBaitProperties(bait);
			
			if (baitProperties != null) {
				final BaitFishing baitFishing = baitProperties.getBaitFishing();
				
				return baitFishing.getFishPossibility(getId());
			}
		}
		
		return 0;
	}
	
	public final double getChance(String bait) {
		if (bait != null) {
			final BaitProperties baitProperties = getConnectBaitProperties(bait);
			
			if (baitProperties != null) {
				final BaitFishing baitFishing = baitProperties.getBaitFishing();
				
				return baitFishing.getFishChance(getId());
			}
		}
		
		return 0;
	}
	
	public final boolean inRegion(String region) {
		if (region != null) {
			if (getRegions().isEmpty()) {
				return true;
			} else {
				for (String key : getRegions()) {
					if (key.equalsIgnoreCase(region)) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	public final boolean inBiome(Biome biome) {
		if (biome != null) {
			return !getBiomes().isEmpty() ? getBiomes().contains(biome) : true;
		} else {
			return false;
		}
	}
}