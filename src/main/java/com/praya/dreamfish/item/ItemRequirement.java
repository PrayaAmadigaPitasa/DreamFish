package com.praya.dreamfish.item;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerFishing;
import com.praya.agarthalib.utility.SenderUtil;

public class ItemRequirement {

	private String permission;
	private int level;
	
	public ItemRequirement() {
		this(null, 1);
	}
	
	public ItemRequirement(String permission, int level) {
		setPermission(permission);
		setLevel(level);
	}
	
	public final String getPermission() {
		return this.permission;
	}
	
	public final int getLevel() {
		return this.level;
	}
	
	public final void setPermission(String permission) {
		this.permission = permission;
	}
	
	public final void setLevel(int level) {
		this.level = Math.max(0, level);
	}
	
	public final boolean isAllowed(Player player) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		
		if (player != null) {
			final PlayerFishing playerFishing = playerFishingManager.getPlayerFishing(player);
			final int playerLevel = playerFishing.getLevel();
			final boolean allowedLevel = getLevel() <= playerLevel;
			final boolean allowedPermission = SenderUtil.hasPermission(player, getPermission());
			
			return allowedLevel && allowedPermission;
		} else {
			return false;
		}
	}
}
