package com.praya.dreamfish.event;

import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import com.praya.agarthalib.utility.PlayerUtil;

public class PlayerFishingLevelChangeEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final UUID playerID;
	private final int level;
	private final LevelChangeReason reason;

	public PlayerFishingLevelChangeEvent(OfflinePlayer player, int level) {
		this(player, level, LevelChangeReason.CUSTOM);
	}

    public PlayerFishingLevelChangeEvent(OfflinePlayer player, int level, LevelChangeReason reason) {
    	this(player.getUniqueId(), level, reason);
    }
	
	public PlayerFishingLevelChangeEvent(UUID playerID, int level) {
		this(playerID, level, LevelChangeReason.CUSTOM);
	}

    public PlayerFishingLevelChangeEvent(UUID playerID, int level, LevelChangeReason reason) {
        this.playerID = playerID;
        this.level = level;
        this.reason = reason;
    }
    
    public final OfflinePlayer getPlayer() {
    	return PlayerUtil.getPlayer(playerID);
    }
    
    public final Player getOnlinePlayer() {
    	return PlayerUtil.getOnlinePlayer(playerID);
    }
    
    public final boolean isPlayerOnline() {
    	return getOnlinePlayer() != null;
    }
    
    public final int getLevel() {
    	return this.level;
    }
    
    public final LevelChangeReason getReason() {
    	return this.reason;
    }
	
	public HandlerList getHandlers() {
        return handlers;
    }

    public static final HandlerList getHandlerList() {
        return handlers;
    }
    
    public enum LevelChangeReason {
    	
    	EXP_UP,
    	COMMAND,
    	CUSTOM;
    }
}
