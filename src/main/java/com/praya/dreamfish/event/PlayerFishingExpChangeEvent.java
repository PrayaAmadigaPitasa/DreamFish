package com.praya.dreamfish.event;

import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import com.praya.agarthalib.utility.PlayerUtil;

public class PlayerFishingExpChangeEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final UUID playerID;
	private final ExpChangeReason reason;
	
	private float exp;
	private boolean cancel;
	
	public PlayerFishingExpChangeEvent(OfflinePlayer player, float exp) {
		this(player, exp, ExpChangeReason.CUSTOM);
	}

    public PlayerFishingExpChangeEvent(OfflinePlayer player, float exp, ExpChangeReason reason) {
    	this(player.getUniqueId(), exp, reason);
    }
	
	public PlayerFishingExpChangeEvent(UUID playerID, float exp) {
		this(playerID, exp, ExpChangeReason.CUSTOM);
	}

    public PlayerFishingExpChangeEvent(UUID playerID, float exp, ExpChangeReason reason) {
    	
        this.playerID = playerID;
        this.reason = reason;
        this.exp = exp;
        
        setCancelled(false);
    }
    
    public final OfflinePlayer getPlayer() {
    	return PlayerUtil.getPlayer(playerID);
    }
    
    public final Player getOnlinePlayer() {
    	return PlayerUtil.getOnlinePlayer(playerID);
    }
    
    public final boolean isPlayerOnline() {
    	return getOnlinePlayer() != null;
    }
    
    public final ExpChangeReason getReason() {
    	return this.reason;
    }
    
    public final float getExp() {
    	return this.exp;
    }
    
    public final void setExp(float exp) {
    	this.exp = exp;
    }
    
    @Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
	
	public HandlerList getHandlers() {
        return handlers;
    }

    public static final HandlerList getHandlerList() {
        return handlers;
    }
    
    public enum ExpChangeReason {
    	
    	FISHING,
    	COMMAND,
    	CUSTOM;
    }
}
