package com.praya.dreamfish.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class FishingChangeTensionEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	private boolean cancel = false;
	private Player player;
    private double tension;

    public FishingChangeTensionEvent(Player player, double tension) {
        this.player = player;
        this.tension = tension;
    }

    public final Player getPlayer() {
    	return this.player;
    }
    
    public final double getTension() {
    	return this.tension;
    }
    
    public final void setTension(double tension) {
    	this.tension = tension;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static final HandlerList getHandlerList() {
        return handlers;
    }

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
}