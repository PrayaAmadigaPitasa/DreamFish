package com.praya.dreamfish.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class FishingChangeFishPowerEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	private boolean cancel = false;
	private Player player;
    private double fishPower;

    public FishingChangeFishPowerEvent(Player player, double fishPower) {
        this.player = player;
        this.fishPower = fishPower;
    }

    public final Player getPlayer() {
    	return this.player;
    }
    
    public final double getFishPower() {
    	return this.fishPower;
    }
    
    public final void setFishPower(double fishPower) {
    	this.fishPower = fishPower;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static final HandlerList getHandlerList() {
        return handlers;
    }

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
}