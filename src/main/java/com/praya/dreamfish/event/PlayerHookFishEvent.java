package com.praya.dreamfish.event;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.bait.BaitFishing;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.manager.game.BaitManager;
import com.praya.dreamfish.manager.game.GameManager;

public class PlayerHookFishEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	private boolean cancel = false;
	private Player player;
    private String fish;
    private String bait;
    private double chance;
    private Entity hook;

    public PlayerHookFishEvent(Player player, String fish, String bait, Entity hook) {
    	final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
    	final GameManager gameManager = plugin.getGameManager();
    	final BaitManager baitManager = gameManager.getBaitManager();
    	final BaitProperties baitProperties = baitManager.getBaitProperties(bait);
    	final BaitFishing baitFishing = baitProperties.getBaitFishing();
    	final double chance = baitFishing.getFishChance(fish);
    	
        this.player = player;
        this.fish = fish;
        this.bait = bait;
        this.chance = chance;
        this.hook = hook;
    }

    public final Entity getHook() {
        return hook;
    }

    public final Player getPlayer() {
    	return this.player;
    }
    
    public final String getFish() {
    	return this.fish;
    }
    
    public final String getBait() {
    	return this.bait;
    }
    
    public final double getChance() {
    	return this.chance;
    }
    
    public final void setChance(double chance) {
    	this.chance = chance;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static final HandlerList getHandlerList() {
        return handlers;
    }

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
}
