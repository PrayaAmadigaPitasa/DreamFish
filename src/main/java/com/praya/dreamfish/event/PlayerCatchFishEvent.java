package com.praya.dreamfish.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.fish.FishDrop;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.manager.game.FishManager;
import com.praya.dreamfish.manager.game.GameManager;

public class PlayerCatchFishEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	private boolean cancel = false;
	private Player player;
	private String fish;
	private double exp;
    private double length;
    private double weight;

    public PlayerCatchFishEvent(Player player, String fish) {
    	final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
    	final GameManager gameManager = plugin.getGameManager();
    	final FishManager fishManager = gameManager.getFishManager();
    	final FishProperties fishProperties = fishManager.getFishProperties(fish);
    	final FishDrop fishDrop = fishProperties.getFishDrop();
    	final double exp = fishDrop.getExp();
    	final double averageLength = fishProperties.getAverageLength();
    	final double averageWeight = fishProperties.getAverageWeight();
    	
        this.player = player;
        this.fish = fish;
        this.exp = exp;
        this.length = averageLength * (1 + ((Math.random()-1)/5));
        this.weight = averageWeight * (1 + ((Math.random()-1)/5));
        
    }

    public final Player getPlayer() {
    	return this.player;
    }
    
    public final String getFish() {
    	return this.fish;
    }
    
    public final double getExp() {
    	return this.exp;
    }
    
    public final double getLength() {
    	return this.length;
    }
    
    public final double getWeight() {
    	return this.weight;
    }
    
    public final void setExp(double exp) {
    	this.exp = exp;
    }
    
    public final void setLength(double length) {
    	this.length = length;
    }
    
    public final void setWeight(double weight) {
    	this.weight = weight;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static final HandlerList getHandlerList() {
        return handlers;
    }

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
}
