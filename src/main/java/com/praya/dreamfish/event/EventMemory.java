package com.praya.dreamfish.event;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.game.EventManager;

import core.praya.agarthalib.enums.main.VersionNMS;

import com.praya.agarthalib.utility.ServerEventUtil;
import com.praya.agarthalib.utility.ServerUtil;

public final class EventMemory extends EventManager {

	private final Constructor contructor;
	private final Set<PlayerFishEvent> fakePlayerFishEvents;
	
	private EventMemory(DreamFish plugin) {
		super(plugin);
		
		final Constructor[] contructors = PlayerFishEvent.class.getConstructors();
		final int index = ServerUtil.isCompatible(VersionNMS.V1_11_R1) ? 0 : 1;
		
		contructor = contructors[index];
		fakePlayerFishEvents = new HashSet<PlayerFishEvent>();
	}
	
	private static class EventMemorySingleton {
		private static final EventMemory INSTANCE;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			INSTANCE = new EventMemory(plugin);
		}
	}
	
	public static final EventMemory getInstance() {
		return EventMemorySingleton.INSTANCE;
	}
	
	@Override
	public final Set<PlayerFishEvent> getAllFakePlayerFishEvent() {
		return getAllFakePlayerFishEvent(true);
	}
	
	protected final Set<PlayerFishEvent> getAllFakePlayerFishEvent(boolean clone) {
		final Set<PlayerFishEvent> fakePlayerFishEvents = this.fakePlayerFishEvents;
		
		return clone ? new HashSet<PlayerFishEvent>(fakePlayerFishEvents) : fakePlayerFishEvents;
	}
	
	@Override
	public final PlayerFishEvent callFakePlayerFishEvent(Player player, Entity entity, Object hookEntity, State state) {
		try {
			final PlayerFishEvent event = (PlayerFishEvent) contructor.newInstance(player, entity, hookEntity, state);
			
			fakePlayerFishEvents.add(event);
			ServerEventUtil.callEvent(event);
			
			return event;
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return null;
    }
	
	@Override
	public final boolean isFakePlayerFishEvent(PlayerFishEvent playerFishEvent) {
		return playerFishEvent != null ? getAllFakePlayerFishEvent().contains(playerFishEvent) : false;
	}
	
	@Override
	public final boolean removeFakePlayerFishEvent(PlayerFishEvent playerFishEvent) {
		if (isFakePlayerFishEvent(playerFishEvent)) {
			
			this.fakePlayerFishEvents.remove(playerFishEvent);
			
			return true;
		} else {
			return false;
		}
	}
}
