package com.praya.dreamfish.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerPullLuresEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	private boolean cancel = false;
	private Player player;
    private String fish;

    public PlayerPullLuresEvent(Player player, String fish) {
        this.player = player;
        this.fish = fish;
    }

    public final Player getPlayer() {
    	return this.player;
    }
    
    public final String getFish() {
    	return this.fish;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static final HandlerList getHandlerList() {
        return handlers;
    }

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
}
