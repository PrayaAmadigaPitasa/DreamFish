package com.praya.dreamfish.plugin;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.plugin.PluginPropertiesManager;

import core.praya.agarthalib.builder.plugin.PluginPropertiesBuild;
import core.praya.agarthalib.builder.plugin.PluginPropertiesResourceBuild;
import core.praya.agarthalib.builder.plugin.PluginPropertiesStreamBuild;

public final class PluginPropertiesMemory extends PluginPropertiesManager {
	
	private final PluginPropertiesConfig pluginPropertiesConfig;
	private final PluginPropertiesResourceBuild pluginPropertiesResource;
	private final PluginPropertiesStreamBuild pluginPropertiesStream;
	
	private PluginPropertiesMemory(DreamFish plugin) {
		super(plugin);
		
		final String name = plugin.getName();
		final PluginPropertiesConfig pluginPropertiesConfig = new PluginPropertiesConfig(plugin);
		final PluginPropertiesResourceBuild pluginPropertiesResource = PluginPropertiesBuild.getPluginPropertiesResource(plugin, plugin.getPluginType(), plugin.getPluginVersion());
		final PluginPropertiesStreamBuild pluginPropertiesStream = pluginPropertiesConfig.mapPluginProperties.get(name);
		
		this.pluginPropertiesConfig = pluginPropertiesConfig;
		this.pluginPropertiesResource = pluginPropertiesResource;
		this.pluginPropertiesStream = pluginPropertiesStream != null ? pluginPropertiesStream : new PluginPropertiesStreamBuild();
	}
	
	private static class PluginPropertiesMemorySingleton {
		private static final PluginPropertiesMemory instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			instance = new PluginPropertiesMemory(plugin);
		}
	}
	
	public static final PluginPropertiesMemory getInstance() {
		return PluginPropertiesMemorySingleton.instance;
	}
	
	public final PluginPropertiesConfig getPluginPropertiesConfig() {
		return this.pluginPropertiesConfig;
	}
	
	@Override
	public final PluginPropertiesResourceBuild getPluginPropertiesResource() {
		return this.pluginPropertiesResource;
	}
	
	@Override
	public final PluginPropertiesStreamBuild getPluginPropertiesStream() {
		return this.pluginPropertiesStream;
	}
	
	@Override
	public final Collection<String> getPluginIds() {
		return getPluginIds(true);
	}
	
	protected final Collection<String> getPluginIds(boolean clone) {
		final Collection<String> pluginIds = getPluginPropertiesConfig().mapPluginProperties.keySet();
		
		return clone ? new ArrayList<String>(pluginIds) : pluginIds;
	}
	
	@Override
	public final Collection<PluginPropertiesStreamBuild> getAllPluginProperties() {
		return getAllPluginProperties(true);
	}
	
	protected final Collection<PluginPropertiesStreamBuild> getAllPluginProperties(boolean clone) {
		final Collection<PluginPropertiesStreamBuild> allPluginProperties = getPluginPropertiesConfig().mapPluginProperties.values();
		
		return clone ? new ArrayList<PluginPropertiesStreamBuild>(allPluginProperties) : allPluginProperties;
	}
	
	@Override
	public final PluginPropertiesStreamBuild getPluginProperties(String id) {
		if (id != null) {
			for (String key : getPluginIds(false)) {
				if (key.equalsIgnoreCase(id)) {
					return getPluginPropertiesConfig().mapPluginProperties.get(key);
				}
			}
		}
		
		return null;
	}
}
