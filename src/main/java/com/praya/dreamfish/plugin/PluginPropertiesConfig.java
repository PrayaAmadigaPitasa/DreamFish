package com.praya.dreamfish.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import com.praya.agarthalib.utility.FileUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerConfig;

import core.praya.agarthalib.builder.plugin.PluginPropertiesStreamBuild;
import core.praya.agarthalib.builder.plugin.PluginTypePropertiesBuild;

public final class PluginPropertiesConfig extends HandlerConfig {

	private static final String URL_PROPERTIES = "https://pastebin.com/raw/8xp3SRnF";
	
	protected final HashMap<String, PluginPropertiesStreamBuild> mapPluginProperties = new HashMap<String, PluginPropertiesStreamBuild>(); 
	
	protected PluginPropertiesConfig(DreamFish plugin) {
		super(plugin);
		
		setup();
	};
	
	public final void setup() {
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapPluginProperties.clear();
	}
	
	private final void loadConfig() {
		final FileConfiguration config = FileUtil.getFileConfigurationURL(URL_PROPERTIES);
		
		if (config != null) {
		
			String author = null;
			String owner = null;
			String company = null;
			String website = null;
			
			for (String key : config.getKeys(false)) {
				if (key.equalsIgnoreCase("Configuration")) {
					final ConfigurationSection dataSection = config.getConfigurationSection(key);
					
					for (String data : dataSection.getKeys(false)) {
						if (data.equalsIgnoreCase("Author")) {
							author = dataSection.getString(data);
						} else if (data.equalsIgnoreCase("Owner")) {
							owner = dataSection.getString(data);
						} else if (data.equalsIgnoreCase("Company")) {
							company = dataSection.getString(data);
						} else if (data.equalsIgnoreCase("Website")) {
							website = dataSection.getString(data);
						}
					}
				}
			}
			
			for (String key : config.getKeys(false)) {
				if (key.equalsIgnoreCase("Configuration")) {
					final ConfigurationSection dataSection = config.getConfigurationSection(key);
					
					for (String data : dataSection.getKeys(false)) {
						if (data.equalsIgnoreCase("Plugins")) {
							final ConfigurationSection pluginNameSection = dataSection.getConfigurationSection(data);
							
							for (String pluginName : pluginNameSection.getKeys(false)) {
								final ConfigurationSection pluginDataSection = pluginNameSection.getConfigurationSection(pluginName);
								final List<String> developer = new ArrayList<String>();
								final HashMap<String, PluginTypePropertiesBuild> mapPluginTypeProperties = new HashMap<String, PluginTypePropertiesBuild>();
								
								boolean activated = true;
								
								for (String pluginData : pluginDataSection.getKeys(false)) {
									if (pluginData.equalsIgnoreCase("Activated")) {
										activated = pluginDataSection.getBoolean(pluginData);
									} else if (pluginData.equalsIgnoreCase("Developer")) {
										final List<String> pluginDeveloper = pluginDataSection.getStringList(pluginData);
										
										developer.addAll(pluginDeveloper);
									} else if (pluginData.equalsIgnoreCase("Type")) {
										final ConfigurationSection pluginTypeSection = pluginDataSection.getConfigurationSection(pluginData);
										
										for (String pluginType : pluginTypeSection.getKeys(false)) {
											final ConfigurationSection pluginTypeDataSection = pluginTypeSection.getConfigurationSection(pluginType);
											
											String pluginVersion = null;
											String pluginWebsite = website;
											String pluginPriceSymbol = "Euro";
											double pluginPriceValue = 0;
											
											for (String pluginTypeData : pluginTypeDataSection.getKeys(false)) {
												if (pluginTypeData.equalsIgnoreCase("Version")) {
													pluginVersion = pluginTypeDataSection.getString(pluginTypeData);
												} else if (pluginTypeData.equalsIgnoreCase("Website")) {
													pluginWebsite = pluginTypeDataSection.getString(pluginTypeData);
												} else if (pluginTypeData.equalsIgnoreCase("Price_Symbol")) {
													pluginPriceSymbol = pluginTypeDataSection.getString(pluginTypeData);
												} else if (pluginTypeData.equalsIgnoreCase("Price_Value")) {
													pluginPriceValue = pluginTypeDataSection.getDouble(pluginTypeData);
												}
											}
											
											final PluginTypePropertiesBuild pluginTypeProperties = new PluginTypePropertiesBuild(pluginType, pluginVersion, pluginWebsite, pluginPriceSymbol, pluginPriceValue);
											
											mapPluginTypeProperties.put(pluginType, pluginTypeProperties);
										}
									}
								}
								
								final PluginPropertiesStreamBuild pluginPropertiesStream = new PluginPropertiesStreamBuild(activated, pluginName, company, author, owner, website, developer, mapPluginTypeProperties);
								
								this.mapPluginProperties.put(pluginName, pluginPropertiesStream);
							}
						}
					}
				}
			}
		}
	}
}
