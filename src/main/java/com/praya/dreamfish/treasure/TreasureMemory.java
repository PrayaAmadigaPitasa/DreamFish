package com.praya.dreamfish.treasure;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.game.TreasureManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;

public final class TreasureMemory extends TreasureManager {

    private static TreasureMemory instance;
    private final TreasureConfig treasureConfig;
    private final Map<UUID, BukkitTask> runnables = new HashMap<>();
    private final Map<UUID, Map<String, Integer>> treasureReceiveCount = new HashMap<>();
    private final Map<UUID, Integer> fishCatchCount = new HashMap<>();

    protected TreasureMemory(DreamFish plugin) {
        super(plugin);

        this.treasureConfig = new TreasureConfig(plugin);
    }

    public static TreasureMemory getInstance() {
        if (instance == null) {
            final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
            instance = new TreasureMemory(plugin);
            return instance;
        }
        return instance;
    }

    public TreasureConfig getTreasureConfig() {
        return treasureConfig;
    }

    @Override
    public Collection<String> getTreasureItemID() {
        return new ArrayList<>(treasureConfig.getTreasureItemMap().keySet()); // Return empty
    }

    @Override
    public Collection<TreasureItem> getTreasureItems() {
        return new ArrayList<>(treasureConfig.getTreasureItemMap().values());
    }

    public Map<UUID, Integer> getFishCatchCount() {
        return fishCatchCount;
    }

    public Map<UUID, Map<String, Integer>> getTreasureReceiveCount() {
        return treasureReceiveCount;
    }

    @Override
    public TreasureItem getTreasureItem(String id) {
        return treasureConfig.getTreasureItemMap().get(id);
    }

    public void createTreasureAnimationTask(Player player, TreasureItem item, Location hookLocation) {
        if (runnables.containsKey(player.getUniqueId())) return;
        final Runnable run = new TreasureAnimationTask(plugin, item, player, hookLocation);
        runnables.put(player.getUniqueId(), Bukkit.getScheduler().runTaskTimer(plugin, run, 1L, 3L));
    }

    public void stopTreasureAnimationTask(Player player) {
        if (!runnables.containsKey(player.getUniqueId())) return;
        runnables.get(player.getUniqueId()).cancel();
        runnables.remove(player.getUniqueId());
    }
}
