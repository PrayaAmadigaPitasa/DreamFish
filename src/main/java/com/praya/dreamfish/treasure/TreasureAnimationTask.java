package com.praya.dreamfish.treasure;

import com.gmail.nossr50.util.ItemUtils;
import com.gmail.nossr50.util.skills.ParticleEffectUtils;
import com.google.common.util.concurrent.AtomicDouble;
import com.praya.agarthalib.utility.*;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerTask;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.placeholder.PlaceholderMemory;
import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import org.bukkit.*;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.util.Vector;

import java.util.Collection;

public class TreasureAnimationTask extends HandlerTask implements Runnable {

    private TreasureItem item;
    private Player target;
    private Location hookLocation;
    private Vector vector;
    private int increment;
    private final int incrementTotal;
    private int particleIncrement;
    private final int particleIncrementTotal;
    private ArmorStand armorStand;
    private final AtomicDouble atomic = new AtomicDouble();

    protected TreasureAnimationTask(DreamFish plugin, TreasureItem item, Player target, Location hookLocation) {
        super(plugin);
        this.item = item;
        this.target = target;
        this.hookLocation = hookLocation;
        this.incrementTotal = 10;
        this.particleIncrementTotal = 15;
        this.vector = new Vector(0D, 0.5D, 0D);
        this.armorStand = (ArmorStand) EntityUtil.addEntity(hookLocation.clone().subtract(new Vector(0, 2.0D, 0)), EntityType.ARMOR_STAND);
        final MessageBuild firstGet = Language.TREASURE_FIRST_GET.getMessage(target);

        // Setup armorstand
        armorStand.setCustomName(TextUtil.colorful(Language.TREASURE_GET.getText()));
        armorStand.setVisible(false);
        armorStand.setInvulnerable(true);
        armorStand.setGravity(false);
        armorStand.setCustomNameVisible(false);

        firstGet.sendMessage(target);
        SenderUtil.playSound(target, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
    }

    @Override
    public void run() {
        final Collection<Player> players = PlayerUtil.getNearbyPlayers(armorStand.getLocation(), 5);
        if (particleIncrement >= particleIncrementTotal) {
            if (armorStand.getHelmet() == null || armorStand.getHelmet().getType().equals(Material.AIR)) {
                final Material headMaterial = MaterialUtil.getMaterial(item.getBlockPreview());
                armorStand.setHelmet(new ItemStack(headMaterial));
            }
            if (increment >= incrementTotal) {
                stop();
                return;
            }
            final Location to = armorStand.getLocation().add(vector);
            to.setYaw((float) atomic.getAndAdd(15) % 360);
            armorStand.teleport(to);

            SenderUtil.playSound(target, SoundEnum.ENTITY_ITEM_PICKUP);
            Bridge.getBridgeParticle().playParticle(players, ParticleEnum.ENCHANTMENT_TABLE, armorStand.getLocation(), 10, 0.15, 0.3, 0.15, 0.05F);
            increment++;
            return;
        }
        Bridge.getBridgeParticle().playParticle(players, ParticleEnum.WATER_BUBBLE, hookLocation, 10, 0.15, 0.2, 0.15, 0.05F);
        Bridge.getBridgeParticle().playParticle(players, ParticleEnum.WATER_WAKE, hookLocation, 10, 0.15, 0.3, 0.15, 0.05F);
        particleIncrement++;
    }

    private void stop() {
        final TreasureMemory treasureMemory = TreasureMemory.getInstance();
        final MessageBuild transferFailed = Language.TREASURE_ITEM_TRANSFER_FAILED.getMessage(target);
        final MessageBuild transferSuccess = Language.TREASURE_ITEM_TRANSFER_SUCCESS.getMessage(target);
        final PlaceholderMemory placeholderMemory = PlaceholderMemory.getInstance();
        SenderUtil.playSound(target, SoundEnum.ENTITY_PLAYER_LEVELUP);

        treasureMemory.stopTreasureAnimationTask(target);
        armorStand.setCustomNameVisible(true);

        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
            SenderUtil.playSound(target, SoundEnum.ENTITY_ITEM_PICKUP);
            if (InventoryUtil.getFirstEmptyBackpack(target.getInventory()) == -1) {
                // If inventory is full
                transferFailed.sendMessage(target);
                armorStand.getLocation().getWorld().dropItem(armorStand.getLocation().clone().add(.5, 1, .5), item.getItemReward());
            } else {
                // If not full
                if (!item.getMessageOnGet().equalsIgnoreCase("-")) {
                    // If the message is specified
                    String message = item.getMessageOnGet();
                    if (message.contains("{prefix}")) message = message.replace("{prefix}", placeholderMemory.getPlaceholder("prefix"));
                    SenderUtil.sendMessage(target, message);
                } else {
                    transferSuccess.sendMessage(target);
                }

                PlayerUtil.addItem(target, item.getItemReward());
            }
        }, 40L);
        final Firework fw = (Firework) EntityUtil.addEntity(armorStand.getLocation(), EntityType.FIREWORK);
        final FireworkMeta fwm = fw.getFireworkMeta();

        fwm.setPower(1);
        fwm.addEffect(FireworkEffect.builder().with(FireworkEffect.Type.BALL).withColor(Color.BLUE).withFade(Color.AQUA).withTrail().flicker(true).build());

        fw.setFireworkMeta(fwm);
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, fw::detonate, 20L);
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
            WorldUtil.createExplosion(hookLocation, 5, false, false);
            armorStand.remove();
        }, 40L);
    }
}
