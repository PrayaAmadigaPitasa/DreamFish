package com.praya.dreamfish.treasure;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.event.PlayerCatchFishEvent;
import com.praya.dreamfish.event.PlayerHookFishEvent;
import com.praya.dreamfish.fish.FishMemory;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.handler.HandlerEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.*;

public class TreasureListener extends HandlerEvent implements Listener {

    public TreasureListener(DreamFish plugin) {
        super(plugin);
    }

    @EventHandler
    public void onHook(PlayerHookFishEvent e) {
        final Player player = e.getPlayer();
        final TreasureMemory treasureMemory = TreasureMemory.getInstance();
        final DreamFishConfig config = plugin.getMainConfig();
        final Location hookLocation = e.getHook().getLocation();
        if (!config.isTreasureEnablePluginTreasure()) return;
        for (TreasureItem item : treasureMemory.getTreasureItems()) {
            final String[] chance = item.getChance().split("/");
            final double minChance = Double.parseDouble(chance[0]);
            final double maxChance = Double.parseDouble(chance[1]);

            if (MathUtil.chanceOf(minChance, maxChance)) {
                if (item.isUltraRare()) {
                    boolean shouldAddTreasure;

                    if (treasureMemory.getTreasureReceiveCount().containsKey(player.getUniqueId())) {
                        final Map<String, Integer> counterMap = treasureMemory.getTreasureReceiveCount().get(player.getUniqueId());
                        if (counterMap.containsKey(item.getTreasureID())) {
                            final int treasureReceive = counterMap.get(item.getTreasureID());
                            if ((treasureReceive + 1) > item.getAmountUntilActive()) {
                                shouldAddTreasure = false;
                            } else {
                                counterMap.put(item.getTreasureID(), treasureReceive + 1);
                                shouldAddTreasure = true;
                            }
                        } else {
                            counterMap.put(item.getTreasureID(), 1);
                            shouldAddTreasure = true;
                        }
                        treasureMemory.getTreasureReceiveCount().put(player.getUniqueId(), counterMap);
                    } else {
                        final Map<String, Integer> map = new HashMap<>();
                        map.put(item.getTreasureID(), 1);
                        treasureMemory.getTreasureReceiveCount().put(player.getUniqueId(), map);
                        shouldAddTreasure = true;
                    }

                    if (shouldAddTreasure) {
                        if (config.isTreasureEnableReplaceFish()) {
                            e.setCancelled(true);
                            treasureMemory.createTreasureAnimationTask(player, item, hookLocation);
                            break;
                        } else {
                            // Auto add fishnya
                            e.setCancelled(true);
                            final FishProperties drop = FishMemory.getInstance().getFishProperties(e.getFish());
                            player.getInventory().addItem(drop.getItem());
                            treasureMemory.createTreasureAnimationTask(player, item, hookLocation);
                            break;
                        }
                    }
                } else {
                    if (config.isTreasureEnableReplaceFish()) {
                        e.setCancelled(true);
                        treasureMemory.createTreasureAnimationTask(player, item, hookLocation);
                    } else {
                        // Auto add fishnya
                        e.setCancelled(true);
                        final FishProperties drop = FishMemory.getInstance().getFishProperties(e.getFish());
                        player.getInventory().addItem(drop.getItem());
                        treasureMemory.createTreasureAnimationTask(player, item, hookLocation);
                    }
                }
                break;
            }
        }
    }

    @EventHandler
    public void onCatch(PlayerCatchFishEvent e) {
        final Player player = e.getPlayer();
        final TreasureMemory treasureMemory = TreasureMemory.getInstance();
        final DreamFishConfig config = plugin.getMainConfig();
        if (!config.isTreasureEnablePluginTreasure()) return;

        if (treasureMemory.getTreasureReceiveCount().containsKey(player.getUniqueId())) {
            final Map<String, Integer> counterMap = treasureMemory.getTreasureReceiveCount().get(player.getUniqueId());
            final Map<UUID, Integer> catchCount = treasureMemory.getFishCatchCount();
            if (!counterMap.isEmpty()) {
                if (!catchCount.containsKey(player.getUniqueId())) {
                    catchCount.put(player.getUniqueId(), 1);
                } else {
                    catchCount.put(player.getUniqueId(), catchCount.get(player.getUniqueId()) + 1);
                }
                final int updatedCatchCount = catchCount.get(player.getUniqueId());
                Iterator<Map.Entry<String, Integer>> ite = counterMap.entrySet().iterator();
                while (ite.hasNext()) {
                    final Map.Entry ent = ite.next();
                    final String treasureID = (String) ent.getKey();
                    final int count = (int) ent.getValue();
                    final TreasureItem treasure = TreasureMemory.getInstance().getTreasureItem(treasureID);

                    if ((count + 1) >= treasure.getAmountUntilActive()) {
                        // Batas maksimal untuk catch treasure udah max. Sekarang kita lanjut cek apakah ini siap di remove atau tidak
                        if (updatedCatchCount >= treasure.getBlockTime()) {
                            // Catch count sudah melebihi batas untuk remove player dari blocked list. Sekarang remove
                            ite.remove();
                        }
                    }
                }
            }
            // Update
            treasureMemory.getTreasureReceiveCount().put(player.getUniqueId(), counterMap);
            treasureMemory.getFishCatchCount().put(player.getUniqueId(), catchCount.get(player.getUniqueId()));
        }
    }
}
