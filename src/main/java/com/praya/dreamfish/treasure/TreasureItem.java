package com.praya.dreamfish.treasure;

import com.praya.agarthalib.utility.ConfigUtil;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

public class TreasureItem {

    private static final String CONFIG_KEY = "Treasures.";

    private final ItemStack itemReward;
    private final String chance; // Misal : 10/100 : nanti di split
    private final String commandReward;
    private final String treasureID;
    private final String messageOnGet;
    private final String blockPreview;
    private final boolean ultraRare;
    private final int blockTime;
    private final int amountUntilActive;
    private final int itemAmount;

    public TreasureItem(String key, FileConfiguration config) {
        this.treasureID = key;
        this.itemReward = ConfigUtil.getItemStack(config.getConfigurationSection(CONFIG_KEY  + key + ".itemReward"));
        this.chance = config.getString(CONFIG_KEY + key + ".chance");
        this.commandReward = (config.getString(CONFIG_KEY + key + ".commandReward").equals("-")) ? "-" : config.getString(CONFIG_KEY + key + ".commandReward");
        this.messageOnGet = (config.getString(CONFIG_KEY + key + ".message").equals("-")) ? "-" : config.getString(CONFIG_KEY + key + ".message");
        this.ultraRare = config.getBoolean(CONFIG_KEY + key + ".ultraRare.enabled");
        this.blockTime = config.getInt(CONFIG_KEY + key + ".ultraRare.blockTime");
        this.amountUntilActive = config.getInt(CONFIG_KEY + key + ".ultraRare.amountUntilActive");
        this.blockPreview = config.getString(CONFIG_KEY + key + ".blockPreview");
        this.itemAmount = config.getInt(CONFIG_KEY + key + ".itemReward.Amount");

        itemReward.setAmount(itemAmount);
    }

    public final int getItemAmount() {
        return itemAmount;
    }

    public final String getBlockPreview() {
        return blockPreview;
    }

    public final boolean isUltraRare() {
        return ultraRare;
    }

    public final int getBlockTime() {
        return blockTime;
    }

    public final int getAmountUntilActive() {
        return amountUntilActive;
    }

    public final ItemStack getItemReward() {
        return itemReward;
    }

    public final String getChance() {
        return chance;
    }

    public final String getCommandReward() {
        return commandReward;
    }

    public final String getTreasureID() {
        return treasureID;
    }

    public final String getMessageOnGet() {
        return messageOnGet;
    }
}
