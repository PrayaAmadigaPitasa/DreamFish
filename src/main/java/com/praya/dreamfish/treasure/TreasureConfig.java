package com.praya.dreamfish.treasure;

import com.praya.agarthalib.utility.ConfigUtil;
import com.praya.agarthalib.utility.FileUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerConfig;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public final class TreasureConfig extends HandlerConfig {

    private static final String PATH_FILE = "Configuration/treasure.yml";

    private final Map<String, TreasureItem> mapTreasure = new HashMap<String, TreasureItem>();

    protected TreasureConfig(DreamFish plugin) {
        super(plugin);
        setup();
    }

    @Override
    public final void setup() {
    	clearCache();
    	loadConfig();
    }

    private final void clearCache() {
    	this.mapTreasure.clear();
    }

    private final void loadConfig() {
        final File file = FileUtil.getFile(plugin, PATH_FILE);

        if (!file.exists()) {
            FileUtil.saveResource(plugin, PATH_FILE);
        }

        final FileConfiguration config = FileUtil.getFileConfiguration(file);

        for (String key : config.getConfigurationSection("Treasures").getKeys(false)) {
            // Build the item
            mapTreasure.put(key, new TreasureItem(key, config));
        }
    }

    public Map<String, TreasureItem> getTreasureItemMap() {
        return mapTreasure;
    }
}