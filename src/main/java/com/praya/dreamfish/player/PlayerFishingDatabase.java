package com.praya.dreamfish.player;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerDatabase;

public abstract class PlayerFishingDatabase extends HandlerDatabase {
	
	protected PlayerFishingDatabase(DreamFish plugin) {
		super(plugin);
	}
	
	protected abstract List<String> getAllId() throws SQLException;
	protected abstract List<String> getAllData() throws SQLException;
	protected abstract String getData(String id) throws SQLException;
	protected abstract boolean isExists(String id) throws SQLException;
	protected abstract PlayerFishing getPlayerFishing(String id) throws SQLException;
	protected abstract boolean saveData(PlayerFishing playerFishing);
	protected abstract boolean deleteData(String id);
	
	protected final String getData(OfflinePlayer player) throws SQLException {
		return player != null ? getData(player.getUniqueId()) : null;
	}
	
	protected final String getData(UUID playerId) throws SQLException {
		return playerId != null ? getData(playerId.toString()) : null;
	}
	
	protected final boolean isExists(OfflinePlayer player) throws SQLException {
		return player != null ? isExists(player.getUniqueId()) : false;  
	}
	
	protected final boolean isExists(UUID playerId) throws SQLException {
		return playerId != null ? isExists(playerId.toString()) : false;  
	}
	
	protected final PlayerFishing getPlayerFishing(OfflinePlayer player) throws SQLException {
		return player != null ? getPlayerFishing(player.getUniqueId()) : null;
	}
	
	protected final PlayerFishing getPlayerFishing(UUID playerId) throws SQLException {
		return playerId != null ? getPlayerFishing(playerId.toString()) : null;
	}
	
	
	protected final boolean deleteData(OfflinePlayer player) {
		return player != null ? deleteData(player.getUniqueId()) : false;
	}
	
	protected final boolean deleteData(UUID playerId) {
		return playerId != null ? deleteData(playerId.toString()) : false;
	}
}