package com.praya.dreamfish.player;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.manager.task.TaskPlayerFishingManager;
import com.praya.dreamfish.player.PlayerFishingDatabaseTask;

public final class PlayerFishingTaskMemory extends TaskPlayerFishingManager {

	private BukkitTask taskPlayerFishingDatabase;
	
	private PlayerFishingTaskMemory(DreamFish plugin) {
		super(plugin);
		
		reloadTaskPlayerFishingDatabase();
	}
	
	private static class PlayerFishingTaskMemorySingleton {
		private static final PlayerFishingTaskMemory instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			instance = new PlayerFishingTaskMemory(plugin);
		}
	}
	
	public static final PlayerFishingTaskMemory getInstance() {
		return PlayerFishingTaskMemorySingleton.instance;
	}
	
	@Override
	public final void reloadTaskPlayerFishingDatabase() {
		if (this.taskPlayerFishingDatabase != null) {
			this.taskPlayerFishingDatabase.cancel();
		}
		
		this.taskPlayerFishingDatabase = createTaskPlayerFishingDatabase();
	}
	
	private final BukkitTask createTaskPlayerFishingDatabase() {
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		final BukkitScheduler scheduler = Bukkit.getScheduler();
		final Runnable runnable = new PlayerFishingDatabaseTask(plugin);
		final int period = mainConfig.getDatabasePeriodSave() * 20;
		final BukkitTask task = scheduler.runTaskTimerAsynchronously(plugin, runnable, period, period);
		
		return task;
	}
}
