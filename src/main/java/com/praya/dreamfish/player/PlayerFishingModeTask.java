package com.praya.dreamfish.player;

import java.util.Collection;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.handler.HandlerTask;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.player.PlayerFishingModeManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.BossBar.Color;
import core.praya.agarthalib.builder.message.BossBar.Style;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.LocationUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.MetadataUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.agarthalib.utility.VectorUtil;

public final class PlayerFishingModeTask extends HandlerTask implements Runnable {
	
	private static final HashMap<String, String> mapPlaceholder = new HashMap<String, String>(); 
	
	private final PlayerFishingMode playerFishingMode;
	private final Collection<Player> players;
	private final double maxTension;
	
	private double x = 0;
	private double y = 0;
	private double z = 0;
	
	protected PlayerFishingModeTask(DreamFish plugin, PlayerFishingMode playerFishingMode) {
		super(plugin);
		
		if (playerFishingMode == null) {
			throw new IllegalArgumentException();
		} else {
			final PlayerManager playerManager = plugin.getPlayerManager();
			final PlayerFishingModeManager playerFishingModeManager = playerManager.getPlayerFishingModeManager();
			final DreamFishConfig mainConfig = plugin.getMainConfig();;
			final Player player = playerFishingMode.getPlayer();
			final Entity hook = playerFishingMode.getHook();
			final Location location = hook.getLocation();
			final double effectRange = mainConfig.getEffectRange();
			
			this.playerFishingMode = playerFishingMode;
			this.players = PlayerUtil.getNearbyPlayers(location, effectRange);
			this.maxTension = playerFishingModeManager.getPlayerLuresMaxTension(player);
		}
	}
	
	public final PlayerFishingMode getPlayerFishingMode() {
		return this.playerFishingMode;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final PlayerFishingMode playerFishingMode = getPlayerFishingMode();
		final Player player = playerFishingMode.getPlayer();
		final Entity hook = playerFishingMode.getHook();
		final Entity fishEntity = playerFishingMode.getFishEntity();
		
		if (playerFishingMode.validate()) {
			final PlayerFishingModeProperties fishingModeProperties = playerFishingMode.getFishingModeProperties();
			final FishProperties fishProperties = playerFishingMode.getFishProperties();
			final double tension = fishingModeProperties.getTension();
			final double fishPower = fishingModeProperties.getFishPower();
			final double maxFishPower = fishProperties.getMaxPower();
			final double resistance = fishProperties.getResistance();
			final double limitDive = fishProperties.getMaxDive();
			final double limitMove = fishProperties.getMaxSpeed();
			final double baseDive = fishProperties.getType().equals(EntityType.SQUID) || fishProperties.getType().equals(EntityType.GUARDIAN) ? 0.02: 0.05;
			
			fishingModeProperties.addTension(MathUtil.chanceOf(resistance) ? (Math.random()*2) : (Math.random()*-2.5));
			fishingModeProperties.addFishPower(((Math.random()-0.25)/5));
			
			if (tension >= maxTension || fishPower >= maxFishPower) {
				final MessageBuild message = Language.FISHING_LURE_BROKEN.getMessage(player);
				
				message.sendMessage(player);
				Bridge.getBridgeMessage().sendActionbar(player, "");
				SenderUtil.playSound(player, SoundEnum.BLOCK_NOTE_SNARE);
				playerFishingMode.unregister();
				return;
			} else {
				final String informationID = "DreamFish Fishing Mode";
				final String bar = mainConfig.getBarSymbol();
				final String barLife = mainConfig.getBarFill();
				final String barEmpty = mainConfig.getBarEmpty();
				final int totalBarLife = mainConfig.getBarLength();
				final double chanceTurnAround = mainConfig.getFishDefaultChanceTurnAround();
				final boolean fishAggressive = mainConfig.isFishEnableAggresive();
				final boolean fishClever = mainConfig.isFishEnableClever();	
				final int informationType = mainConfig.getFishingInformationType();
				final int cooldown = 3000;
				final int fireTicks = fishEntity.getFireTicks();
				
				if (fireTicks > 0) {
					fishEntity.setFireTicks(0);
				}
				
				if (fishPower > 0) {
					final String metadataPullID = "DreamFish Pull";
					final Location base = fishEntity.getLocation();
					
					if (!MetadataUtil.isCooldown(player, metadataPullID)) {
						final Location loc = LocationUtil.getNewLocation(base);
						final EntityType type = fishEntity.getType();
						final double multiplierFall = getMultiplierDive(type);
						
						x = x + ((Math.random()-0.5)/20);
						y = -baseDive + (((Math.random()-0.5)/20));
						z = z + ((Math.random()-0.5)/20);
						
						if (MathUtil.chanceOf(chanceTurnAround, 1D)) {
							x = -x;
							y = -y;
							z = -z;
						}
						
						if (fishAggressive) {
							if (x < (limitMove/2) && x > -(limitMove/2)) {
								x = x > 0 ? (limitMove/2) : -(limitMove/2);
							}
							
							if (z < (limitMove/2) && x > -(limitMove/2)) {
								z = z > 0 ? (limitMove/2) : -(limitMove/2);
							}
						}
						
						if (fishClever) {
							final Vector away = VectorUtil.getRelativeHorizontal(player.getLocation(), base, true).multiply(0.05D);
							
							x = x + away.getX();
							z = z + away.getZ();
						}
						
						x = MathUtil.limitDouble(x, -limitMove, limitMove);
						y = y > 0 ? MathUtil.limitDouble(y, -limitDive, limitDive) : MathUtil.limitDouble(y, (-baseDive -limitDive), 0);
						z = MathUtil.limitDouble(z, -limitMove, limitMove);
						
						loc.add((x > 0 ? x+1 : x-1), 0, 0);
						
						if (MaterialEnum.isFluid(loc)) {
							loc.subtract((x > 0 ? 1 : -1), 0, 0);
						} else {
							loc.subtract((x > 0 ? x+1 : x-1), 0, 0);
							
							if (fishAggressive) {
								x = -x;
								 
								loc.add((x > 0 ? x+1 : x-1), 0, 0);
								
								if (MaterialEnum.isFluid(loc)) {
									loc.subtract((x > 0 ? 1 : -1), 0, 0);
								} else {
									loc.subtract((x > 0 ? x+1 : x-1), 0, 0);
								}
							}
						}
						
						loc.add(0, 0, (z > 0 ? z+1 : z-1));
						
						if (MaterialEnum.isFluid(loc)) {
							loc.subtract(0, 0, (z > 0 ? 1 : -1));
						} else {
							loc.subtract(0, 0, (z > 0 ? z+1 : z-1));
							
							if (fishAggressive) {
								z = -z;
								
								if (MaterialEnum.isFluid(loc)) {
									loc.subtract(0, 0, (z > 0 ? 1 : -1));
								} else {
									loc.subtract(0, 0, (z > 0 ? z+1 : z-1));
								}
							}
						}
						
						final Vector relative = loc.subtract(base).toVector();
						final Vector velocity = fishEntity.getVelocity();
						
						base.setDirection(relative);
						fishEntity.teleport(base);
						
						if (fishEntity.getPassenger() == null) {
							fishEntity.setPassenger(hook);
						}
						
						if (velocity.add(relative).length() > limitMove) {
							velocity.normalize().multiply(limitMove);
						}
						
						if (y < 0) {
							if (MaterialEnum.isFluid(base.getBlock().getRelative(BlockFace.DOWN))) {
								velocity.setY(y);
							} else if (MaterialEnum.isFluid(base)) {
								final double heightInDepth = base.getY() % 1;
								final double speedY = heightInDepth > 0.5 ? -(heightInDepth - 0.5) / 2.5 : 0;
								
								velocity.setY(speedY);
							}
						} else {
							if (MaterialEnum.isFluid(base.getBlock().getRelative(BlockFace.UP))) {
								velocity.setY(y);
							} else {
								final double heightSurface = base.getY() % 1;
								final double speedY = heightSurface > 0.5 ? -(heightSurface - 0.5) * multiplierFall / 2.5 : 0;
								
								velocity.setY(speedY);
							}
						}
						
						fishEntity.setVelocity(velocity);
						
						Bridge.getBridgeParticle().playParticle(players, ParticleEnum.WATER_BUBBLE, base, 10, 0.15, 0.2, 0.15, 0.05F);
						Bridge.getBridgeParticle().playParticle(players, ParticleEnum.WATER_WAKE, base, 10, 0.15, 0.3, 0.15, 0.05F);
					}
					
					if (informationType != -1) {
						final double distance = base.distance(player.getLocation());
						
						mapPlaceholder.clear();
						mapPlaceholder.put("distance", String.valueOf(MathUtil.roundNumber(distance, 2)));
						mapPlaceholder.put("fishPower", String.valueOf(MathUtil.roundNumber(fishPower, 2)));
						
						if (informationType == 0 || informationType == 1) {
							final int priorityActionbar = mainConfig.getPriorityActionbar();
							final String tension_bar = TextUtil.getBar(bar, totalBarLife, (tension/maxTension), barLife, barEmpty);
							
							String message = mainConfig.getFishingFormatActionbarFight();										
							
							mapPlaceholder.put("tension_bar", tension_bar);
							message = TextUtil.placeholder(mapPlaceholder, message);
							
							Bridge.getBridgeMessage().sendProtectedActionbar(player, message, informationID, priorityActionbar, cooldown);
						}
						
						if (informationType == 0 || informationType == 2) {
							final int priorityBossBar = mainConfig.getPriorityBossBar();
							final Color color = mainConfig.getBossBarColor();
							final Style style = mainConfig.getBossBarStyle();
							final float progress = (float) (tension/maxTension);
							
							String message = mainConfig.getFishingFormatBossBarFight();
							
							message = TextUtil.placeholder(mapPlaceholder, message);
							
							Bridge.getBridgeMessage().sendProtectedBossBar(player, message, informationID, priorityBossBar, cooldown, color, style, progress);
						}
					}
				} else {
					if (informationType != -1) {
						final Location loc = fishEntity.getLocation();
						final double distance = loc.distance(player.getLocation());
						
						mapPlaceholder.clear();
						mapPlaceholder.put("distance", String.valueOf(MathUtil.roundNumber(distance, 2)));
						
						if (informationType == 0 || informationType == 1) {
							final int priorityActionbar = mainConfig.getPriorityActionbar();
							
							String message = mainConfig.getFishingFormatActionbarPull();		
							
							message = TextUtil.placeholder(mapPlaceholder, message);
							
							Bridge.getBridgeMessage().sendProtectedActionbar(player, message, informationID, priorityActionbar, cooldown);
						}
						
						if (informationType == 0 || informationType == 2) {
							final int priorityBossBar = mainConfig.getPriorityBossBar();
							final Color color = mainConfig.getBossBarColor();
							final Style style = mainConfig.getBossBarStyle();
							
							String message = mainConfig.getFishingFormatBossBarPull();						
														
							mapPlaceholder.clear();
							mapPlaceholder.put("distance", String.valueOf(MathUtil.roundNumber(distance, 2)));
							message = TextUtil.placeholder(mapPlaceholder, message);
							
							Bridge.getBridgeMessage().sendProtectedBossBar(player, message, informationID, priorityBossBar, cooldown, color, style, 0f);
						}
					}
				}
			}
		}
	}
	
	private final double getMultiplierDive(EntityType type) {
		if (type != null) {
			switch (type.toString()) {
			case "BLAZE" : return 1.2;
			case "GHAST" : return 1.5;
			case "PHANTOM" : return 2.0;
			case "ENDER_DRAGON" : return 1.5;
			default : return 1.0;
			}
		}
		
		return 1.0;
	}
}
