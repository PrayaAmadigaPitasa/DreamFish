package com.praya.dreamfish.player;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

public class PlayerBait {

	private final UUID playerId;
	
	private String markBait;
	private String hookBait;
	
	protected PlayerBait(OfflinePlayer player) {
		this(player, null, null);
	}
	
	protected PlayerBait(OfflinePlayer player, String markBait, String hookBait) {
		this(player.getUniqueId(), markBait, hookBait);
	}
	
	protected PlayerBait(UUID playerId, String markBait, String hookBait) {
		if (playerId == null) {
			throw new IllegalArgumentException();
		} else {
			this.playerId = playerId;
			this.markBait = markBait;
			this.hookBait = hookBait;
		}
	}
	
	public final UUID getPlayerId() {
		return this.playerId;
	}
	
	public final String getMarkBait() {
		return this.markBait;
	}
	
	public final boolean hasMarkBait() {
		return getMarkBait() != null;
	}
	
	public final void setMarkBait(String bait) {
		this.markBait = bait;
	}
	
	public final void removeMarkBait() {
		this.markBait = null;
	}
	
	public final String getHookBait() {
		return this.hookBait;
	}
	
	public final boolean hasHookBait() {
		return getHookBait() != null;
	}
	
	public final void setHookBait(String bait) {
		this.hookBait = bait;
	}
	
	public final void removeHookBait() {
		this.hookBait = null;
	}
	
	public final void clearBait() {
		removeMarkBait();
		removeHookBait();
	}
}
