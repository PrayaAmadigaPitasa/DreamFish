package com.praya.dreamfish.player;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.player.PlayerBaitManager;

public final class PlayerBaitMemory extends PlayerBaitManager {
	
	private final HashMap<UUID, PlayerBait> mapPlayerBait = new HashMap<UUID, PlayerBait>();
	
	private PlayerBaitMemory(DreamFish plugin) {
		super(plugin);
	};
	
	private static class PlayerBaitMemorySingleton {
		private static final PlayerBaitMemory instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			instance = new PlayerBaitMemory(plugin);
		}
	}
	
	public static final PlayerBaitMemory getInstance() {
		return PlayerBaitMemorySingleton.instance;
	}
	
	@Override
	public final PlayerBait getPlayerBait(OfflinePlayer player) {
		if (player != null) {
			final UUID playerId = player.getUniqueId();
			final PlayerBait playerBait = this.mapPlayerBait.get(playerId);
			
			if (playerBait != null) {
				return playerBait;
			} else {
				final PlayerBait playerBaitNew = new PlayerBait(player);
				
				this.mapPlayerBait.put(playerId, playerBaitNew);
				
				return playerBaitNew;
			}
		} else {
			return null;
		}
	}
}
