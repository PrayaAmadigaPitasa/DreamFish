package com.praya.dreamfish.player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import com.praya.agarthalib.utility.ServerUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.player.PlayerBaitManager;
import com.praya.dreamfish.manager.player.PlayerFishingModeManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerFishingMode;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.VersionNMS;

public class PlayerFishingModeMemory extends PlayerFishingModeManager {

	private final HashMap<UUID, PlayerFishingMode> mapPlayerFishingMode = new HashMap<UUID, PlayerFishingMode>(); 
	
	private PlayerFishingModeMemory(DreamFish plugin) {
		super(plugin);
	};
	
	private static class PlayerFishingModeMemorySingleton {
		private static final PlayerFishingModeMemory instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			instance = new PlayerFishingModeMemory(plugin);
		}
	}
	
	public static final PlayerFishingModeMemory getInstance() {
		return PlayerFishingModeMemorySingleton.instance;
	}
	
	@Override
	public final Collection<UUID> getPlayerIds() {
		return getPlayerIds(true);
	}
	
	protected final Collection<UUID> getPlayerIds(boolean clone) {
		final Collection<UUID> playerIds = this.mapPlayerFishingMode.keySet();
		
		return clone ? new ArrayList<UUID>(playerIds) : playerIds;
	}
	
	@Override
	public final Collection<PlayerFishingMode> getAllPlayerFishingMode() {
		return this.mapPlayerFishingMode.values();
	}
	
	protected final Collection<PlayerFishingMode> getAllPlayerFishingMode(boolean clone) {
		final Collection<PlayerFishingMode> allPlayerFishingMode = this.mapPlayerFishingMode.values();
		
		return clone ? new ArrayList<PlayerFishingMode>(allPlayerFishingMode) : allPlayerFishingMode;
	}

	@Override
	public final PlayerFishingMode getPlayerFishingMode(Player player) {
		if (player != null) {
			final UUID playerId = player.getUniqueId();
			
			return this.mapPlayerFishingMode.get(playerId);
		} else {
			return null;
		}
	}
	
	protected final boolean register(PlayerFishingMode playerFishingMode) {
		if (playerFishingMode != null) {
			final Player player = playerFishingMode.getPlayer();
			final UUID playerId = player.getUniqueId();
			
			this.mapPlayerFishingMode.put(playerId, playerFishingMode);
			
			return true;
		} else {
			return false;
		}
	}
	
	protected final boolean unregister(PlayerFishingMode playerFishingMode) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerBaitManager playerBaitManager = playerManager.getPlayerBaitManager();
		
		if (playerFishingMode != null && getAllPlayerFishingMode(false).contains(playerFishingMode)) {
			final Player player = playerFishingMode.getPlayer();
			final Entity entityHook = playerFishingMode.getHook();
			final Entity entityFish = playerFishingMode.getFishEntity();
			final UUID playerId = player.getUniqueId();
			final String informationID = "DreamFish Fishing Mode";
			final PlayerBait playerBait = playerBaitManager.getPlayerBait(player);
			final PlayerFishingModeAction playerFishingModeAction = playerFishingMode.getPlayerFishingModeAction();
			final BukkitTask taskFishing = playerFishingMode.getTaskFishing();
			
			if (!ServerUtil.isCompatible(VersionNMS.V1_9_R1)) {
				playerFishingModeAction.setAntiBug(true);
			}
			
			taskFishing.cancel();
			playerBait.removeHookBait();
			entityHook.remove();
			entityFish.remove();
			Bridge.getBridgeMessage().removeBossBar(player, informationID);
			
			this.mapPlayerFishingMode.remove(playerId);
			
			return true;
		} else {
			return false;
		}
	}
}
