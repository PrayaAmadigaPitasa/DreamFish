package com.praya.dreamfish.player;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

public final class PlayerFishingModeAction {

	private final UUID playerId;
	
	private boolean antiBug;
	private long timeCooldown;
	
	protected PlayerFishingModeAction(OfflinePlayer player) {
		if (player == null) {
			throw new IllegalArgumentException();
		} else {
			final UUID playerId = player.getUniqueId();
			
			this.playerId = playerId;
			this.antiBug = false;
			this.timeCooldown = 0;
		}
	}
	
	public final UUID getPlayerId() {
		return this.playerId;
	}
	
	public final boolean hasAntiBug() {
		return this.antiBug;
	}
	
	public final void setAntiBug(boolean toggle) {
		this.antiBug = toggle;
	}
	
	public final boolean isActionCooldown() {
		final long now = System.currentTimeMillis();
		
		return now < this.timeCooldown;
	}
	
	public final void setActionCooldown() {
		final long timeCooldown = System.currentTimeMillis() + 50L;
		
		this.timeCooldown = timeCooldown;
	}
}
