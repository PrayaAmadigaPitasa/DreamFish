package com.praya.dreamfish.player;

import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.player.PlayerFishingModeManager;
import com.praya.dreamfish.manager.player.PlayerManager;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;

import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.MetadataUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

public final class PlayerFishingMode {
	
	private final Player player;
	private final Entity hook;
	private final Entity fishEntity;
	private final FishProperties fishProperties;
	private final PlayerFishingModeAction playerFishingModeAction;
	private final PlayerFishingModeProperties fishingModeProperties;
	private final BukkitTask taskFishing;
	
	@SuppressWarnings("deprecation")
	protected PlayerFishingMode(Player player, Entity hook, FishProperties fishProperties) {
		final PlayerFishingModeMemory playerFishingModeMemory = PlayerFishingModeMemory.getInstance();
				
		if (player == null || hook == null || fishProperties == null) {
			throw new IllegalArgumentException();
		} else if (playerFishingModeMemory.isFishing(player)) {
			throw new IllegalStateException();
		} else {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			final DreamFishConfig mainConfig = plugin.getMainConfig();
			final EntityType fishType = fishProperties.getType();
			final String fish = fishProperties.getId();
			final String fishFormatName = mainConfig.getFishFormatName();
			final String fishName = TextUtil.placeholder(TextUtil.colorful(fishFormatName), "fish", fish);
			final String metadataFishID = "DreamFish Fish";
			final MetadataValue metadataFishValue = MetadataUtil.createMetadata(true);
			final boolean enableFishName = mainConfig.isFishEnableName();
			final boolean isInvisible = fishProperties.isInvisible();
			final Location location = hook.getLocation();
			final World world = hook.getWorld();
			final Collection<Player> players = PlayerUtil.getNearbyPlayers(location, 128);
			
			this.player = player;
			this.hook = hook;
			this.fishEntity = world.spawnEntity(location, fishType);
			this.fishProperties = fishProperties;
			this.playerFishingModeAction = new PlayerFishingModeAction(player);
			this.fishingModeProperties = new PlayerFishingModeProperties(fishProperties);
			this.taskFishing = createTaskFishing();
			
			hook.setCustomName(fishName);
			hook.setCustomNameVisible(enableFishName);
			fishEntity.setPassenger(hook);
			fishEntity.setMetadata(metadataFishID, metadataFishValue);
			CombatUtil.applyPotion(fishEntity, PotionEffectType.SLOW, 100000, 10);
			
			if (isInvisible) {
				Bridge.getBridgePlayer().hideEntity(players, fishEntity);
			}
			
			playerFishingModeMemory.register(this);
		}
	}
	
	public final Player getPlayer() {
		return this.player;
	}
	
	public final Entity getHook() {
		return this.hook;
	}
	
	public final Entity getFishEntity() {
		return this.fishEntity;
	}
	
	public final FishProperties getFishProperties() {
		return this.fishProperties;
	}
	
	public final PlayerFishingModeAction getPlayerFishingModeAction() {
		return this.playerFishingModeAction;
	}
	
	public final PlayerFishingModeProperties getFishingModeProperties() {
		return this.fishingModeProperties;
	}
	
	public final double getFishPower() {
		return this.fishingModeProperties.getFishPower();
	}
	
	public final double getTension() {
		return this.fishingModeProperties.getTension();
	}
	
	public final void addFishPower(double additional) {
		this.fishingModeProperties.addFishPower(additional);
	}
	
	public final void addTension(double additional) {
		this.fishingModeProperties.addTension(additional);
	}
	
	protected final BukkitTask getTaskFishing() {
		return this.taskFishing;
	}
	
	private final BukkitTask createTaskFishing() {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final BukkitScheduler scheduler = Bukkit.getScheduler();
		final Runnable runnable = new PlayerFishingModeTask(plugin, this);
		final int delay = 1;
		final int period = 1;
		final BukkitTask task = scheduler.runTaskTimer(plugin, runnable, delay, period);
		
		return task;
	}
	
	protected final boolean validate() {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingModeManager playerFishingModeManager = playerManager.getPlayerFishingModeManager();
		
		if (!player.isOnline()) {
			unregister();
			return false;
		} else if (fishEntity.isDead()) {
			final MessageBuild message = Language.FISHING_FISH_DEAD.getMessage(player);
			
			unregister();
			message.sendMessage(player);
			Bridge.getBridgeMessage().sendActionbar(player, "");
			SenderUtil.playSound(player, SoundEnum.ENTITY_SQUID_DEATH);
			return false;
		} else if (hook.isDead()) {
			final MessageBuild message = Language.FISHING_LURE_BROKEN.getMessage(player);
			
			unregister();
			message.sendMessage(player);
			Bridge.getBridgeMessage().sendActionbar(player, "");
			SenderUtil.playSound(player, SoundEnum.BLOCK_NOTE_SNARE);
			return false;
		} else {
			final Slot slot = playerFishingModeManager.getActiveSlot(player);
			
			if (slot == null) {
				unregister();
				return false;
			} else {
				return true;
			}
		}
	}
	
	public final boolean unregister() {
		final PlayerFishingModeMemory playerFishingModeMemory = PlayerFishingModeMemory.getInstance();
		
		return playerFishingModeMemory.unregister(this);
	}
	
	public static final PlayerFishingMode startFishingMode(Player player, Entity hook, FishProperties fishProperties) {
		final PlayerFishingModeMemory playerFishingModeMemory = PlayerFishingModeMemory.getInstance();
		
		if (!playerFishingModeMemory.isFishing(player)) {
			final PlayerFishingMode playerFishingMode = new PlayerFishingMode(player, hook, fishProperties);
			
			return playerFishingMode;
		} else {
			return null;
		}
	}
}