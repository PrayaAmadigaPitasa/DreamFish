package com.praya.dreamfish.player;

import org.bukkit.entity.Player;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerTask;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.agarthalib.utility.PlayerUtil;

public final class PlayerFishingDatabaseTask extends HandlerTask implements Runnable {

	protected PlayerFishingDatabaseTask(DreamFish plugin) {
		super(plugin);
	}

	@Override
	public void run() {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		
		for (Player player : PlayerUtil.getOnlinePlayers()) {
			final PlayerFishing playerFishing = playerFishingManager.getPlayerFishing(player, false);
			
			if (playerFishing != null) {
				playerFishing.save();
			}
		}
	}
}
