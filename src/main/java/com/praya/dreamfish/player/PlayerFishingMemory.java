package com.praya.dreamfish.player;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.database.DatabaseType;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.player.PlayerFishing;

public final class PlayerFishingMemory extends PlayerFishingManager {

	protected final HashMap<UUID, PlayerFishing> mapPlayerFishing;
	
	private PlayerFishingDatabase playerFishingDatabase;
	
	private PlayerFishingMemory(DreamFish plugin) {
		super(plugin);

		this.mapPlayerFishing = new HashMap<UUID, PlayerFishing>();
		
		setupPlayerFishingDatabase();
	}
	
	private static class PlayerFishingMemorySingleton {
		private static final PlayerFishingMemory instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			instance = new PlayerFishingMemory(plugin);
		}
	}
	
	public static final PlayerFishingMemory getInstance() {
		return PlayerFishingMemorySingleton.instance;
	}
	
	protected final PlayerFishingDatabase getPlayerFishingDatabase() {
		return this.playerFishingDatabase;
	}
	
	@Override
	public final void setupPlayerFishingDatabase() {
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final DatabaseType databaseType = mainConfig.getDatabaseServerType();
		
		if (this.playerFishingDatabase != null) {
			this.playerFishingDatabase.closeConnection();
		}
		
		switch (databaseType) {
		case MySQL:
		case SQLite:
			this.playerFishingDatabase = new PlayerFishingDatabaseSQL(plugin);
			break;
		default: 
			this.playerFishingDatabase = null;
		}
		
		this.mapPlayerFishing.clear();
	}
	
	@Override
	public final Collection<UUID> getPlayerIds() {
		return getPlayerIds(true);
	}
	
	protected final Collection<UUID> getPlayerIds(boolean clone) {
		final Collection<UUID> playerIds = this.mapPlayerFishing.keySet();
		
		return clone ? new ArrayList<UUID>(playerIds) : playerIds;
	}
	
	@Override
	public final Collection<PlayerFishing> getAllPlayerFishing() {
		return this.mapPlayerFishing.values();
	}
	
	protected final Collection<PlayerFishing> getAllPlayerFishing(boolean clone) {
		final Collection<PlayerFishing> allPlayerFishing = this.mapPlayerFishing.values();
		
		return clone ? new ArrayList<PlayerFishing>(allPlayerFishing) : allPlayerFishing;
	}
	
	@Override
	public final PlayerFishing getPlayerFishing(OfflinePlayer player) {
		return getPlayerFishing(player, true);
	}
	
	@Override
	public final PlayerFishing getPlayerFishing(OfflinePlayer player, boolean generate) {
		if (player != null) {
			final UUID playerId = player.getUniqueId();
			final PlayerFishing playerFishing = this.mapPlayerFishing.get(playerId);
			
			if (playerFishing != null) {
				return playerFishing;
			} else {
				try {
					final PlayerFishing playerFishingLoad = getPlayerFishingDatabase().getPlayerFishing(player);
					
					if (playerFishingLoad != null) {
						
						this.mapPlayerFishing.put(playerId, playerFishingLoad);
						
						return playerFishingLoad;
					} else if (generate) {
						final PlayerFishing playerFishingNew = new PlayerFishing(player);
						
						playerFishingNew.save();
						
						return playerFishingNew;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final boolean removeFromCache(OfflinePlayer player) {
		if (player != null) {
			final UUID playerId = player.getUniqueId();
			
			if (getPlayerIds().contains(playerId)) {
				
				this.mapPlayerFishing.remove(playerId);
				
				return true;
			}
		}
		
		return false;
	}
	
	protected final boolean save(PlayerFishing playerFishing) {
		if (getPlayerFishingDatabase().saveData(playerFishing)) {
			final UUID playerId = playerFishing.getPlayerId();
			
			this.mapPlayerFishing.put(playerId, playerFishing);
			
			return true;
		} else {
			return false;
		}
	}
}