package com.praya.dreamfish.player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.event.PlayerFishingExpChangeEvent;
import com.praya.dreamfish.event.PlayerFishingLevelChangeEvent;
import com.praya.dreamfish.event.PlayerFishingExpChangeEvent.ExpChangeReason;
import com.praya.dreamfish.event.PlayerFishingLevelChangeEvent.LevelChangeReason;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.item.ItemRequirement;
import com.praya.dreamfish.manager.game.BaitManager;
import com.praya.dreamfish.manager.game.FishManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.ServerEventUtil;

public class PlayerFishing {

	private final UUID playerId;
	
	private int level;
	private float exp;
	
	public PlayerFishing(OfflinePlayer player) {
		this(player, 1, 0);
	}
	
	public PlayerFishing(OfflinePlayer player, int level, float exp) {
		this(player.getUniqueId(), level, exp);
	}
	
	private PlayerFishing(UUID playerId, int level, float exp) {
		if (playerId == null) {
			throw new IllegalArgumentException();
		} else {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			final DreamFishConfig mainConfig = plugin.getMainConfig();;
			final int maxLevel = mainConfig.getFishingMaxLevel();
			
			this.playerId = playerId;
			this.level = MathUtil.limitInteger(level, 1, maxLevel);
			this.exp = Math.min(exp, getExpToUp(this.level));
		}
	}
	
	public final UUID getPlayerId() {
		return this.playerId;
	}
	
	public final int getLevel() {
		return this.level;
	}
	
	public final void addLevel(int level) {
		addLevel(level, null);
	}
	
	public final void addLevel(int level, LevelChangeReason reason) {
		final int finalLevel = getLevel() + level;
		
		setLevel(finalLevel, reason);;
	}
	
	public final void takeLevel(int level) {
		takeExp(level, null);
	}
	
	public final void takeLevel(int level, LevelChangeReason reason) {
		final int finalLevel = getLevel() - level;
		
		setLevel(finalLevel, reason);;
	}
	
	public final void setLevel(int level) {
		setLevel(level, null);
	}
	
	public final void setLevel(int level, LevelChangeReason reason) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		final int maxLevel = mainConfig.getFishingMaxLevel();
		
		this.level = MathUtil.limitInteger(level, 1, maxLevel);
		
		if (reason != null) {
			final PlayerFishingLevelChangeEvent playerHerbalismLevelChangeEvent = new PlayerFishingLevelChangeEvent(getPlayerId(), level, reason);
			
			ServerEventUtil.callEvent(playerHerbalismLevelChangeEvent);
		}
	}
	
	public final float getExp() {
		return this.exp;
	}
	
	public final void addExp(float exp) {
		addExp(exp, null);
	}
	
	public final void addExp(float exp, ExpChangeReason reason) {
		final float finalExp = getExp() + exp;
		
		setExp(finalExp, reason);
	}
	
	public final void takeExp(float exp) {
		takeExp(exp, null);
	}
	
	public final void takeExp(float exp, ExpChangeReason reason) {
		final float finalExp = getExp() - exp;
		
		setExp(finalExp, reason);
	}
	
	public final void setExp(float exp) {
		setExp(exp, null);
	}
	
	public final void setExp(float exp, ExpChangeReason reason) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		final float playerExp = getExp();
		
		if (playerExp != exp) {
			if (reason != null) {
				final PlayerFishingExpChangeEvent playerHerbalismExpChangeEvent = new PlayerFishingExpChangeEvent(getPlayerId(), exp, reason);
				
				ServerEventUtil.callEvent(playerHerbalismExpChangeEvent);
				
				if (playerHerbalismExpChangeEvent.isCancelled()) {
					return;
				} else {
					exp = playerHerbalismExpChangeEvent.getExp();
				}
			}
			
			final int playerLevel = getLevel();
			final int maxLevel = mainConfig.getFishingMaxLevel();
			
			int level = playerLevel;
			
			while (true) {
				final float expToUp = getExpToUp(level);
				
				if (level < maxLevel) {
					if (exp >= expToUp) {
						exp = exp - expToUp;
						level = level + 1;
					} else {
						break;
					}
				} else {
					exp = Math.min(exp, expToUp);
					break;
				}
			}
			
			if (playerLevel != level) {
				final PlayerFishingLevelChangeEvent playerHerbalismLevelChangeEvent = new PlayerFishingLevelChangeEvent(getPlayerId(), level, LevelChangeReason.EXP_UP);
				
				ServerEventUtil.callEvent(playerHerbalismLevelChangeEvent);
			}
			
			this.exp = exp;
			this.level = level;
		}
	}
	
	public final float getExpToUp() {
		return getExpToUp(getLevel());
	}
	
	public final float getExpToUp(int level) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		final int maxLevel = mainConfig.getFishingMaxLevel();
		final int up = level + 1; 
		
		if (up <= maxLevel) {
			final double factorA = mainConfig.getFishingFormulaExpA();
			final double factorB = mainConfig.getFishingFormulaExpB();
			final double factorC = mainConfig.getFishingFormulaExpC();
			final float expToUp = (float) ((factorA * (up*up)) + (factorB * up) + factorC);
			
			return expToUp;
		} else {
			return 0;
		}
	}
	
	public final double getBonusEffectiveness() {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		final int maxEffectivenessLevel = mainConfig.getFishingMaxBonusEffectivenessLevel();
		final double maxEffectiveness = mainConfig.getFishingMaxBonusEffectiveness();
		final double progress = (double) (Math.min(getLevel(), maxEffectivenessLevel)) / maxEffectivenessLevel;
		final double result = progress * maxEffectiveness;
		
		return result;
	}
	
	public final double getBonusEndurance() {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		final int maxEnduranceLevel = mainConfig.getFishingMaxBonusEnduranceLevel();
		final double maxEndurance = mainConfig.getFishingMaxBonusEndurance();
		final double progress = (double) (Math.min(getLevel(), maxEnduranceLevel)) / maxEnduranceLevel;
		final double result = progress * maxEndurance;
		
		return result;
	}
	
	public final double getBonusSpeed() {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		final int maxSpeedLevel = mainConfig.getFishingMaxBonusSpeedLevel();
		final double maxSpeed = mainConfig.getFishingMaxBonusSpeed();
		final double progress = (double) (Math.min(getLevel(), maxSpeedLevel)) / maxSpeedLevel;
		final double result = progress * maxSpeed;
		
		return result;
	}
	
	public final List<BaitProperties> getUnlockedBait() {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final GameManager gameManager = plugin.getGameManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		final Player player = PlayerUtil.getOnlinePlayer(getPlayerId());
		final List<BaitProperties> unlockedBait = new ArrayList<BaitProperties>();
		
		if (player != null) {
			for (BaitProperties baitProperties : baitManager.getAllBaitProperties()) {
				final ItemRequirement requirement = baitProperties.getRequirement();
				
				if (requirement.isAllowed(player)) {
					unlockedBait.add(baitProperties);
				}
			}
		}
		
		return unlockedBait;
	}
	
	public final List<FishProperties> getUnlockedFish() {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final GameManager gameManager = plugin.getGameManager();
		final FishManager fishManager = gameManager.getFishManager();
		final Player player = PlayerUtil.getOnlinePlayer(getPlayerId());
		final List<FishProperties> unlockedFish = new ArrayList<FishProperties>();
		
		if (player != null) {
			for (FishProperties fishProperties : fishManager.getAllFishProperties()) {
				final ItemRequirement requirement = fishProperties.getRequirement();
				
				if (requirement.isAllowed(player)) {
					unlockedFish.add(fishProperties);
				}
			}
		}
		
		return unlockedFish;
	}
	
	public final void save() {
		final PlayerFishingMemory playerFishingMemory = PlayerFishingMemory.getInstance();
		
		playerFishingMemory.save(this);
	}
	
	public final String serialize() {
		final FileConfiguration config = new YamlConfiguration();
		final UUID playerId = getPlayerId();
		
		config.set("playerId", playerId.toString());
		config.set("level", level);
		config.set("exp", exp);
		
		return config.saveToString();
	}
	
	public static final PlayerFishing deserialize(String serialize) throws InvalidConfigurationException {
		if (serialize != null) {
			final FileConfiguration config = new YamlConfiguration();
			
			config.loadFromString(serialize);
			
			UUID playerId = null;
			int level = 1;
			float exp = 0;
			
			for (String key : config.getKeys(false)) {
				if (key.equalsIgnoreCase("playerId")) {
					playerId = UUID.fromString(config.getString(key));
				} else if (key.equalsIgnoreCase("level")) {
					level = config.getInt(key);
				} else if (key.equalsIgnoreCase("exp")) {
					exp = (float) config.getDouble(key);
				}
			}
			
			if (playerId != null) {
				final PlayerFishing playerHerbalism = new PlayerFishing(playerId, level, exp); 
				
				return playerHerbalism;
			}
		} 
			
		return null;
	}
	
	public static final PlayerFishing deserializeSilent(String serialize) {
		try {
			return deserialize(serialize);
		} catch (InvalidConfigurationException e) {
			return null;
		}
	}
}
