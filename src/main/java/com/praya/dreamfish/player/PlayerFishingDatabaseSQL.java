package com.praya.dreamfish.player;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.praya.agarthalib.database.Database;
import com.praya.agarthalib.database.DatabaseClient;
import com.praya.agarthalib.database.DatabaseError.DatabaseErrorType;
import com.praya.agarthalib.database.DatabaseMySQL;
import com.praya.agarthalib.database.DatabaseSQL;
import com.praya.agarthalib.database.DatabaseSQLColumn;
import com.praya.agarthalib.database.DatabaseSQLTable;
import com.praya.agarthalib.database.DatabaseSQLite;
import com.praya.agarthalib.database.DatabaseType;
import com.praya.agarthalib.utility.FileUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;

public final class PlayerFishingDatabaseSQL extends PlayerFishingDatabase {

	private static final String PATH_FILE = "Database/player.db";
	private static final String TABLE = "Player_Fishing";
	private static final String KEY_ID = "Id";
	private static final String KEY_DATA = "Data";
	
	private final DatabaseSQL database;
	
	protected PlayerFishingDatabaseSQL(DreamFish plugin) {
		super(plugin);
		
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final DatabaseSQLColumn columnId = new DatabaseSQLColumn(KEY_ID, String.class);
		final DatabaseSQLColumn columnData = new DatabaseSQLColumn(KEY_DATA, String.class, 65535);
		final DatabaseType type = mainConfig.getDatabaseServerType();
		final List<DatabaseSQLColumn> listColumn = new ArrayList<DatabaseSQLColumn>();
		
		listColumn.add(columnId);
		listColumn.add(columnData);
		
		final DatabaseSQLTable dbTable = new DatabaseSQLTable(TABLE, KEY_ID, listColumn);
		
		if (type.equals(DatabaseType.SQLite)) {
			final File file = FileUtil.getFile(plugin, PATH_FILE);
			
			if (!file.exists()) {
				FileUtil.createFileSilent(file);
			}
			
			this.database = new DatabaseSQLite(plugin, dbTable, file);
		} else if (type.equals(DatabaseType.MySQL)) {
			final String clientHost = mainConfig.getDatabaseClientHost();
			final int clientPort = mainConfig.getDatabaseClientPort();
			final String clientDatabase = mainConfig.getDatabaseClientDatabase();
			final String clientUsername = mainConfig.getDatabaseClientUsername();
			final String clientPassword = mainConfig.getDatabaseClientPassword();
			final DatabaseClient databaseClient = new DatabaseClient(clientHost, clientPort, clientDatabase, clientUsername, clientPassword);
			
			this.database = new DatabaseMySQL(plugin, dbTable, databaseClient);
		} else {
			this.database = null;
		}
		
		if (this.database != null) {
			this.database.initialize();
		}
	}
	
	@Override
	protected final Database getDatabase() {
		return this.database;
	}
	
	@Override
	protected final List<String> getAllId() throws SQLException {
		final List<String> allId = new ArrayList<String>();
		
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
    	try {
    		final Connection connection = database.getConnection();
    		final String statement = "SELECT * FROM " + TABLE + ";";
        	
        	preparedStatement = connection.prepareStatement(statement);
        	resultSet = preparedStatement.executeQuery();
        	
        	while (resultSet.next()) {
        		final String id = resultSet.getString(KEY_ID); 
        		
        		allId.add(id);
        	}
        } catch (SQLException exception) {
        	final DatabaseErrorType databaseErrorType = DatabaseErrorType.STATEMENT_FAILED_EXECUTE;
        	
        	database.throwError(exception, databaseErrorType);
        } finally {
            database.close(preparedStatement, resultSet);
        }
		
		return allId;
	}
	
	@Override
	protected final List<String> getAllData() throws SQLException {
		final List<String> allData = new ArrayList<String>();
		
		Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
    	try {
    		final String statement = "SELECT * FROM " + TABLE + ";";
    		
        	connection = database.getConnection();
        	preparedStatement = connection.prepareStatement(statement);
        	resultSet = preparedStatement.executeQuery();
        	
        	while (resultSet.next()) {
        		final String data = resultSet.getString(KEY_DATA); 
        		
        		allData.add(data);
        	}
        } catch (SQLException exception) {
        	final DatabaseErrorType databaseErrorType = DatabaseErrorType.STATEMENT_FAILED_EXECUTE;
        	
        	database.throwError(exception, databaseErrorType);
        } finally {
            database.close(preparedStatement, resultSet);
        }
		
		return allData;
	}
	
	@Override
	protected final String getData(String id) throws SQLException {
        if (id != null) {
        	
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            
	    	try {
	    		final Connection connection = database.getConnection();
	    		final String statement = "SELECT * FROM " + TABLE + " WHERE " + KEY_ID + " = '" + id + "';";
	    		
	        	preparedStatement = connection.prepareStatement(statement);
	        	resultSet = preparedStatement.executeQuery();
	        	
	        	if (resultSet.next()) {
	        		final String data = resultSet.getString(KEY_DATA); 
	        		
	        		return data;
	        	}
	        } catch (SQLException exception) {
	        	final DatabaseErrorType databaseErrorType = DatabaseErrorType.STATEMENT_FAILED_EXECUTE;
	        	
	        	database.throwError(exception, databaseErrorType);
	        } finally {
	            database.close(preparedStatement, resultSet);
	        }
        }
        
        return null;
	}
	
	@Override
	protected final boolean isExists(String id) throws SQLException {
		 return getData(id) != null;
	}
	
	@Override
	protected final PlayerFishing getPlayerFishing(String id) throws SQLException {
		if (id != null) {
			final String data = getData(id);
			final PlayerFishing playerFishing = PlayerFishing.deserializeSilent(data);
			
			return playerFishing;
		} else {
			return null;
		}
	}
	
	@Override
	protected final boolean saveData(PlayerFishing playerFishing) {
		if (playerFishing != null) {
			
			PreparedStatement preparedStatement = null;
			
			try {
				final Connection connection = database.getConnection();
				final String id = playerFishing.getPlayerId().toString();
				final String data = playerFishing.serialize();
				
				preparedStatement = connection.prepareStatement("REPLACE INTO " + TABLE + " (id,data) VALUES(?,?)");
				
				preparedStatement.setString(1, id);
				preparedStatement.setString(2, data);
				preparedStatement.executeUpdate();
	            return true;
			} catch (SQLException exception) {
				final DatabaseErrorType databaseErrorType = DatabaseErrorType.STATEMENT_FAILED_EXECUTE;
	        	
	        	database.throwError(exception, databaseErrorType);
	        } finally {
	            database.close(preparedStatement);
	        }
		}
		
		return false;
	}
	
	@Override
	protected final boolean deleteData(String id) {
		if (id != null) {
			
			PreparedStatement preparedStatement = null;
			
			try {
				final Connection connection = database.getConnection();
				
				preparedStatement = connection.prepareStatement("DELETE FROM " + TABLE + " WHERE " + KEY_ID + " = '" + id + "';");
				
				preparedStatement.executeUpdate();
	            return true;
			} catch (SQLException exception) {
				final DatabaseErrorType databaseErrorType = DatabaseErrorType.STATEMENT_FAILED_EXECUTE;
	        	
	        	database.throwError(exception, databaseErrorType);
	        } finally {
	            database.close(preparedStatement);
	        }
        }
		
		return false;
	}
}
