package com.praya.dreamfish.player;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.fish.FishProperties;

public class PlayerFishingModeProperties {

	private double fishPower;
	private double tension;
	
	public PlayerFishingModeProperties(FishProperties fishProperties) {
		final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final double fishOffsetPower = mainConfig.getFishOffsetPower();
		
		this.fishPower = fishProperties.getPower() + ((Math.random()-0.5) * fishOffsetPower);
		this.tension = 0;
	}
	
	public PlayerFishingModeProperties(double fishPower, double tension) {
		this.fishPower = fishPower;
		this.tension = tension;
	}
	
	public final double getFishPower() {
		return this.fishPower;
	}
	
	public final double getTension() {
		return this.tension;
	}
	
	public final void setFishPower(double fishPower) {
		this.fishPower = MathUtil.limitDouble(fishPower, 0, fishPower);
	}
	
	public final void addTension(double additional) {
		if (getFishPower() > 0) {
			
			double tension = getTension() + additional;
			
			tension = MathUtil.limitDouble(tension, 0, tension);
			
			setTension(tension);
		} else {
			setTension(0);
		}
	}
	
	public final void setTension(double tension) {
		this.tension = MathUtil.limitDouble(tension, 0, tension);
	}
	
	public final void addFishPower(double additional) {
		if (getFishPower() > 0) {
			
			double fishPower = getFishPower() + additional;
			
			fishPower = MathUtil.limitDouble(fishPower, 0D, fishPower);
			
			setFishPower(fishPower);
		} else {
			setFishPower(0);
		}
	}
}
