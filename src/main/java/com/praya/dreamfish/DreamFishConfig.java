package com.praya.dreamfish;

import java.io.File;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import com.praya.dreamfish.handler.HandlerConfig;

import core.praya.agarthalib.builder.message.BossBar.Color;
import core.praya.agarthalib.builder.message.BossBar.Style;

import com.praya.agarthalib.database.DatabaseType;
import com.praya.agarthalib.utility.EntityUtil;
import com.praya.agarthalib.utility.FileUtil;

public final class DreamFishConfig extends HandlerConfig {
	
	private static final String PATH_FILE = "Configuration/config.yml";
	
	public static final FileConfiguration config = new YamlConfiguration();
	
	protected DreamFishConfig(DreamFish plugin) {
		super(plugin);
		
		moveOldFile();
		setup();
	}
	
	public final String getGeneralVersion() {
		return config.getString("Configuration.General.Version");
	}
	
	public final String getGeneralLocale() {
		return config.getString("Configuration.General.Locale");
	}
	
	public final boolean isMetricsMessage() {
		return config.getBoolean("Configuration.Metrics.Message");
	}
	
	public final boolean isHookMessage() {
		return config.getBoolean("Configuration.Hook.Message");
	}
	
	public final String getUtilityTooltip() {
		return config.getString("Configuration.Utility.Tooltip");
	}
	
	public final String getUtilityCurrency() {
		return config.getString("Configuration.Utility.Currency");
	}
	
	public final int getBarLength() {
		return config.getInt("Configuration.Bar.Length");
	}
	
	public final String getBarSymbol() {
		return config.getString("Configuration.Bar.Symbol");
	}
	
	public final String getBarFill() {
		return config.getString("Configuration.Bar.Fill");
	}
	
	public final String getBarEmpty() {
		return config.getString("Configuration.Bar.Empty");
	}
	
	public final double getEffectRange() {
		return config.getDouble("Configuration.Effect.Range");
	}
	
	public final int getListContent() {
		return config.getInt("Configuration.List.Content");
	}
	
	public final int getPriorityActionbar() {
		return config.getInt("Configuration.Priority.Actionbar");
	}
	
	public final int getPriorityBossBar() {
		return config.getInt("Configuration.Priority.BossBar");
	}
	
	public final DatabaseType getDatabaseServerType() {
		return (DatabaseType) config.get("Configuration.Database.Server_Type");
	}
	
	public final String getDatabaseServerIdentifier() {
		return config.getString("Configuration.Database.Server_Identifier");
	}
	
	public final int getDatabasePeriodSave() {
		return config.getInt("Configuration.Database.Periode_Save");
	}
	
	public final String getDatabaseClientHost() {
		return config.getString("Configuration.Database.Client_Host");
	}
	
	public final int getDatabaseClientPort() {
		return config.getInt("Configuration.Database.Client_Port");
	}
	
	public final String getDatabaseClientDatabase() {
		return config.getString("Configuration.Database.Client_Database");
	}
	
	public final String getDatabaseClientUsername() {
		return config.getString("Configuration.Database.Client_Username");
	}
	
	public final String getDatabaseClientPassword() {
		return config.getString("Configuration.Database.Client_Password");
	}
	
	public final boolean isSupportEnableMcMMO() {
		return config.getBoolean("Configuration.Support.Enable_mcMMO");
	}
	
	public final boolean isSupportEnableJobsReborn() {
		return config.getBoolean("Configuration.Support.Enable_JobsReborn");
	}
	
	public final boolean isSupportEnableSkillAPI() {
		return config.getBoolean("Configuration.Support.Enable_SkillAPI");
	}
	
	public final double getSupportExpMultiplierMcMMO() {
		return config.getDouble("Configuration.Support.Exp_Multiplier_McMMO");
	}
	
	public final double getSupportExpMultiplierJobsReborn() {
		return config.getDouble("Configuration.Support.Exp_Multiplier_JobsReborn");
	}
	
	public final double getSupportExpMultiplierSkillAPI() {
		return config.getDouble("Configuration.Support.Exp_Multiplier_SkillAPI");
	}
	
	public final double getSupportExpMultiplierVanilla() {
		return config.getDouble("Configuration.Support.Exp_Multiplier_Vanilla");
	}
	
	public final boolean isMenuEnablePageBuyBait() {
		return config.getBoolean("Configuration.Menu.Enable_Page_Buy_Bait");
	}
	
	public final boolean isMenuEnablePageSellBait() {
		return config.getBoolean("Configuration.Menu.Enable_Page_Sell_Bait");
	}
	
	public final boolean isMenuEnablePageSellFish() {
		return config.getBoolean("Configuration.Menu.Enable_Page_Sell_Fish");
	}
	
	public final boolean isMenuEnableActionHome() {
		return config.getBoolean("Configuration.Menu.Enable_Action_Home");
	}
	
	public final boolean isMenuEnableActionBack() {
		return config.getBoolean("Configuration.Menu.Enable_Action_Back");
	}
	
	public final String getMenuPermissionHome() {
		return config.getString("Configuration.Menu.Permission_Home");
	}
	
	public final String getMenuPermissionBuyBait() {
		return config.getString("Configuration.Menu.Permission_Buy_Bait");
	}
	
	public final String getMenuPermissionSellBait() {
		return config.getString("Configuration.Menu.Permission_Sell_Bait");
	}
	
	public final String getMenuPermissionSellFish() {
		return config.getString("Configuration.Menu.Permission_Sell_Fish");
	}
	
	public final String getMenuPermissionGuide() {
		return config.getString("Configuration.Menu.Permission_Guide");
	}
	
	public final double getRodDefaultPullPower() {
		return config.getDouble("Configuration.Rod.Default_Pull_Power");
	}
	
	public final double getRodDefaultPullTension() {
		return config.getDouble("Configuration.Rod.Default_Pull_Tension");
	}
	
	public final double getRodDefaultPullSpeed() {
		return config.getDouble("Configuration.Rod.Default_Pull_Speed");
	}
	
	public final double getRodDefaultPullMaxSpeed() {
		return config.getDouble("Configuration.Rod.Default_Pull_Max_Speed");
	}
	
	public final double getRodDefaultLuresMaxTension() {
		return config.getDouble("Configuration.Rod.Default_Lures_Max_Tension");
	}
	
	public final boolean isBaitEnableAutoHook() {
		return config.getBoolean("Configuration.Bait.Enable_Auto_Hook");
	}
	
	public final boolean isBaitEnableCheckOnCatch() {
		return config.getBoolean("Configuration.Bait.Enable_Check_On_Catch");
	}
	
	public final boolean isFishEnableName() {
		return config.getBoolean("Configuration.Fish.Enable_Name");
	}
	
	public final boolean isFishEnableAggresive() {
		return config.getBoolean("Configuration.Fish.Enable_Aggresive");
	}
	
	public final boolean isFishEnableClever() {
		return config.getBoolean("Configuration.Fish.Enable_Clever");
	}
	
	public final double getFishOffsetPower() {
		return config.getDouble("Configuration.Fish.Offset_Power");
	}
	
	public final EntityType getFishDefaultType() {
		return (EntityType) config.get("Configuration.Fish.Default_Type");
	}
	
	public final double getFishDefaultResistance() {
		return config.getDouble("Configuration.Fish.Default_Resistance");
	}
	
	public final double getFishDefaultPower() {
		return config.getDouble("Configuration.Fish.Default_Power");
	}
	
	public final double getFishDefaultChanceTurnAround() {
		return config.getDouble("Configuration.Fish.Default_Chance_Turn_Around");
	}
	
	public final double getFishDefaultMaxPower() {
		return config.getDouble("Configuration.Fish.Default_Max_Power");
	}
	
	public final double getFishDefaultMaxSpeed() {
		return config.getDouble("Configuration.Fish.Default_Max_Speed");
	}
	
	public final double getFishDefaultMaxDive() {
		return config.getDouble("Configuration.Fish.Default_Max_Dive");
	}
	
	public final double getFishDefaultExp() {
		return config.getDouble("Configuration.Fish.Default_Exp");
	}
	
	public final String getFishFormatName() {
		return config.getString("Configuration.Fish.Format_Name");
	}
	
	public final boolean isTreasureEnableReplaceFish() {
		return config.getBoolean("Configuration.Treasure.Enable_Replace_Fish");
	}

	public final boolean isTreasureEnablePluginTreasure() {
		return config.getBoolean("Configuration.Treasure.Enable_Plugin_Treasure");
	}
	
	public final boolean isFishingEnableRealistic() {
		return config.getBoolean("Configuration.Fishing.Enable_Realistic");
	}
	
	public final boolean isFishingEnableAutoCatch() {
		return config.getBoolean("Configuration.Fishing.Enable_Auto_Catch");
	}
	
	public final int getFishingMaxLevel() {
		return config.getInt("Configuration.Fishing.Max_Level");
	}
	
	public final int getFishingMaxBonusEffectivenessLevel() {
		return config.getInt("Configuration.Fishing.Max_Bonus_Effectiveness_Level");
	}
	
	public final int getFishingMaxBonusEnduranceLevel() {
		return config.getInt("Configuration.Fishing.Max_Bonus_Endurance_Level");
	}
	
	public final int getFishingMaxBonusSpeedLevel() {
		return config.getInt("Configuration.Fishing.Max_Bonus_Speed_Level");
	}
	
	public final double getFishingMaxBonusEffectiveness() {
		return config.getDouble("Configuration.Fishing.Max_Bonus_Effectiveness");
	}
	
	public final double getFishingMaxBonusEndurance() {
		return config.getDouble("Configuration.Fishing.Max_Bonus_Endurance");
	}
	
	public final double getFishingMaxBonusSpeed() {
		return config.getDouble("Configuration.Fishing.Max_Bonus_Speed");
	}
	
	public final double getFishingFormulaExpA() {
		return config.getDouble("Configuration.Fishing.Formula_Exp_A");
	}
	
	public final double getFishingFormulaExpB() {
		return config.getDouble("Configuration.Fishing.Formula_Exp_B");
	}
	
	public final double getFishingFormulaExpC() {
		return config.getDouble("Configuration.Fishing.Formula_Exp_C");
	}
	
	public final int getFishingInformationType() {
		return config.getInt("Configuration.Fishing.Information_Type");
	}
	
	public final String getFishingFormatActionbarFight() {
		return config.getString("Configuration.Fishing.Format_Actionbar_Fight");
	}
	
	public final String getFishingFormatActionbarPull() {
		return config.getString("Configuration.Fishing.Format_Actionbar_Pull");
	}
	
	public final String getFishingFormatBossBarFight() {
		return config.getString("Configuration.Fishing.Format_BossBar_Fight");
	}
	
	public final String getFishingFormatBossBarPull() {
		return config.getString("Configuration.Fishing.Format_BossBar_Pull");
	}

	public final Color getBossBarColor() {
		return (Color) config.get("Configuration.BossBar.Color");
	}
	
	public final Style getBossBarStyle() {
		return (Style) config.get("Configuration.BossBar.Style");
	}
	
	public final void setup() {
		final File file = FileUtil.getFile(plugin, PATH_FILE);
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration configurationResource = FileUtil.getFileConfigurationResource(plugin, PATH_FILE);
		final FileConfiguration configurationFile = FileUtil.getFileConfiguration(file);
		
		loadConfig(config, configurationResource);
		loadConfig(config, configurationFile);
		loadOldConfig(config, configurationFile);
	}
	
	private final void loadConfig(FileConfiguration config, FileConfiguration source) {
		for (String key : source.getKeys(false)) {
			if (key.equalsIgnoreCase("Configuration") || key.equalsIgnoreCase("Config")) {
				final ConfigurationSection dataSection = source.getConfigurationSection(key);
				
				for (String data : dataSection.getKeys(false)) {
					if (data.equalsIgnoreCase("General")) {
						final ConfigurationSection generalDataSection = dataSection.getConfigurationSection(data);
						
						for (String generalData : generalDataSection.getKeys(false)) {
							if (generalData.equalsIgnoreCase("Version")) {
								final String path = "Configuration.General.Version";
								final String generalVersion = generalDataSection.getString(generalData);
								
								config.set(path, generalVersion);
							} else if (generalData.equalsIgnoreCase("Locale")) {
								final String path = "Configuration.General.Locale";
								final String generalLocale = generalDataSection.getString(generalData);
								
								config.set(path, generalLocale);
							}
						}
					} else if (data.equalsIgnoreCase("Metrics")) {
						final ConfigurationSection metricsDataSection = dataSection.getConfigurationSection(data);
						
						for (String metricsData : metricsDataSection.getKeys(false)) {
							if (metricsData.equalsIgnoreCase("Message")) {
								final String path = "Configuration.Metrics.Message";
								final boolean metricsMessage = metricsDataSection.getBoolean(metricsData);
								
								config.set(path, metricsMessage);
							}
						}
					} else if (data.equalsIgnoreCase("Hook")) {
						final ConfigurationSection hookDataSection = dataSection.getConfigurationSection(data);
						
						for (String hookData : hookDataSection.getKeys(false)) {
							if (hookData.equalsIgnoreCase("Message")) {
								final String path = "Configuration.Hook.Message";
								final boolean hookMessage = hookDataSection.getBoolean(hookData);
								
								config.set(path, hookMessage);
							}
						}
					} else if (data.equalsIgnoreCase("Utility")) {
						final ConfigurationSection utilityDataSection = dataSection.getConfigurationSection(data);
						
						for (String utilityData : utilityDataSection.getKeys(false)) {
							if (utilityData.equalsIgnoreCase("Tooltip")) {
								final String path = "Configuration.Utility.Tooltip";
								final String utilityTooltip = utilityDataSection.getString(utilityData);
								
								config.set(path, utilityTooltip);
							} else if (utilityData.equalsIgnoreCase("Currency")) {
								final String path = "Configuration.Utility.Currency";
								final String utilityCurrency = utilityDataSection.getString(utilityData);
								
								config.set(path, utilityCurrency);
							}
						}
					} else if (data.equalsIgnoreCase("Bar")) {
						final ConfigurationSection barDataSection = dataSection.getConfigurationSection(data);
						
						for (String barData : barDataSection.getKeys(false)) {
							if (barData.equalsIgnoreCase("Length")) {
								final String path = "Configuration.Bar.Length";
								final int barLength = barDataSection.getInt(barData);
								
								config.set(path, barLength);
							} else if (barData.equalsIgnoreCase("Symbol")) {
								final String path = "Configuration.Bar.Symbol";
								final String barSymbol = barDataSection.getString(barData);
								
								config.set(path, barSymbol);
							} else if (barData.equalsIgnoreCase("Fill")) {
								final String path = "Configuration.Bar.Fill";
								final String barFill = barDataSection.getString(barData);
								
								config.set(path, barFill);
							} else if (barData.equalsIgnoreCase("Empty")) {
								final String path = "Configuration.Bar.Empty";
								final String barEmpty = barDataSection.getString(barData);
								
								config.set(path, barEmpty);
							}
						} 
					} else if (data.equalsIgnoreCase("Effect")) {
						final ConfigurationSection effectDataSection = dataSection.getConfigurationSection(data);
						
						for (String effectData : effectDataSection.getKeys(false)) {
							if (effectData.equalsIgnoreCase("Range")) {
								final String path = "Configuration.Effect.Range";
								final double effectRange = effectDataSection.getDouble(effectData);
								
								config.set(path, effectRange);
							}
						}
					} else if (data.equalsIgnoreCase("List")) {
						final ConfigurationSection listDataSection = dataSection.getConfigurationSection(data);
						
						for (String listData : listDataSection.getKeys(false)) {
							if (listData.equalsIgnoreCase("Content")) {
								final String path = "Configuration.List.Content";
								final int listContent = listDataSection.getInt(listData);
								
								config.set(path, listContent);
							}
						}
					} else if (data.equalsIgnoreCase("Priority")) {
						final ConfigurationSection priorityDataSection = dataSection.getConfigurationSection(data);
						
						for (String priorityData : priorityDataSection.getKeys(false)) {
							if (priorityData.equalsIgnoreCase("Actionbar")) {
								final String path = "Configuration.Priority.Actionbar";
								final int priorityActionbar = priorityDataSection.getInt(priorityData);
								
								config.set(path, priorityActionbar);
							} else if (priorityData.equalsIgnoreCase("BossBar")) {
								final String path = "Configuration.Priority.BossBar";
								final int priorityBossBar = priorityDataSection.getInt(priorityData);
								
								config.set(path, priorityBossBar);
							}
						}
					} else if (data.equalsIgnoreCase("Database")) {
						final ConfigurationSection databaseDataSection = dataSection.getConfigurationSection(data);
						
						for (String databaseData : databaseDataSection.getKeys(false)) {
							if (databaseData.equalsIgnoreCase("Server_Type")) {
								final String path = "Configuration.Database.Server_Type";
								final String databaseServerTypeText = databaseDataSection.getString(databaseData);
								final DatabaseType databaseServerType = DatabaseType.getDatabaseType(databaseServerTypeText);
								
								config.set(path, databaseServerType);
							} else if (databaseData.equalsIgnoreCase("Server_Identifier")) {
								final String path = "Configuration.Database.Server_Identifier";
								final String databaseServerIdentifier = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseServerIdentifier);
							} else if (databaseData.equalsIgnoreCase("Periode_Save")) {
								final String path = "Configuration.Database.Periode_Save";
								final int databasePeriodSave = databaseDataSection.getInt(databaseData);
								
								config.set(path, databasePeriodSave);
							} else if (databaseData.equalsIgnoreCase("Client_Host")) {
								final String path = "Configuration.Database.Client_Host";
								final String databaseClientHost = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseClientHost);
							} else if (databaseData.equalsIgnoreCase("Client_Port")) {
								final String path = "Configuration.Database.Client_Port";
								final int databaseClientPort = databaseDataSection.getInt(databaseData);
								
								config.set(path, databaseClientPort);
							} else if (databaseData.equalsIgnoreCase("Client_Database")) {
								final String path = "Configuration.Database.Client_Database";
								final String databaseClientDatabase = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseClientDatabase);
							} else if (databaseData.equalsIgnoreCase("Client_Username")) {
								final String path = "Configuration.Database.Client_Username";
								final String databaseClientUsername = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseClientUsername);
							} else if (databaseData.equalsIgnoreCase("Client_Password")) {
								final String path = "Configuration.Database.Client_Password";
								final String databaseClientPassword = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseClientPassword);
							}
						}
					} else if (data.equalsIgnoreCase("Menu")) {
						final ConfigurationSection menuDataSection = dataSection.getConfigurationSection(data);
						
						for (String menuData : menuDataSection.getKeys(false)) {
							if (menuData.equalsIgnoreCase("Enable_Page_Buy_Bait") || menuData.equalsIgnoreCase("Enable_Buy_Bait")) {
								final String path = "Configuration.Menu.Enable_Page_Buy_Bait";
								final boolean menuEnablePageBuyBait = menuDataSection.getBoolean(menuData);
								
								config.set(path, menuEnablePageBuyBait);
							} else if (menuData.equalsIgnoreCase("Enable_Page_Sell_Bait") || menuData.equalsIgnoreCase("Enable_Sell_Bait")) {
								final String path = "Configuration.Menu.Enable_Page_Sell_Bait";
								final boolean menuEnablePageSellBait = menuDataSection.getBoolean(menuData);
								
								config.set(path, menuEnablePageSellBait);
							} else if (menuData.equalsIgnoreCase("Enable_Page_Sell_Fish") || menuData.equalsIgnoreCase("Enable_Sell_Fish")) {
								final String path = "Configuration.Menu.Enable_Page_Sell_Fish";
								final boolean menuEnablePageSellFish = menuDataSection.getBoolean(menuData);
								
								config.set(path, menuEnablePageSellFish);
							} else if (menuData.equalsIgnoreCase("Enable_Action_Home")) {
								final String path = "Configuration.Menu.Enable_Action_Home";
								final boolean menuEnableActionHome = menuDataSection.getBoolean(menuData);
								
								config.set(path, menuEnableActionHome);
							} else if (menuData.equalsIgnoreCase("Enable_Action_Back")) {
								final String path = "Configuration.Menu.Enable_Action_Back";
								final boolean menuEnableActionBack = menuDataSection.getBoolean(menuData);
								
								config.set(path, menuEnableActionBack);
							} else if (menuData.equalsIgnoreCase("Permission_Home")) {
								final String path = "Configuration.Menu.Permission_Home";
								final String menuHomePermission = menuDataSection.getString(menuData);
								
								config.set(path, menuHomePermission);
							} else if (menuData.equalsIgnoreCase("Permission_Buy_Bait")) {
								final String path = "Configuration.Menu.Permission_Buy_Bait";
								final String menuPermissionBuyBait = menuDataSection.getString(menuData);
								
								config.set(path, menuPermissionBuyBait);
							} else if (menuData.equalsIgnoreCase("Permission_Sell_Bait")) {
								final String path = "Configuration.Menu.Permission_Sell_Bait";
								final String menuPermissionSellBait = menuDataSection.getString(menuData);
								
								config.set(path, menuPermissionSellBait);
							} else if (menuData.equalsIgnoreCase("Permission_Sell_Fish")) {
								final String path = "Configuration.Menu.Permission_Sell_Fish";
								final String menuPermissionSellFish = menuDataSection.getString(menuData);
								
								config.set(path, menuPermissionSellFish);
							} else if (menuData.equalsIgnoreCase("Permission_Guide")) {
								final String path = "Configuration.Menu.Permission_Guide";
								final String menuPermissionGuide = menuDataSection.getString(menuData);
								
								config.set(path, menuPermissionGuide);
							}
						}
					} else if (data.equalsIgnoreCase("Support")) {
						final ConfigurationSection supportDataSection = dataSection.getConfigurationSection(data);
						
						for (String supportData : supportDataSection.getKeys(false)) {
							if (supportData.equalsIgnoreCase("Enable_mcMMO")) {
								final String path = "Configuration.Support.Enable_mcMMO";
								final boolean supportEnableMcMMO = supportDataSection.getBoolean(supportData);
								
								config.set(path, supportEnableMcMMO);
							} else if (supportData.equalsIgnoreCase("Enable_JobsReborn")) {
								final String path = "Configuration.Support.Enable_JobsReborn";
								final boolean supportEnableJobsReborn = supportDataSection.getBoolean(supportData);
								
								config.set(path, supportEnableJobsReborn);
							} else if (supportData.equalsIgnoreCase("Enable_SkillAPI")) {
								final String path = "Configuration.Support.Enable_SkillAPI";
								final boolean supportEnableSkillAPI = supportDataSection.getBoolean(supportData);
								
								config.set(path, supportEnableSkillAPI);
							} else if (supportData.equalsIgnoreCase("Exp_Multiplier_McMMO")) {
								final String path = "Configuration.Support.Exp_Multiplier_McMMO";
								final double supportExpMultiplierMcMMO = supportDataSection.getDouble(supportData);
								
								config.set(path, supportExpMultiplierMcMMO);
							} else if (supportData.equalsIgnoreCase("Exp_Multiplier_JobsReborn")) {
								final String path = "Configuration.Support.Exp_Multiplier_JobsReborn";
								final double supportExpMultiplierJobsReborn = supportDataSection.getDouble(supportData);
								
								config.set(path, supportExpMultiplierJobsReborn);
							} else if (supportData.equalsIgnoreCase("Exp_Multiplier_SkillAPI")) {
								final String path = "Configuration.Support.Exp_Multiplier_SkillAPI";
								final double supportExpMultiplierSkillAPI = supportDataSection.getDouble(supportData);
								
								config.set(path, supportExpMultiplierSkillAPI);
							} else if (supportData.equalsIgnoreCase("Exp_Multiplier_Vanilla")) {
								final String path = "Configuration.Support.Exp_Multiplier_Vanilla";
								final double supportExpMultiplierVanilla = supportDataSection.getDouble(supportData);
								
								config.set(path, supportExpMultiplierVanilla);
							}
						}
					} else if (data.equalsIgnoreCase("Rod")) {
						final ConfigurationSection rodDataSection = dataSection.getConfigurationSection(data);
						
						for (String rodData : rodDataSection.getKeys(false)) {
							if (rodData.equalsIgnoreCase("Default_Pull_Power")) {
								final String path = "Configuration.Rod.Default_Pull_Power";
								final double rodDefaultPullPower = rodDataSection.getDouble(rodData);
								
								config.set(path, rodDefaultPullPower);
							} else if (rodData.equalsIgnoreCase("Default_Pull_Tension")) {
								final String path = "Configuration.Rod.Default_Pull_Tension";
								final double rodDefaultPullTension = rodDataSection.getDouble(rodData);
								
								config.set(path, rodDefaultPullTension);
							} else if (rodData.equalsIgnoreCase("Default_Pull_Speed")) {
								final String path = "Configuration.Rod.Default_Pull_Speed";
								final double rodDefaultPullSpeed = rodDataSection.getDouble(rodData);
								
								config.set(path, rodDefaultPullSpeed);
							} else if (rodData.equalsIgnoreCase("Default_Pull_Max_Speed")) {
								final String path = "Configuration.Rod.Default_Pull_Max_Speed";
								final double rodDefaultPullMaxSpeed = rodDataSection.getDouble(rodData);
								
								config.set(path, rodDefaultPullMaxSpeed);
							} else if (rodData.equalsIgnoreCase("Default_Lures_Max_Tension")) {
								final String path = "Configuration.Rod.Default_Lures_Max_Tension";
								final double rodDefaultLuresMaxTension = rodDataSection.getDouble(rodData);
								
								config.set(path, rodDefaultLuresMaxTension);
							}
						} 
					} else if (data.equalsIgnoreCase("Bait")) {
						final ConfigurationSection baitDataSection = dataSection.getConfigurationSection(data);
						
						for (String baitData : baitDataSection.getKeys(false)) {
							if (baitData.equalsIgnoreCase("Enable_Auto_Hook")) {
								final String path = "Configuration.Bait.Enable_Auto_Hook";
								final boolean baitEnableAutoHook = baitDataSection.getBoolean(baitData);
								
								config.set(path, baitEnableAutoHook);
							} else if (baitData.equalsIgnoreCase("Enable_Check_On_Catch")) {
								final String path = "Configuration.Bait.Enable_Check_On_Catch";
								final boolean baitEnableCheckOnCatch = baitDataSection.getBoolean(baitData);
								
								config.set(path, baitEnableCheckOnCatch);
							}
						}
					} else if (data.equalsIgnoreCase("Fish")) {
						final ConfigurationSection fishDataSection = dataSection.getConfigurationSection(data);
						
						for (String fishData : fishDataSection.getKeys(false)) {
							if (fishData.equalsIgnoreCase("Enable_Name")) {
								final String path = "Configuration.Fish.Enable_Name";
								final boolean fishEnableName = fishDataSection.getBoolean(fishData);
								
								config.set(path, fishEnableName);
							} else if (fishData.equalsIgnoreCase("Enable_Aggresive")) {
								final String path = "Configuration.Fish.Enable_Aggresive";
								final boolean fishEnableAggresive = fishDataSection.getBoolean(fishData);
								
								config.set(path, fishEnableAggresive);
							} else if (fishData.equalsIgnoreCase("Enable_Clever")) {
								final String path = "Configuration.Fish.Enable_Clever";
								final boolean fishEnableClever = fishDataSection.getBoolean(fishData);
								
								config.set(path, fishEnableClever);
							} else if (fishData.equalsIgnoreCase("Offset_Power")) {
								final String path = "Configuration.Fish.Offset_Power";
								final double fishOffsetPower = fishDataSection.getDouble(fishData);
								
								config.set(path, fishOffsetPower);
							} else if (fishData.equalsIgnoreCase("Default_Type")) {
								final String path = "Configuration.Fish.Default_Type";
								final String fishDefaultTypeText = fishDataSection.getString(fishData);
								final EntityType fishDefaultType = EntityUtil.getEntityType(fishDefaultTypeText);
								
								config.set(path, fishDefaultType != null ? fishDefaultType : EntityType.SQUID);
							} else if (fishData.equalsIgnoreCase("Default_Resistance")) {
								final String path = "Configuration.Fish.Default_Resistance";
								final double fishDefaultResistance = fishDataSection.getDouble(fishData);
								
								config.set(path, fishDefaultResistance);
							} else if (fishData.equalsIgnoreCase("Default_Power")) {
								final String path = "Configuration.Fish.Default_Power";
								final double fishDefaultPower = fishDataSection.getDouble(fishData);
								
								config.set(path, fishDefaultPower);
							} else if (fishData.equalsIgnoreCase("Default_Chance_Turn_Around")) {
								final String path = "Configuration.Fish.Default_Chance_Turn_Around";
								final double fishDefaultChanceTurnAround = fishDataSection.getDouble(fishData);
								
								config.set(path, fishDefaultChanceTurnAround);
							} else if (fishData.equalsIgnoreCase("Default_Max_Power")) {
								final String path = "Configuration.Fish.Default_Max_Power";
								final double fishDefaultMaxPower = fishDataSection.getDouble(fishData);
								
								config.set(path, fishDefaultMaxPower);
							} else if (fishData.equalsIgnoreCase("Default_Max_Speed")) {
								final String path = "Configuration.Fish.Default_Max_Speed";
								final double fishDefaultMaxSpeed = fishDataSection.getDouble(fishData);
								
								config.set(path, fishDefaultMaxSpeed);
							} else if (fishData.equalsIgnoreCase("Default_Max_Dive")) {
								final String path = "Configuration.Fish.Default_Max_Dive";
								final double fishDefaultMaxDive = fishDataSection.getDouble(fishData);
								
								config.set(path, fishDefaultMaxDive);
							} else if (fishData.equalsIgnoreCase("Default_Exp")) {
								final String path = "Configuration.Fish.Default_Exp";
								final double fishDefaultExp = fishDataSection.getDouble(fishData);
								
								config.set(path, fishDefaultExp);
							} else if (fishData.equalsIgnoreCase("Format_Name")) {
								final String path = "Configuration.Fish.Format_Name";
								final String fishFormatName = fishDataSection.getString(fishData);
								
								config.set(path, fishFormatName);
							}
						} 
					} else if (data.equalsIgnoreCase("Treasure")) {
						final ConfigurationSection treasureDataSection = dataSection.getConfigurationSection(data);
						
						for (String treasureData : treasureDataSection.getKeys(false)) {
							if (treasureData.equalsIgnoreCase("Enable_Replace_Fish")) {
								final String path = "Configuration.Treasure.Enable_Replace_Fish";
								final boolean treasureEnableReplaceFish = treasureDataSection.getBoolean(treasureData);
								
								config.set(path, treasureEnableReplaceFish);
							} else if (treasureData.equalsIgnoreCase("Enable_Plugin_Treasure")) {
								final String path = "Configuration.Treasure.Enable_Plugin_Treasure";
								final boolean treasureEnablePluginTreasure = treasureDataSection.getBoolean(treasureData);

								config.set(path, treasureEnablePluginTreasure);
							}
						}
					} else if (data.equalsIgnoreCase("Fishing")) {
						final ConfigurationSection fishingDataSection = dataSection.getConfigurationSection(data);
						
						for (String fishingData : fishingDataSection.getKeys(false)) {
							if (fishingData.equalsIgnoreCase("Enable_Realistic")) {
								final String path = "Configuration.Fishing.Enable_Realistic";
								final boolean fishingEnableRealistic = fishingDataSection.getBoolean(fishingData);
								
								config.set(path, fishingEnableRealistic);
							} else if (fishingData.equalsIgnoreCase("Enable_Auto_Catch")) {
								final String path = "Configuration.Fishing.Enable_Auto_Catch";
								final boolean fishingEnableAutoCatch = fishingDataSection.getBoolean(fishingData);
								
								config.set(path, fishingEnableAutoCatch);
							} else if (fishingData.equalsIgnoreCase("Max_Level")) {
								final String path = "Configuration.Fishing.Max_Level";
								final int fishingMaxLevel = fishingDataSection.getInt(fishingData);
								
								config.set(path, fishingMaxLevel);
							} else if (fishingData.equalsIgnoreCase("Max_Bonus_Effectiveness_Level")) {
								final String path = "Configuration.Fishing.Max_Bonus_Effectiveness_Level";
								final int fishingMaxBonusEffectivenessLevel = fishingDataSection.getInt(fishingData);
								
								config.set(path, fishingMaxBonusEffectivenessLevel);
							} else if (fishingData.equalsIgnoreCase("Max_Bonus_Endurance_Level")) {
								final String path = "Configuration.Fishing.Max_Bonus_Endurance_Level";
								final int fishingMaxBonusEnduranceLevel = fishingDataSection.getInt(fishingData);
								
								config.set(path, fishingMaxBonusEnduranceLevel);
							} else if (fishingData.equalsIgnoreCase("Max_Bonus_Speed_Level")) {
								final String path = "Configuration.Fishing.Max_Bonus_Speed_Level";
								final int fishingMaxBonusSpeedLevel = fishingDataSection.getInt(fishingData);
								
								config.set(path, fishingMaxBonusSpeedLevel);
							} else if (fishingData.equalsIgnoreCase("Max_Bonus_Effectiveness")) {
								final String path = "Configuration.Fishing.Max_Bonus_Effectiveness";
								final double fishingMaxBonusEffectiveness = fishingDataSection.getDouble(fishingData);
								
								config.set(path, fishingMaxBonusEffectiveness);
							} else if (fishingData.equalsIgnoreCase("Max_Bonus_Endurance")) {
								final String path = "Configuration.Fishing.Max_Bonus_Endurance";
								final double fishingMaxBonusEndurance = fishingDataSection.getDouble(fishingData);
								
								config.set(path, fishingMaxBonusEndurance);
							} else if (fishingData.equalsIgnoreCase("Max_Bonus_Speed")) {
								final String path = "Configuration.Fishing.Max_Bonus_Speed";
								final double fishingMaxSBonuSpeed = fishingDataSection.getDouble(fishingData);
								
								config.set(path, fishingMaxSBonuSpeed);
							} else if (fishingData.equalsIgnoreCase("Formula_Exp_A")) {
								final String path = "Configuration.Fishing.Formula_Exp_A";
								final double fishingFormulaExpA = fishingDataSection.getDouble(fishingData);
								
								config.set(path, fishingFormulaExpA);
							} else if (fishingData.equalsIgnoreCase("Formula_Exp_B")) {
								final String path = "Configuration.Fishing.Formula_Exp_B";
								final double fishingFormulaExpB = fishingDataSection.getDouble(fishingData);
								
								config.set(path, fishingFormulaExpB);
							} else if (fishingData.equalsIgnoreCase("Formula_Exp_C")) {
								final String path = "Configuration.Fishing.Formula_Exp_C";
								final double fishingFormulaExpC = fishingDataSection.getDouble(fishingData);
								
								config.set(path, fishingFormulaExpC);
							} else if (fishingData.equalsIgnoreCase("Information_Type")) {
								final String path = "Configuration.Fishing.Information_Type";
								final String fishingInformationTypeText = fishingDataSection.getString(fishingData);
								final int fishingInformationType;
								
								switch (fishingInformationTypeText.toUpperCase()) {
								case "ALL" : fishingInformationType = 0; break;
								case "BOTH" : fishingInformationType = 0; break;
								case "ACTIONBAR" : fishingInformationType = 1; break;
								case "BOSSBAR" : fishingInformationType = 2; break;
								default : fishingInformationType = -1;
								}
								
								config.set(path, fishingInformationType);
							} else if (fishingData.equalsIgnoreCase("Format_Actionbar_Fight")) {
								final String path = "Configuration.Fishing.Format_Actionbar_Fight";
								final String fishingFormatActionbarFight = fishingDataSection.getString(fishingData);
								
								config.set(path, fishingFormatActionbarFight);
							} else if (fishingData.equalsIgnoreCase("Format_Actionbar_Pull")) {
								final String path = "Configuration.Fishing.Format_Actionbar_Pull";
								final String fishingFormatActionbarPull = fishingDataSection.getString(fishingData);
								
								config.set(path, fishingFormatActionbarPull);
							} else if (fishingData.equalsIgnoreCase("Format_BossBar_Fight")) {
								final String path = "Configuration.Fishing.Format_BossBar_Fight";
								final String fishingFormatBossBarFight = fishingDataSection.getString(fishingData);
								
								config.set(path, fishingFormatBossBarFight);
							} else if (fishingData.equalsIgnoreCase("Format_BossBar_Pull")) {
								final String path = "Configuration.Fishing.Format_BossBar_Pull";
								final String fishingFormatBossBarPull = fishingDataSection.getString(fishingData);
								
								config.set(path, fishingFormatBossBarPull);
							}
						}
					} else if (data.equalsIgnoreCase("BossBar")) {
						final ConfigurationSection bossDataSection = dataSection.getConfigurationSection(data);
						
						for (String bossData : bossDataSection.getKeys(false)) {
							if (bossData.equalsIgnoreCase("Color")) {
								final String path = "Configuration.BossBar.Color";
								final String bossBarColorText = bossDataSection.getString(bossData);
								final Color bossBarColor = Color.getColor(bossBarColorText);
								
								config.set(path, bossBarColor != null ? bossBarColor : Color.GREEN);
							} else if (bossData.equalsIgnoreCase("Style")) {
								final String path = "Configuration.BossBar.Style";
								final String bossBarColorText = bossDataSection.getString(bossData);
								final Style bossBarStyle = Style.getStyle(bossBarColorText);
								
								config.set(path, bossBarStyle != null ? bossBarStyle : Style.PROGRESS);
							}
						}
					}
				}
			}
		}
	}
	
	private final void loadOldConfig(FileConfiguration config, FileConfiguration source) {
		for (String data : source.getKeys(false)) {
			if (data.equalsIgnoreCase("Metrics_Message")) {
				final String path = "Configuration.Metrics.Message";
				final boolean metricsMessage = source.getBoolean(data);
				
				config.set(path, metricsMessage);
			} else if (data.equalsIgnoreCase("Hook_Message")) {
				final String path = "Configuration.Hook.Message";
				final boolean hookMessage = source.getBoolean(data);
				
				config.set(path, hookMessage);
			} else if (data.equalsIgnoreCase("Code_Tooltip")) {
				final String path = "Configuration.Utility.Tooltip";
				final String utilityTooltip = source.getString(data);
				
				config.set(path, utilityTooltip);
			} else if (data.equalsIgnoreCase("Hook_Message")) {
				final String path = "Configuration.Utility.Currency";
				final String utilityCurrency = source.getString(data);
				
				config.set(path, utilityCurrency);
			} else if (data.equalsIgnoreCase("Effect_Range")) {
				final String path = "Configuration.Effect.Range";
				final double  effectRange = source.getDouble(data);
				
				config.set(path, effectRange);
			} else if (data.equalsIgnoreCase("List_Content")) {
				final String path = "Configuration.List.Content";
				final int listContent = source.getInt(data);
				
				config.set(path, listContent);
			} else if (data.equalsIgnoreCase("Total_Bar_Life")) {
				final String path = "Configuration.Bar.Length";
				final int barLength = source.getInt(data);
				
				config.set(path, barLength);
			} else if (data.equalsIgnoreCase("Code_Bar")) {
				final String path = "Configuration.Bar.Symbol";
				final String barSymbol = source.getString(data);
				
				config.set(path, barSymbol);
			} else if (data.equalsIgnoreCase("Code_Bar_Life")) {
				final String path = "Configuration.Bar.Fill";
				final String barFill = source.getString(data);
				
				config.set(path, barFill);
			} else if (data.equalsIgnoreCase("Code_Bar_Empty")) {
				final String path = "Configuration.Bar.Empty";
				final String barEmpty = source.getString(data);
				
				config.set(path, barEmpty);
			} else if (data.equalsIgnoreCase("Enable_Hook_mcMMO")) {
				final String path = "Configuration.Support.Enable_mcMMO";
				final boolean supportEnableMcMMO = source.getBoolean(data);
				
				config.set(path, supportEnableMcMMO);
			} else if (data.equalsIgnoreCase("Enable_Hook_Jobs")) {
				final String path = "Configuration.Support.Enable_JobsReborn";
				final boolean supportEnableJobsReborn = source.getBoolean(data);
				
				config.set(path, supportEnableJobsReborn);
			} else if (data.equalsIgnoreCase("Exp_Multiplier_mcMMO")) {
				final String path = "Configuration.Support.Exp_Multiplier_McMMO";
				final double supportExpMultiplierMcMMO = source.getDouble(data);
				
				config.set(path, supportExpMultiplierMcMMO);
			} else if (data.equalsIgnoreCase("Exp_Multiplier_Jobs")) {
				final String path = "Configuration.Support.Exp_Multiplier_JobsReborn";
				final double supportExpMultiplierJobsReborn = source.getDouble(data);
				
				config.set(path, supportExpMultiplierJobsReborn);
			} else if (data.equalsIgnoreCase("Enable_Buy_Bait")) {
				final String path = "Configuration.Bait.Enable_Buy";
				final boolean baitEnableBuy = source.getBoolean(data);
				
				config.set(path, baitEnableBuy);
			} else if (data.equalsIgnoreCase("Enable_Sell_Bait")) {
				final String path = "Configuration.Bait.Enable_Sell";
				final boolean baitEnableSell = source.getBoolean(data);
				
				config.set(path, baitEnableSell);
			} else if (data.equalsIgnoreCase("Enable_Sell_Fish")) {
				final String path = "Configuration.Fish.Enable_Sell";
				final boolean fishEnableSell = source.getBoolean(data);
				
				config.set(path, fishEnableSell);
			} else if (data.equalsIgnoreCase("Enable_Fish_Name")) {
				final String path = "Configuration.Fish.Enable_Name";
				final boolean fishEnableName = source.getBoolean(data);
				
				config.set(path, fishEnableName);
			} else if (data.equalsIgnoreCase("Enable_Fish_Agressive")) {
				final String path = "Configuration.Fish.Enable_Aggresive";
				final boolean fishEnableAggresive = source.getBoolean(data);
				
				config.set(path, fishEnableAggresive);
			} else if (data.equalsIgnoreCase("Enable_Fish_Clever")) {
				final String path = "Configuration.Fish.Enable_Clever";
				final boolean fishEnableClever = source.getBoolean(data);
				
				config.set(path, fishEnableClever);
			} else if (data.equalsIgnoreCase("Offset_Fish_Power")) {
				final String path = "Configuration.Fish.Offset_Power";
				final double fishOffsetPower = source.getDouble(data);
				
				config.set(path, fishOffsetPower);
			} else if (data.equalsIgnoreCase("Default_Fish_Type")) {
				final String path = "Configuration.Fish.Default_Type";
				final String fishDefaultTypeText = source.getString(data);
				final EntityType fishDefaultType = EntityUtil.getEntityType(fishDefaultTypeText);
				
				config.set(path, fishDefaultType != null ? fishDefaultType : EntityType.SQUID);
			} else if (data.equalsIgnoreCase("Default_Fish_Resistance")) {
				final String path = "Configuration.Fish.Default_Resistance";
				final double fishDefaultResistance = source.getDouble(data);
				
				config.set(path, fishDefaultResistance);
			} else if (data.equalsIgnoreCase("Default_Fish_Power")) {
				final String path = "Configuration.Fish.Default_Power";
				final double fishDefaultPower = source.getDouble(data);
				
				config.set(path, fishDefaultPower);
			} else if (data.equalsIgnoreCase("Default_Fish_Chance_Turn_Around")) {
				final String path = "Configuration.Fish.Default_Chance_Turn_Around";
				final double fishDefaultChanceTurnAround = source.getDouble(data);
				
				config.set(path, fishDefaultChanceTurnAround);
			} else if (data.equalsIgnoreCase("Default_Max_Fish_Power")) {
				final String path = "Configuration.Fish.Default_Max_Power";
				final double fishDefaultMaxPower = source.getDouble(data);
				
				config.set(path, fishDefaultMaxPower);
			} else if (data.equalsIgnoreCase("Default_Max_Fish_Speed")) {
				final String path = "Configuration.Fish.Default_Max_Speed";
				final double fishDefaultMaxSpeed = source.getDouble(data);
				
				config.set(path, fishDefaultMaxSpeed);
			} else if (data.equalsIgnoreCase("Default_Max_Fish_Dive")) {
				final String path = "Configuration.Fish.Default_Max_Dive";
				final double fishDefaultMaxDive = source.getDouble(data);
				
				config.set(path, fishDefaultMaxDive);
			} else if (data.equalsIgnoreCase("Default_Exp")) {
				final String path = "Configuration.Fish.Default_Exp";
				final double fishDefaultExp = source.getDouble(data);
				
				config.set(path, fishDefaultExp);
			}
		} 
	}
	
	private final void moveOldFile() {
		final String pathSource = "config.yml";
		final String pathTarget = PATH_FILE;
		final File fileSourcce = FileUtil.getFile(plugin, pathSource);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSourcce.exists()) {
			FileUtil.moveFileSilent(fileSourcce, fileTarget);
		}
	}
}