package com.praya.dreamfish;

import java.util.List;

import com.praya.agarthalib.utility.ServerUtil;
import com.praya.dreamfish.treasure.TreasureListener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.handler.HandlerDatabase;
import com.praya.dreamfish.listener.custom.ListenerMenuOpen;
import com.praya.dreamfish.listener.custom.ListenerPlayerCatchFish;
import com.praya.dreamfish.listener.custom.ListenerPlayerExtendLures;
import com.praya.dreamfish.listener.custom.ListenerPlayerFishingExpChange;
import com.praya.dreamfish.listener.custom.ListenerPlayerFishingLevelChange;
import com.praya.dreamfish.listener.custom.ListenerPlayerPullLures;
import com.praya.dreamfish.listener.main.ListenerPlayerInteract;
import com.praya.dreamfish.listener.main.ListenerEntityCombust;
import com.praya.dreamfish.listener.main.ListenerEntityDamage;
import com.praya.dreamfish.listener.main.ListenerEntityDamageByEntity;
import com.praya.dreamfish.listener.main.ListenerItemHeld;
import com.praya.dreamfish.listener.main.ListenerPlayerFish;
import com.praya.dreamfish.listener.main.ListenerPlayerQuit;
import com.praya.dreamfish.listener.support.ListenerJobsExpGain;
import com.praya.dreamfish.listener.support.ListenerJobsPayment;
import com.praya.dreamfish.listener.support.ListenerMcMMOPlayerXpGain;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerFishingModeManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.manager.plugin.PluginManager;
import com.praya.dreamfish.manager.task.TaskManager;
import com.praya.dreamfish.player.PlayerFishingMode;
import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.face.Agartha;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.PluginUtil;
import com.praya.agarthalib.utility.ServerEventUtil;

public class DreamFish extends JavaPlugin implements Agartha {
	
	private final String type = "Free";
	private final String placeholder = "dreamfish";
	
	private DreamFishConfig mainConfig;
	
	private PluginManager pluginManager;
	private PlayerManager playerManager;
	private GameManager gameManager;
	private TaskManager taskManager;
	
	@Override
	public String getPluginName() {
		return this.getName();
	}

	@Override
	public String getPluginType() {
		return this.type;
	}
	
	@Override
	public String getPluginVersion() {
		return getDescription().getVersion();
	}
	
	@Override
	public String getPluginPlaceholder() {
		return this.placeholder;
	}

	@Override
	public String getPluginWebsite() {
		return getPluginManager().getPluginPropertiesManager().getPluginWebsite();
	}

	@Override
	public String getPluginLatest() {
		return getPluginManager().getPluginPropertiesManager().getPluginVersion(getPluginType());
	}
	
	@Override
	public List<String> getPluginDevelopers() {
		return getPluginManager().getPluginPropertiesManager().getPluginDevelopers();
	}
	
	public final DreamFishConfig getMainConfig() {
		return this.mainConfig;
	}
	
	public final PluginManager getPluginManager() {
		return this.pluginManager;
	}
	
	public final PlayerManager getPlayerManager() {
		return this.playerManager;
	}
	
	public final GameManager getGameManager() {
		return this.gameManager;
	}
	
	public final TaskManager getTaskManager() {
		return this.taskManager;
	}
	
	@Override
	public void onEnable() {
		registerConfig();
		registerManager();
		registerListener();
		registerPlaceholder();				
	}
	
	private final void registerConfig() {
		this.mainConfig = new DreamFishConfig(this);
	}
	
	private final void registerManager() {
		this.pluginManager = new DreamFishPluginMemory(this);
		this.playerManager = new DreamFishPlayerMemory(this);
		this.gameManager = new DreamFishGameMemory(this);
		this.taskManager = new DreamFishTaskMemory(this);
	}
	
	private final void registerPlaceholder() {
		getPluginManager().getPlaceholderManager().registerAll();
	}
	
	private final void registerListener() {
		final Listener listenerMenuOpen = new ListenerMenuOpen(this);
		final Listener listenerPlayerCatchFish = new ListenerPlayerCatchFish(this);
		final Listener listenerPlayerExtendLures = new ListenerPlayerExtendLures(this);
		final Listener listenerPlayerFishingExpChange = new ListenerPlayerFishingExpChange(this);
		final Listener listenerPlayerFishingLevelChange = new ListenerPlayerFishingLevelChange(this);
		final Listener listenerPlayerPullLures = new ListenerPlayerPullLures(this);
		final Listener listenerClick = new ListenerPlayerInteract(this);
		final Listener listenerPlayerFish = new ListenerPlayerFish(this);
		final Listener listenerPlayerQuit = new ListenerPlayerQuit(this);
		final Listener listenerEntityCombust = new ListenerEntityCombust(this);
		final Listener listenerEntityDamage = new ListenerEntityDamage(this);
		final Listener listenerEntityDamageByEntity = new ListenerEntityDamageByEntity(this);
		final Listener listenerItemHeld = new ListenerItemHeld(this);
		final Listener listenerTreasure = new TreasureListener(this);
		
		ServerEventUtil.registerEvent(this, listenerMenuOpen);
		ServerEventUtil.registerEvent(this, listenerPlayerCatchFish);
		ServerEventUtil.registerEvent(this, listenerPlayerExtendLures);
		ServerEventUtil.registerEvent(this, listenerPlayerFishingExpChange);
		ServerEventUtil.registerEvent(this, listenerPlayerFishingLevelChange);
		ServerEventUtil.registerEvent(this, listenerPlayerPullLures);
		ServerEventUtil.registerEvent(this, listenerClick);
		ServerEventUtil.registerEvent(this, listenerPlayerFish);
		ServerEventUtil.registerEvent(this, listenerPlayerQuit);
		ServerEventUtil.registerEvent(this, listenerEntityCombust);
		ServerEventUtil.registerEvent(this, listenerEntityDamage);
		ServerEventUtil.registerEvent(this, listenerEntityDamageByEntity);
		ServerEventUtil.registerEvent(this, listenerItemHeld);
		ServerEventUtil.registerEvent(this, listenerTreasure);
		
		if (PluginUtil.isPluginInstalled("Jobs")) {
			final Listener listenerJobsExpGain = new ListenerJobsExpGain(this);
			final Listener listenerJobsPayment = new ListenerJobsPayment(this);
			
			ServerEventUtil.registerEvent(this, listenerJobsExpGain);
			ServerEventUtil.registerEvent(this, listenerJobsPayment);
		}
		
		if (PluginUtil.isPluginInstalled("mcMMO")) {
			final Listener listenerMcMMOPlayerXpGain = new ListenerMcMMOPlayerXpGain(this);
			
			ServerEventUtil.registerEvent(this, listenerMcMMOPlayerXpGain);
		}
	}
	
	@Override
	public void onDisable() {
		final PlayerFishingManager playerFishingManager = getPlayerManager().getPlayerFishingManager();
		final PlayerFishingModeManager playerFishingModeManager = getPlayerManager().getPlayerFishingModeManager();
		final String bossBarID = "DreamFish Fishing Mode";
		
		HandlerDatabase.closeAllConnection();
		Bukkit.getScheduler().cancelTasks(this);
		
		for (Player player : PlayerUtil.getOnlinePlayers()) {
			playerFishingManager.removeFromCache(player);
			Bridge.getBridgeMessage().removeBossBar(player, bossBarID);	
		}
		
		for (PlayerFishingMode playerFishingMode : playerFishingModeManager.getAllPlayerFishingMode()) {
			playerFishingMode.unregister();
		}
	}
}
