package com.praya.dreamfish.menu;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.handler.HandlerMenuExecutor;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.game.BaitManager;
import com.praya.dreamfish.manager.game.FishManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.manager.game.MenuManager;
import api.praya.agarthalib.builder.support.SupportVault;
import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;
import core.praya.agarthalib.builder.menu.Menu;
import core.praya.agarthalib.builder.menu.MenuExecutor;
import core.praya.agarthalib.builder.menu.MenuSlotAction.ActionType;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.InventoryUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;

public final class MenuDreamFishExecutor extends HandlerMenuExecutor implements MenuExecutor {

	public MenuDreamFishExecutor(DreamFish plugin) {
		super(plugin);
	}

	@Override
	public void onClick(Player player, Menu menu, ActionType actionType, String... args) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final GameManager gameManager = plugin.getGameManager();
		final MenuManager menuManager = gameManager.getMenuManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		final FishManager fishManager = gameManager.getFishManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();;
		final SupportVault supportVault = supportManagerAPI.getSupportVault();
		
		if (args.length > 0) {
			final String label = args[0];
			
			if (label.equalsIgnoreCase("DreamFish")) {
				if (args.length > 1) {
					final String key = args[1];
					
					if (key.equalsIgnoreCase("Menu")) {
						if (args.length > 2) {
							final MenuDreamFish menuDreamFish = menuManager.getMenuDreamFish();
							final String menuID = args[2];
							final int page;
							
							if (args.length > 3) {
								final String textPage = args[3];
								
								if (MathUtil.isNumber(textPage)) {
									page = MathUtil.parseInteger(textPage);
								} else {
									page = 1;
								}
							} else {
								page = 1;
							}
							
							if (menuID.equalsIgnoreCase("Home")) {
								menuDreamFish.openMenuHome(player);
								return;
							} else if (menuID.equalsIgnoreCase("Player")) {
								menuDreamFish.openMenuPlayer(player);
								return;
							} else if (menuID.equalsIgnoreCase("Guide")) {
								menuDreamFish.openMenuGuide(player);
							} else if (menuID.equalsIgnoreCase("Progress")) {
								menuDreamFish.openMenuProgress(player);
								return;
							} else if (menuID.equalsIgnoreCase("Progress_Bait")) {
								menuDreamFish.openMenuProgressBait(player, page);
								return;
							} else if (menuID.equalsIgnoreCase("Progress_Fish")) {
								menuDreamFish.openMenuProgressFish(player, page);
								return;
							} else if (menuID.equalsIgnoreCase("Shop")) {
								menuDreamFish.openMenuShop(player);
								return;
							} else if (menuID.equalsIgnoreCase("Shop_Bait_Buy")) {
								menuDreamFish.openMenuBaitBuy(player, page);
								return;
							} else if (menuID.equalsIgnoreCase("Shop_Bait_Sell")) {
								menuDreamFish.openMenuBaitSell(player, page);
							} else if (menuID.equalsIgnoreCase("Shop_Fish_Sell")) {
								menuDreamFish.openMenuFishSell(player, page);
							}
						}
					} else if (key.equalsIgnoreCase("Action")) {
						if (args.length > 2) {
							final String action = args[2];
							
							if (action.equalsIgnoreCase("Bait")) {
								if (args.length > 3) {
									final String subAction = args[3];
									
									if (subAction.equalsIgnoreCase("Buy")) {
										if (supportVault == null) {
											final MessageBuild messageBuild = Language.PLUGIN_NOT_INSTALLED.getMessage(player);

											messageBuild.sendMessage(player, "plugin", "Vault");
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										} else if (args.length < 5) {
											final MessageBuild messageBuild = Language.ARGUMENT_NOT_COMPLETE.getMessage(player);
											
											messageBuild.sendMessage(player);
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										} else {
											final String bait = args[4];
											final BaitProperties baitProperties = baitManager.getBaitProperties(bait);
											
											if (baitProperties == null) {
												final MessageBuild messageBuild = Language.ITEM_NOT_EXIST.getMessage(player);
												
												messageBuild.sendMessage(player, "nameid", bait);
												SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
												return;
											} else {
												final double money = supportManagerAPI.getSupportVault().getBalance(player);
												final double price = baitProperties.getPrice();
												final ItemStack item = baitProperties.getItem().clone();
												
												int amount = 1;
												
												if (args.length > 5) {
													final String textAmount = args[5];
													
													if (MathUtil.isNumber(textAmount)) {
														amount = MathUtil.parseInteger(textAmount);
														amount = MathUtil.limitInteger(amount, 1, 64);
													} else {
														final MessageBuild messageBuild = Language.ARGUMENT_INVALID_VALUE.getMessage(player);
														
														messageBuild.sendMessage(player);
														SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
														return;
													}
												}
												
												final double totalPrice = amount*price;
												
												if (money < totalPrice) {
													final MessageBuild messageBuild = Language.ARGUMENT_LACK_MONEY.getMessage(player);
													
													messageBuild.sendMessage(player);
													SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
													return;
												} else {
													final String symbolCurrency = mainConfig.getUtilityCurrency();
													final HashMap<String, String> map = new HashMap<String, String>();
													
													MessageBuild messageBuild = Language.COMMAND_BAIT_BUY_SUCCESS.getMessage(player);
													
													map.put("amount", String.valueOf(amount));
													map.put("bait", bait);
													map.put("money", String.valueOf(MathUtil.roundNumber(totalPrice, 2)));
													map.put("symbol_currency", symbolCurrency);
													
													messageBuild.sendMessage(player, map);
													supportManagerAPI.getSupportVault().remBalance(player, totalPrice);
													InventoryUtil.addItem(player.getInventory(), item, amount);
													SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
													return;
												}
											}
										}
									} else if (subAction.equalsIgnoreCase("Sell")) {
										if (supportVault == null) {
											final MessageBuild messageBuild = Language.PLUGIN_NOT_INSTALLED.getMessage(player);

											messageBuild.sendMessage(player, "plugin", "Vault");
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										} else if (args.length < 5) {
											final MessageBuild messageBuild = Language.ARGUMENT_NOT_COMPLETE.getMessage(player);
											
											messageBuild.sendMessage(player);
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										} else {
											final String bait = args[4];
											final BaitProperties baitProperties = baitManager.getBaitProperties(bait);
											
											if (baitProperties == null) {
												final MessageBuild messageBuild = Language.ITEM_NOT_EXIST.getMessage(player);
												
												messageBuild.sendMessage(player, "nameid", bait);
												SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
												return;
											} else {
												final double price = baitProperties.getPrice();
												final ItemStack item = baitProperties.getItem().clone();
												
												int amount = 1;
												
												if (args.length > 5) {
													final String textAmount = args[5];
													
													if (MathUtil.isNumber(textAmount)) {
														amount = MathUtil.parseInteger(textAmount);
														amount = MathUtil.limitInteger(amount, 1, 64);
													} else {
														final MessageBuild messageBuild = Language.ARGUMENT_INVALID_VALUE.getMessage(player);
														
														messageBuild.sendMessage(player);
														SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
														return;
													}
												}
												
												final int totalItem = InventoryUtil.getCountItem(player.getInventory(), item);
												
												amount = MathUtil.limitInteger(amount, 0, totalItem);
												
												if (totalItem == 0) {
													final MessageBuild messageBuild = Language.ARGUMENT_LACK_ITEM.getMessage(player);
													
													messageBuild.sendMessage(player);
													SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
													return;
												} else {
													final String symbolCurrency = mainConfig.getUtilityCurrency();
													final double totalPrice = amount*price;
													final MessageBuild messageBuild = Language.COMMAND_BAIT_SELL_SUCCESS.getMessage(player);
													final HashMap<String, String> map = new HashMap<String, String>();
													
													map.put("amount", String.valueOf(amount));
													map.put("bait", bait);
													map.put("money", String.valueOf(MathUtil.roundNumber(totalPrice, 2)));
													map.put("symbol_currency", symbolCurrency);
													
													messageBuild.sendMessage(player, map);
													supportManagerAPI.getSupportVault().addBalance(player, totalPrice);
													InventoryUtil.removeItem(player.getInventory(), item, amount);
													SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
													return;
												}
											}
										}
									}
								} 
							} else if (action.equalsIgnoreCase("Fish")) {
								final String subAction = args[3];
								
								if (subAction.equalsIgnoreCase("Sell")) {
									if (supportVault == null) {
										final MessageBuild messageBuild = Language.PLUGIN_NOT_INSTALLED.getMessage(player);

										messageBuild.sendMessage(player, "plugin", "Vault");
										SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
										return;
									} else if (args.length < 5) {
										final MessageBuild messageBuild = Language.ARGUMENT_NOT_COMPLETE.getMessage(player);
										
										messageBuild.sendMessage(player);
										SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
										return;
									} else {
										final String fish = args[4];
										final FishProperties fishProperties = fishManager.getFishProperties(fish);
										
										if (fishProperties == null) {
											final MessageBuild messageBuild = Language.ITEM_NOT_EXIST.getMessage(player);
											
											messageBuild.sendMessage(player, "nameid", fish);
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										} else {
											final double price = fishProperties.getPrice();
											final ItemStack item = fishProperties.getItem().clone();
											
											int amount = 1;
											
											if (args.length > 5) {
												final String textAmount = args[5];
												
												if (MathUtil.isNumber(textAmount)) {
													amount = MathUtil.parseInteger(textAmount);
													amount = MathUtil.limitInteger(amount, 1, 64);
												} else {
													final MessageBuild messageBuild = Language.ARGUMENT_INVALID_VALUE.getMessage(player);
													
													messageBuild.sendMessage(player);
													SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
													return;
												}
											}
											
											final int totalItem = InventoryUtil.getCountItem(player.getInventory(), item);
											
											amount = MathUtil.limitInteger(amount, 0, totalItem);
											
											if (totalItem == 0) {
												final MessageBuild messageBuild = Language.ARGUMENT_LACK_ITEM.getMessage(player);
												
												messageBuild.sendMessage(player);
												SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
												return;
											} else {
												final String symbolCurrency = mainConfig.getUtilityCurrency();
												final double totalPrice = amount*price;
												final MessageBuild messageBuild = Language.COMMAND_DREAMFISH_SELL_SUCCESS.getMessage(player);
												final HashMap<String, String> map = new HashMap<String, String>();
												
												map.put("amount", String.valueOf(amount));
												map.put("fish", fish);
												map.put("money", String.valueOf(MathUtil.roundNumber(totalPrice, 2)));
												map.put("symbol_currency", symbolCurrency);
												
												messageBuild.sendMessage(player, map);
												supportManagerAPI.getSupportVault().addBalance(player, totalPrice);
												InventoryUtil.removeItem(player.getInventory(), item, amount);
												SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
												return;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
