package com.praya.dreamfish.menu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.block.Biome;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.SortUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.DreamFishConfig;
import com.praya.dreamfish.bait.BaitFishing;
import com.praya.dreamfish.bait.BaitProperties;
import com.praya.dreamfish.fish.FishProperties;
import com.praya.dreamfish.handler.HandlerMenu;
import com.praya.dreamfish.item.ItemRequirement;
import com.praya.dreamfish.language.Language;
import com.praya.dreamfish.manager.game.BaitManager;
import com.praya.dreamfish.manager.game.FishManager;
import com.praya.dreamfish.manager.game.GameManager;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.manager.plugin.PlaceholderManager;
import com.praya.dreamfish.manager.plugin.PluginManager;
import com.praya.dreamfish.player.PlayerFishing;

import core.praya.agarthalib.builder.menu.Menu;
import core.praya.agarthalib.builder.menu.MenuExecutor;
import core.praya.agarthalib.builder.menu.MenuGUI;
import core.praya.agarthalib.builder.menu.MenuSlot;
import core.praya.agarthalib.builder.menu.MenuSlotAction.ActionCategory;
import core.praya.agarthalib.builder.menu.MenuSlotAction.ActionType;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.builder.text.Text;
import core.praya.agarthalib.builder.text.TextLine;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;

public final class MenuDreamFish extends HandlerMenu {

	public MenuDreamFish(DreamFish plugin, MenuExecutor menuExecutor) {
		super(plugin, menuExecutor);
	}
	
	public final void openMenuHome(Player player) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		
		if (player != null) {
			final String permission = mainConfig.getMenuPermissionHome();
			
			if (!SenderUtil.hasPermission(player, permission)) {
				final MessageBuild message = Language.PERMISSION_LACK.getMessage(player);
				
				message.sendMessage(player, "permission", permission);
				SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final MenuExecutor executor = getMenuExecutor();
				final String playerName = player.getName();
				
				final String id = "DreamFish Home";
				final String textTitle = Language.MENU_PAGE_TITLE_HOME.getText(player);
				final int row = 3;
				final int size = row * 9;
				final Text title = new TextLine(textTitle);
				final String prefix = placeholderManager.getPlaceholder("prefix");
				final String headerPlayer = Language.MENU_ITEM_HEADER_HOME_PLAYER.getText(player);
				final String headerGuide = Language.MENU_ITEM_HEADER_HOME_GUIDE.getText(player); 
				final String headerShop = Language.MENU_ITEM_HEADER_HOME_SHOP.getText(player);
				final int[] arraySlotPaneWhite = new int[] {0, 8, 9, 17, 18, 26};
				final MenuSlot menuSlotPlayer = new MenuSlot(11);
				final MenuSlot menuSlotGuide = new MenuSlot(13);
				final MenuSlot menuSlotShop = new MenuSlot(15);
				final HashMap<Integer, MenuSlot> mapSlot = new HashMap<Integer, MenuSlot>();
				
				List<String> lorePlayer = Language.MENU_ITEM_LORES_HOME_PLAYER.getListText(player);
				List<String> loreGuide = Language.MENU_ITEM_LORES_HOME_GUIDE.getListText(player);
				List<String> loreShop = Language.MENU_ITEM_LORES_HOME_SHOP.getListText(player);
				
				final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
				final ItemStack itemPaneWhite = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, prefix, 1);
				final ItemStack itemPlayer = EquipmentUtil.createPlayerHead(playerName, headerPlayer, lorePlayer);
				final ItemStack itemGuide = EquipmentUtil.createItem(MaterialEnum.WRITABLE_BOOK, headerGuide, 1, loreGuide);
				final ItemStack itemShop = EquipmentUtil.createItem(MaterialEnum.TROPICAL_FISH, headerShop, 1, loreShop);
				
				menuSlotPlayer.setItem(itemPlayer);
				menuSlotGuide.setItem(itemGuide);
				menuSlotShop.setItem(itemShop);
				
				menuSlotPlayer.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Player");
				menuSlotGuide.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Guide");
				menuSlotShop.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop");
		
				mapSlot.put(menuSlotPlayer.getSlot(), menuSlotPlayer);
				mapSlot.put(menuSlotGuide.getSlot(), menuSlotGuide);
				mapSlot.put(menuSlotShop.getSlot(), menuSlotShop);
				
				for (int slot : arraySlotPaneWhite) {
					final MenuSlot menuSlot = new MenuSlot(slot);
					
					menuSlot.setItem(itemPaneWhite);
					mapSlot.put(slot, menuSlot);
				}
				
				for (int slot = 0; slot < size; slot++) {
					if (!mapSlot.containsKey(slot)) {
						final MenuSlot menuSlot = new MenuSlot(slot);
						
						menuSlot.setItem(itemPaneBlack);
						mapSlot.put(slot, menuSlot);
					}
				}
				
				final MenuGUI menuGUI = new MenuGUI(null, id, row, title, executor, false, mapSlot);
				
				Menu.openMenu(player, menuGUI);
				return;
			}
		}
	}
	
	public final void openMenuPlayer(Player player) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		
		if (player != null) {
			final MenuExecutor executor = getMenuExecutor();
			final PlayerFishing playerFishing = playerFishingManager.getPlayerFishing(player);
			final String playerName = player.getName();
			final int playerLevel = playerFishing.getLevel();
			final double bonusEffectiveness = playerFishing.getBonusEffectiveness();
			final double bonusEndurance = playerFishing.getBonusEndurance();
			final double bonusSpeed = playerFishing.getBonusSpeed();
			final float playerExp = playerFishing.getExp();
			final float playerExpUp = playerFishing.getExpToUp();
			final boolean enableActionHome = mainConfig.isMenuEnableActionHome();
			
			final String id = "DreamFish Player";
			final String textTitle = Language.MENU_PAGE_TITLE_PLAYER.getText(player);
			final int row = 3;
			final int size = row * 9;
			final Text title = new TextLine(textTitle);
			final String prefix = placeholderManager.getPlaceholder("prefix");
			final String headerPageHome = Language.MENU_ITEM_HEADER_PAGE_HOME.getText(player);
			final String headerStats = Language.MENU_ITEM_HEADER_PLAYER_STATS.getText(player);
			final String headerProgress = Language.MENU_ITEM_HEADER_PLAYER_PROGRESS.getText(player);
			final int[] arraySlotPaneWhite = new int[] {0, 8, 9, 17, 18, 26};
			final MenuSlot menuSlotPageHome = new MenuSlot(13);
			final MenuSlot menuSlotStats = new MenuSlot(11);
			final MenuSlot menuSlotProgress = new MenuSlot(15);
			final HashMap<Integer, MenuSlot> mapSlot = new HashMap<Integer, MenuSlot>();
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			List<String> lorePageHome = Language.MENU_ITEM_LORES_PAGE_HOME.getListText(player);
			List<String> loreStats = Language.MENU_ITEM_LORES_PLAYER_STATS.getListText(player);
			List<String> loreProgress = Language.MENU_ITEM_LORES_PLAYER_PROGRESS.getListText(player);
			
			mapPlaceholder.put("player_name", playerName);
			mapPlaceholder.put("player_level", String.valueOf(playerLevel));
			mapPlaceholder.put("player_fishing_bonus_effectiveness", String.valueOf(MathUtil.roundNumber(bonusEffectiveness)));
			mapPlaceholder.put("player_fishing_bonus_endurance", String.valueOf(MathUtil.roundNumber(bonusEndurance)));
			mapPlaceholder.put("player_fishing_bonus_speed", String.valueOf(MathUtil.roundNumber(bonusSpeed)));
			mapPlaceholder.put("player_exp", String.valueOf(MathUtil.roundNumber(playerExp, 1)));
			mapPlaceholder.put("player_exp_up", String.valueOf(MathUtil.roundNumber(playerExpUp, 1)));
			
			loreStats = TextUtil.placeholder(mapPlaceholder, loreStats);
			loreProgress = TextUtil.placeholder(mapPlaceholder, loreProgress);
			
			final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
			final ItemStack itemPaneWhite = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, prefix, 1);
			final ItemStack itemPageHome = EquipmentUtil.createItem(MaterialEnum.LILY_PAD, headerPageHome, 1, lorePageHome);
			final ItemStack itemStats = EquipmentUtil.createPlayerHead(playerName, headerStats, loreStats);
			final ItemStack itemProgress = EquipmentUtil.createItem(MaterialEnum.BOOK, headerProgress, 1, loreProgress);
			
			menuSlotStats.setItem(itemStats);
			menuSlotProgress.setItem(itemProgress);
			
			menuSlotProgress.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Progress");
	
			mapSlot.put(menuSlotStats.getSlot(), menuSlotStats);
			mapSlot.put(menuSlotProgress.getSlot(), menuSlotProgress);
			
			if (enableActionHome) {
				menuSlotPageHome.setItem(itemPageHome);
				menuSlotPageHome.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Home");
				mapSlot.put(menuSlotPageHome.getSlot(), menuSlotPageHome);
			}
			
			for (int slot : arraySlotPaneWhite) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPaneWhite);
				mapSlot.put(slot, menuSlot);
			}
			
			for (int slot = 0; slot < size; slot++) {
				if (!mapSlot.containsKey(slot)) {
					final MenuSlot menuSlot = new MenuSlot(slot);
					
					menuSlot.setItem(itemPaneBlack);
					mapSlot.put(slot, menuSlot);
				}
			}
			
			final MenuGUI menuGUI = new MenuGUI(null, id, row, title, executor, false, mapSlot);
			
			Menu.openMenu(player, menuGUI);
			return;
		}
	}
	
	public final void openMenuProgress(Player player) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerFishingManager playerFishingManager = playerManager.getPlayerFishingManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		final FishManager fishManager = gameManager.getFishManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		final PlayerFishing playerFishing = playerFishingManager.getPlayerFishing(player);
		final String playerName = player.getName();
		final int playerLevel = playerFishing.getLevel();
		final int unlockedBait = playerFishing.getUnlockedBait().size();
		final int unlockedFish = playerFishing.getUnlockedFish().size();
		final int maxBait = baitManager.getBaitPropertiesIds().size();
		final int maxFish = fishManager.getFishPropertiesIds().size();
		final float playerExp = playerFishing.getExp();
		final float playerExpUp = playerFishing.getExpToUp();
		final boolean enableActionBack = mainConfig.isMenuEnableActionBack();
		
		final String id = "DreamFish Progress";
		final String textTitle = Language.MENU_PAGE_TITLE_PROGRESS.getText(player);
		final int row = 5;
		final int size = row * 9;
		final Text title = new TextLine(textTitle);
		final String prefix = placeholderManager.getPlaceholder("prefix");
		final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
		final String headerProgressStats = Language.MENU_ITEM_HEADER_PROGRESS_STATS.getText(player);
		final String headerProgressBait = Language.MENU_ITEM_HEADER_PROGRESS_BAIT.getText(player);
		final String headerProgressFish = Language.MENU_ITEM_HEADER_PROGRESS_FISH.getText(player);
		final int[] arraySlotPaneWhite = new int[] {0, 8, 9, 17, 18, 26, 27, 35, 36, 44};
		final int[] arraySlotPaneRed = new int[] {1, 2, 6, 7, 37, 38, 42, 43};
		final int[] arraySlotPageBack = new int[] {39, 40, 41};
		final int[] arraySlotProgressStats = new int[] {3, 4, 5};
		final MenuSlot menuSlotProgressBait = new MenuSlot(20);
		final MenuSlot menuSlotProgressFish = new MenuSlot(24);
		final HashMap<Integer, MenuSlot> mapSlot = new HashMap<Integer, MenuSlot>();
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		
		List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
		List<String> loreProgressStats = Language.MENU_ITEM_LORES_PROGRESS_STATS.getListText(player);
		List<String> loreProgressBait = Language.MENU_ITEM_LORES_PROGRESS_BAIT.getListText(player);
		List<String> loreProgressFish = Language.MENU_ITEM_LORES_PROGRESS_FISH.getListText(player);
		
		mapPlaceholder.put("player_name", playerName);
		mapPlaceholder.put("player_level", String.valueOf(playerLevel));
		mapPlaceholder.put("player_exp", String.valueOf(MathUtil.roundNumber(playerExp, 1)));
		mapPlaceholder.put("player_exp_up", String.valueOf(MathUtil.roundNumber(playerExpUp, 1)));
		mapPlaceholder.put("player_unlocked_bait", String.valueOf(unlockedBait));
		mapPlaceholder.put("player_unlocked_fish", String.valueOf(unlockedFish));
		mapPlaceholder.put("max_bait", String.valueOf(maxBait));
		mapPlaceholder.put("max_fish", String.valueOf(maxFish));
		
		loreProgressStats = TextUtil.placeholder(mapPlaceholder, loreProgressStats);
		loreProgressBait = TextUtil.placeholder(mapPlaceholder, loreProgressBait);
		loreProgressFish = TextUtil.placeholder(mapPlaceholder, loreProgressFish);

		final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPaneWhite = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPaneRed = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
		final ItemStack itemProgressStats = EquipmentUtil.createItem(MaterialEnum.SIGN, headerProgressStats, 1, loreProgressStats);
		final ItemStack itemProgressBait = EquipmentUtil.createItem(MaterialEnum.WHEAT_SEEDS, headerProgressBait, 1, loreProgressBait);
		final ItemStack itemProgressFish = EquipmentUtil.createItem(MaterialEnum.COD, headerProgressFish, 1, loreProgressFish);
		
		menuSlotProgressBait.setItem(itemProgressBait);
		menuSlotProgressFish.setItem(itemProgressFish);
		
		menuSlotProgressBait.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Progress_Bait");
		menuSlotProgressFish.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Progress_Fish");
		
		mapSlot.put(menuSlotProgressBait.getSlot(), menuSlotProgressBait);
		mapSlot.put(menuSlotProgressFish.getSlot(), menuSlotProgressFish);
		
		if (enableActionBack) {
			for (int slot : arraySlotPageBack) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageBack);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Player");
				mapSlot.put(slot, menuSlot);
			}
		}
		
		for (int slot : arraySlotProgressStats) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemProgressStats);
			mapSlot.put(slot, menuSlot);
		} 
		
		for (int slot : arraySlotPaneWhite) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneWhite);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot : arraySlotPaneRed) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneRed);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot = 0; slot < size; slot++) {
			if (!mapSlot.containsKey(slot)) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPaneBlack);
				mapSlot.put(slot, menuSlot);
			}
		}
		
		final MenuGUI menuGUI = new MenuGUI(null, id, row, title, executor, false, mapSlot);
		
		Menu.openMenu(player, menuGUI);
		return;
	}
	
	public final void openMenuProgressBait(Player player) {
		openMenuProgressBait(player, 1);
	}
	
	public final void openMenuProgressBait(Player player, int page) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		final String id = "DreamFish Progress_Fish";
		final String currency = mainConfig.getUtilityCurrency();
		final boolean enableActionBack = mainConfig.isMenuEnableActionBack();
		final Collection<BaitProperties> allBaitProperties = baitManager.getAllBaitProperties();
		
		final int row = 6;
		final int size = allBaitProperties.size();
		final int slots = 28;
		final int maxPage = Math.max(1, size % slots > 0 ? (size/slots) + 1 : size/slots);
		final String prefix = placeholderManager.getPlaceholder("prefix");
		final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
		final String headerPageNext = Language.MENU_ITEM_HEADER_PAGE_NEXT.getText(player);
		final String headerPagePrevious = Language.MENU_ITEM_HEADER_PAGE_PREVIOUS.getText(player);
		final int[] arraySlotPane = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
		final int[] arraySlotBack = new int[] {48, 49, 50};
		final int[] arraySlotNext = new int[] {26, 35};
		final int[] arraySlotPrevious = new int[] {18, 27};
		final int[] arraySlotComponent = new int[] {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
		
		final HashMap<String, Integer> mapFish = new HashMap<String, Integer>();
		final HashMap<Integer, MenuSlot> mapSlot = new HashMap<Integer, MenuSlot>();
		final HashMap<String, List<BaitProperties>> mapListBait = new HashMap<String, List<BaitProperties>>();
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		final List<BaitProperties> listBaitUnlocked = new ArrayList<BaitProperties>();
		final List<BaitProperties> listBaitLocked = new ArrayList<BaitProperties>();
		
		for (BaitProperties baitProperties : allBaitProperties) {
			final ItemRequirement requirement = baitProperties.getRequirement();
			final ItemStack item = baitProperties.getItem();
			final String serialize = "Type: " + item.getType() + ", Data: "+ EquipmentUtil.getData(item);
			final String fish = baitProperties.getId();
			final int level = requirement.getLevel();
			
			mapFish.put(fish, level);
			
			if (mapListBait.containsKey(serialize)) {
				mapListBait.get(serialize).add(baitProperties);
			} else {
				final List<BaitProperties> listBait = new ArrayList<BaitProperties>();
				
				listBait.add(baitProperties);
				mapListBait.put(serialize, listBait);
			}
		}
		
		for (List<BaitProperties> listBait : mapListBait.values()) {
			for (BaitProperties baitProperties : listBait) {
				final ItemRequirement requirement = baitProperties.getRequirement();
				
				if (requirement.isAllowed(player)) {
					listBaitUnlocked.add(baitProperties);
				}
			}
		}
		
		for (String bait : SortUtil.sortByValue(mapFish).keySet()) {
			final BaitProperties baitProperties = baitManager.getBaitProperties(bait);
			final ItemRequirement requirement = baitProperties.getRequirement();
			
			if (!requirement.isAllowed(player)) {
				listBaitLocked.add(baitProperties);
			}
		}
		
		page = MathUtil.limitInteger(page, 1, maxPage);
		
		String textTitle = Language.MENU_PAGE_TITLE_PROGRESS_BAIT.getText(player);
		
		List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
		List<String> lorePageNext = Language.MENU_ITEM_LORES_PAGE_NEXT.getListText(player);
		List<String> lorePagePrevious = Language.MENU_ITEM_LORES_PAGE_PREVIOUS.getListText(player);
		
		mapPlaceholder.put("page", String.valueOf(page));
		mapPlaceholder.put("max_page", String.valueOf(maxPage));
		
		textTitle = TextUtil.placeholder(mapPlaceholder, textTitle);
		
		final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
		final ItemStack itemPageNext = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPageNext, 1, lorePageNext);
		final ItemStack itemPagePrevious = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPagePrevious, 1, lorePagePrevious);
		
		for (int slot : arraySlotPane) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneBlack);
			mapSlot.put(slot, menuSlot);
		}
		
		if (enableActionBack) {
			for (int slot : arraySlotBack) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageBack);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Progress");
				mapSlot.put(slot, menuSlot);
			}
		}
		
		if (page < maxPage) {
			for (int slot : arraySlotNext) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageNext);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Progress_Bait " + (page+1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		if (page > 1) {
			for (int slot : arraySlotPrevious) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPagePrevious);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Progress_Bait " + (page-1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		for (int index = 0; index < arraySlotComponent.length; index++) {
			final int slot = arraySlotComponent[index];
			final int indexRegister = index + (slots*(page-1));
			
			if (indexRegister < size) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				if (indexRegister < listBaitUnlocked.size()) {
					final BaitProperties baitProperties = listBaitUnlocked.get(indexRegister);
					final ItemStack item = baitProperties.getItem().clone();
					final MaterialEnum materialEnum = MaterialEnum.getMaterialEnum(item);
					final String bait = baitProperties.getId();
					final double price = baitProperties.getPrice();
					
					String headerUnlocked = Language.MENU_ITEM_HEADER_PROGRESS_BAIT_UNLOCKED.getText(player);
					List<String> loreUnlocked = Language.MENU_ITEM_LORES_PROGRESS_BAIT_UNLOCKED.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("bait_id", bait);
					mapPlaceholder.put("price", String.valueOf(price));
					mapPlaceholder.put("symbol_currency", currency);
					
					headerUnlocked = TextUtil.placeholder(mapPlaceholder, headerUnlocked);
					loreUnlocked = TextUtil.placeholder(mapPlaceholder, loreUnlocked);
					
					final ItemStack itemBait = EquipmentUtil.createItem(materialEnum, headerUnlocked, 1, loreUnlocked);
					
					menuSlot.setItem(itemBait);
					mapSlot.put(slot, menuSlot);
				} else {
					final BaitProperties baitProperties = listBaitLocked.get(indexRegister - listBaitUnlocked.size());
					final ItemRequirement requirement = baitProperties.getRequirement();
					final String bait = baitProperties.getId();
					final String requirementPermission = requirement.getPermission();
					final int requirementLevel = requirement.getLevel();
					
					String headerLocked = Language.MENU_ITEM_HEADER_PROGRESS_BAIT_LOCKED.getText(player);
					List<String> loreLocked = Language.MENU_ITEM_LORES_PROGRESS_BAIT_LOCKED.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("bait_id", bait);
					mapPlaceholder.put("requirement_permission", requirementPermission != null ? requirementPermission : "null");
					mapPlaceholder.put("requirement_level", String.valueOf(requirementLevel));
					
					headerLocked = TextUtil.placeholder(mapPlaceholder, headerLocked);
					loreLocked = TextUtil.placeholder(mapPlaceholder, loreLocked);
					
					final ItemStack itemBait = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerLocked, 1, loreLocked);
					
					menuSlot.setItem(itemBait);
					mapSlot.put(slot, menuSlot);
				}
			} else {
				break;
			}
		}
		
		final Text title = new TextLine(textTitle);
		final MenuGUI menuGUI = new MenuGUI(player, id, row, title, executor, false, mapSlot);
		
		Menu.openMenu(player, menuGUI);
		return;
	}
	
	public final void openMenuProgressFish(Player player) {
		openMenuProgressFish(player, 1);
	}
	
	public final void openMenuProgressFish(Player player, int page) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final FishManager fishManager = gameManager.getFishManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		final String id = "DreamFish Progress_Fish";
		final String currency = mainConfig.getUtilityCurrency();
		final boolean enableActionBack = mainConfig.isMenuEnableActionBack();
		final Collection<FishProperties> allFishProperties = fishManager.getAllFishProperties();
		
		final int row = 6;
		final int size = allFishProperties.size();
		final int slots = 28;
		final int maxPage = Math.max(1, size % slots > 0 ? (size/slots) + 1 : size/slots);
		final String prefix = placeholderManager.getPlaceholder("prefix");
		final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
		final String headerPageNext = Language.MENU_ITEM_HEADER_PAGE_NEXT.getText(player);
		final String headerPagePrevious = Language.MENU_ITEM_HEADER_PAGE_PREVIOUS.getText(player);
		final int[] arraySlotPane = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
		final int[] arraySlotBack = new int[] {48, 49, 50};
		final int[] arraySlotNext = new int[] {26, 35};
		final int[] arraySlotPrevious = new int[] {18, 27};
		final int[] arraySlotComponent = new int[] {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
		
		final HashMap<String, Integer> mapFish = new HashMap<String, Integer>();
		final HashMap<Integer, MenuSlot> mapSlot = new HashMap<Integer, MenuSlot>();
		final HashMap<String, List<FishProperties>> mapListFish = new HashMap<String, List<FishProperties>>();
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		final List<FishProperties> listFishUnlocked = new ArrayList<FishProperties>();
		final List<FishProperties> listFishLocked = new ArrayList<FishProperties>();
		
		for (FishProperties fishProperties : allFishProperties) {
			final ItemRequirement requirement = fishProperties.getRequirement();
			final ItemStack item = fishProperties.getItem();
			final String serialize = "Type: " + item.getType() + ", Data: "+ EquipmentUtil.getData(item);
			final String fish = fishProperties.getId();
			final int level = requirement.getLevel();
			
			mapFish.put(fish, level);
			
			if (mapListFish.containsKey(serialize)) {
				mapListFish.get(serialize).add(fishProperties);
			} else {
				final List<FishProperties> listFish = new ArrayList<FishProperties>();
				
				listFish.add(fishProperties);
				mapListFish.put(serialize, listFish);
			}
		}
		
		for (List<FishProperties> listFish : mapListFish.values()) {
			for (FishProperties fishProperties : listFish) {
				final ItemRequirement requirement = fishProperties.getRequirement();
				
				if (requirement.isAllowed(player)) {
					listFishUnlocked.add(fishProperties);
				}
			}
		}
		
		for (String fish : SortUtil.sortByValue(mapFish).keySet()) {
			final FishProperties fishProperties = fishManager.getFishProperties(fish);
			final ItemRequirement requirement = fishProperties.getRequirement();
			
			if (!requirement.isAllowed(player)) {
				listFishLocked.add(fishProperties);
			}
		}
		
		page = MathUtil.limitInteger(page, 1, maxPage);
		
		String textTitle = Language.MENU_PAGE_TITLE_PROGRESS_FISH.getText(player);
		
		List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
		List<String> lorePageNext = Language.MENU_ITEM_LORES_PAGE_NEXT.getListText(player);
		List<String> lorePagePrevious = Language.MENU_ITEM_LORES_PAGE_PREVIOUS.getListText(player);
		
		mapPlaceholder.put("page", String.valueOf(page));
		mapPlaceholder.put("max_page", String.valueOf(maxPage));
		
		textTitle = TextUtil.placeholder(mapPlaceholder, textTitle);
		
		final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, headerPageBack, 1, lorePageBack);
		final ItemStack itemPageNext = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPageNext, 1, lorePageNext);
		final ItemStack itemPagePrevious = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPagePrevious, 1, lorePagePrevious);
		
		for (int slot : arraySlotPane) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneBlack);
			mapSlot.put(slot, menuSlot);
		}
		
		if (enableActionBack) {
			for (int slot : arraySlotBack) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageBack);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Progress");
				mapSlot.put(slot, menuSlot);
			}
		}
			
		if (page < maxPage) {
			for (int slot : arraySlotNext) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageNext);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Progress_Fish " + (page+1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		if (page > 1) {
			for (int slot : arraySlotPrevious) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPagePrevious);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Progress_Fish " + (page-1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		for (int index = 0; index < arraySlotComponent.length; index++) {
			final int slot = arraySlotComponent[index];
			final int indexRegister = index + (slots*(page-1));
			
			if (indexRegister < size) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				if (indexRegister < listFishUnlocked.size()) {
					final FishProperties fishProperties = listFishUnlocked.get(indexRegister);
					final ItemStack item = fishProperties.getItem().clone();
					final MaterialEnum materialEnum = MaterialEnum.getMaterialEnum(item);
					final EntityType entityType = fishProperties.getType();
					final String fish = fishProperties.getId();
					final String type = TextUtil.toTitleCase(entityType.toString().replace("_", " "));
					final String divider = "\n";
					final boolean asBait = fishProperties.asBait();
					final boolean isInvisibile = fishProperties.isInvisible();
					final double resistance = fishProperties.getResistance();
					final double power = fishProperties.getPower();
					final double maxPower = fishProperties.getMaxPower();
					final double maxSpeed = fishProperties.getMaxSpeed();
					final double maxDive = fishProperties.getMaxDive();
					final double length = fishProperties.getAverageLength();
					final double weight = fishProperties.getAverageWeight();
					final double price = fishProperties.getPrice();
					final List<BaitProperties> allConnectBaitProperties = fishProperties.getAllConnectBaitProperties();
					final List<String> allRegion = fishProperties.getRegions();
					final List<Biome> allBiome = fishProperties.getBiomes();
					final List<String> lorePossibility = new ArrayList<String>();
					final List<String> loreRegion = new ArrayList<String>();
					final List<String> loreBiome = new ArrayList<String>();
					
					String header = Language.MENU_ITEM_HEADER_PROGRESS_FISH_UNLOCKED.getText(player);
					List<String> loreDescription = Language.MENU_ITEM_LORES_PROGRESS_FISH_UNLOCKED.getListText(player);
					
					if (!allConnectBaitProperties.isEmpty()) {
						final List<String> possibilityHeader = Language.MENU_ITEM_FORMAT_PROGRESS_FISH_POSSIBILITY_HEADER.getListText(player);
						
						lorePossibility.addAll(possibilityHeader);
						
						for (BaitProperties baitProperties : allConnectBaitProperties) {
							final String bait = baitProperties.getId();
							final BaitFishing baitFishing = baitProperties.getBaitFishing();
							final double possibilityTotal = baitFishing.getTotalPossibility();
							final double possibility = baitFishing.getFishPossibility(fish);
							final double possibilityRate = (possibility / possibilityTotal) * 100;
							final double chance = baitFishing.getFishChance(fish);
							
							List<String> formatPossibility = Language.MENU_ITEM_FORMAT_PROGRESS_FISH_POSSIBILITY_LIST.getListText(player);
							
							mapPlaceholder.clear();
							mapPlaceholder.put("bait", bait);
							mapPlaceholder.put("possibility", String.valueOf(MathUtil.roundNumber(possibilityRate)));
							mapPlaceholder.put("chance", String.valueOf(MathUtil.roundNumber(chance)));
							
							formatPossibility = TextUtil.placeholder(mapPlaceholder, formatPossibility);
							
							lorePossibility.addAll(formatPossibility);
						}
					} else {
						final List<String> possibilityEmpty = Language.MENU_ITEM_FORMAT_PROGRESS_FISH_POSSIBILITY_EMPTY.getListText(player);
						
						lorePossibility.addAll(possibilityEmpty);
					}
					
					if (!allRegion.isEmpty()) {
						final List<String> regionHeader = Language.MENU_ITEM_FORMAT_PROGRESS_FISH_REGION_HEADER.getListText(player); 
						
						loreRegion.addAll(regionHeader);
						
						for (String region : allRegion) {
							
							List<String> formatRegion = Language.MENU_ITEM_FORMAT_PROGRESS_FISH_REGION_LIST.getListText(player);
							
							mapPlaceholder.clear();
							mapPlaceholder.put("region", region);
							
							formatRegion = TextUtil.placeholder(mapPlaceholder, formatRegion);
							
							loreRegion.addAll(formatRegion);
						}
					} else {
						final List<String> regionAll = Language.MENU_ITEM_FORMAT_PROGRESS_FISH_REGION_ALL.getListText(player);
						
						loreRegion.addAll(regionAll);
					}
					
					if (!allBiome.isEmpty()) {
						final List<String> biomeHeader = Language.MENU_ITEM_FORMAT_PROGRESS_FISH_BIOME_HEADER.getListText(player);
						
						loreBiome.addAll(biomeHeader);
						
						for (Biome biome : allBiome) {
							List<String> formatBiome = Language.MENU_ITEM_FORMAT_PROGRESS_FISH_BIOME_LIST.getListText(player);
							
							mapPlaceholder.clear();
							mapPlaceholder.put("biome", TextUtil.toTitleCase(biome.toString().replace("_", " ")));
							
							formatBiome = TextUtil.placeholder(mapPlaceholder, formatBiome);
							
							loreBiome.addAll(formatBiome);
						}
					} else {
						final List<String> biomeAll = Language.MENU_ITEM_FORMAT_PROGRESS_FISH_BIOME_ALL.getListText(player);
						
						loreBiome.addAll(biomeAll);
					}
					
					final String mergePossibility = TextUtil.convertListToString(lorePossibility, divider);
					final String mergeRegion = TextUtil.convertListToString(loreRegion, divider);
					final String mergeBiome = TextUtil.convertListToString(loreBiome, divider);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("fish_id", fish);
					mapPlaceholder.put("type", type);
					mapPlaceholder.put("asbait", String.valueOf(asBait));
					mapPlaceholder.put("invisible", String.valueOf(isInvisibile));
					mapPlaceholder.put("resistance", String.valueOf(resistance));
					mapPlaceholder.put("power", String.valueOf(power));
					mapPlaceholder.put("max_power", String.valueOf(maxPower));
					mapPlaceholder.put("max_speed", String.valueOf(maxSpeed));
					mapPlaceholder.put("max_dive", String.valueOf(maxDive));
					mapPlaceholder.put("length", String.valueOf(length));
					mapPlaceholder.put("weight", String.valueOf(weight));
					mapPlaceholder.put("price", String.valueOf(price));
					mapPlaceholder.put("fish_possibility", mergePossibility);
					mapPlaceholder.put("fish_region", mergeRegion);
					mapPlaceholder.put("fish_biome", mergeBiome);
					mapPlaceholder.put("symbol_currency", currency);
					
					header = TextUtil.placeholder(mapPlaceholder, header);
					loreDescription = TextUtil.placeholder(mapPlaceholder, loreDescription);
					
					final ItemStack itemFishInformation = EquipmentUtil.createItem(materialEnum, header, 1, loreDescription);
					
					menuSlot.setItem(itemFishInformation);
					mapSlot.put(slot, menuSlot);
				} else {
					final FishProperties fishProperties = listFishLocked.get(indexRegister - listFishUnlocked.size());
					final ItemRequirement requirement = fishProperties.getRequirement();
					final String fish = fishProperties.getId();
					final String requirementPermission = requirement.getPermission();
					final int requirementLevel = requirement.getLevel();;
					
					String headerFertilizer = Language.MENU_ITEM_HEADER_PROGRESS_FISH_LOCKED.getText(player);
					List<String> loreFertilizer = Language.MENU_ITEM_LORES_PROGRESS_FISH_LOCKED.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("fish_id", fish);
					mapPlaceholder.put("requirement_permission", requirementPermission != null ? requirementPermission : "null");
					mapPlaceholder.put("requirement_level", String.valueOf(requirementLevel));
					
					headerFertilizer = TextUtil.placeholder(mapPlaceholder, headerFertilizer);
					loreFertilizer = TextUtil.placeholder(mapPlaceholder, loreFertilizer);
					
					final ItemStack itemFertilizer = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerFertilizer, 1, loreFertilizer);
					
					menuSlot.setItem(itemFertilizer);
					mapSlot.put(slot, menuSlot);
				}
			} else {
				break;
			}
		}
		
		final Text title = new TextLine(textTitle);
		final MenuGUI menuGUI = new MenuGUI(player, id, row, title, executor, false, mapSlot);
		
		Menu.openMenu(player, menuGUI);
		return;
	}
	
	public final void openMenuShop(Player player) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		final boolean enableActionBack = mainConfig.isMenuEnableActionBack();
		final boolean enablePageBuyBait = mainConfig.isMenuEnablePageBuyBait();
		final boolean enablePageSellBait = mainConfig.isMenuEnablePageSellBait();
		final boolean enablePageSellFish = mainConfig.isMenuEnablePageSellFish();
		
		final String id = "DreamFish Shop";
		final String textTitle = Language.MENU_PAGE_TITLE_SHOP.getText(player);
		final int row = 5;
		final int size = row * 9;
		final Text title = new TextLine(textTitle);
		final String prefix = placeholderManager.getPlaceholder("prefix");
		final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
		final String headerInformation = Language.MENU_ITEM_HEADER_SHOP_INFORMATION.getText(player);
		final String headerBaitBuy = Language.MENU_ITEM_HEADER_SHOP_BAIT_BUY.getText(player);
		final String headerBaitSell = Language.MENU_ITEM_HEADER_SHOP_BAIT_SELL.getText(player);
		final String headerFishSell = Language.MENU_ITEM_HEADER_SHOP_FISH_SELL.getText(player);
		final int[] arraySlotPaneWhite = new int[] {0, 8, 9, 17, 18, 26, 27, 35, 36, 44};
		final int[] arraySlotPaneRed = new int[] {1, 2, 6, 7, 37, 38, 42, 43};
		final int[] arraySlotPageBack = new int[] {39, 40, 41};
		final int[] arraySlotInformation = new int[] {3, 4, 5};
		final MenuSlot menuSlotBaitBuy = new MenuSlot(20);
		final MenuSlot menuSlotBaitSell = new MenuSlot(22);
		final MenuSlot menuSlotFishSell = new MenuSlot(24);
		final HashMap<Integer, MenuSlot> mapSlot = new HashMap<Integer, MenuSlot>();
		
		List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
		List<String> loreInformation = Language.MENU_ITEM_LORES_SHOP_INFORMATION.getListText(player);
		List<String> loreBaitBuy = enablePageBuyBait ? Language.MENU_ITEM_LORES_SHOP_BAIT_BUY.getListText(player) : Language.MENU_ITEM_LORES_MENU_DISABLED.getListText(player);
		List<String> loreBaitSell = enablePageSellBait ? Language.MENU_ITEM_LORES_SHOP_BAIT_SELL.getListText(player) : Language.MENU_ITEM_LORES_MENU_DISABLED.getListText(player);
		List<String> loreFishSell = enablePageSellFish ? Language.MENU_ITEM_LORES_SHOP_FISH_SELL.getListText(player) : Language.MENU_ITEM_LORES_MENU_DISABLED.getListText(player);

		final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPaneWhite = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPaneRed = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
		final ItemStack itemInformation = EquipmentUtil.createItem(MaterialEnum.SIGN, headerInformation, 1, loreInformation);
		final ItemStack itemBaitBuy = EquipmentUtil.createItem(MaterialEnum.WHEAT_SEEDS, headerBaitBuy, 1, loreBaitBuy);
		final ItemStack itemBaitSell = EquipmentUtil.createItem(MaterialEnum.PUMPKIN_SEEDS, headerBaitSell, 1, loreBaitSell);
		final ItemStack itemFishSell = EquipmentUtil.createItem(MaterialEnum.COD, headerFishSell, 1, loreFishSell);
		
		menuSlotBaitBuy.setItem(itemBaitBuy);
		menuSlotBaitSell.setItem(itemBaitSell);
		menuSlotFishSell.setItem(itemFishSell);
		
		if (enablePageBuyBait) {
			menuSlotBaitBuy.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop_Bait_Buy");
		}
		
		if (enablePageSellBait) {
			menuSlotBaitSell.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop_Bait_Sell");
		}
		
		if (enablePageSellFish) {
			menuSlotFishSell.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop_Fish_Sell");
		}
		
		mapSlot.put(menuSlotBaitBuy.getSlot(), menuSlotBaitBuy);
		mapSlot.put(menuSlotBaitSell.getSlot(), menuSlotBaitSell);
		mapSlot.put(menuSlotFishSell.getSlot(), menuSlotFishSell);
		
		if (enableActionBack) {
			for (int slot : arraySlotPageBack) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageBack);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Home");
				mapSlot.put(slot, menuSlot);
			}
		}
		
		for (int slot : arraySlotInformation) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemInformation);
			mapSlot.put(slot, menuSlot);
		} 
		
		for (int slot : arraySlotPaneWhite) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneWhite);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot : arraySlotPaneRed) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneRed);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot = 0; slot < size; slot++) {
			if (!mapSlot.containsKey(slot)) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPaneBlack);
				mapSlot.put(slot, menuSlot);
			}
		}
		
		final MenuGUI menuGUI = new MenuGUI(null, id, row, title, executor, false, mapSlot);
		
		Menu.openMenu(player, menuGUI);
		return;
	}
	
	public final void openMenuBaitBuy(Player player) {
		openMenuBaitBuy(player, 1);
	}
	
	public final void openMenuBaitBuy(Player player, int page) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		
		if (player != null) {
			final boolean enablePageBuyBait = mainConfig.isMenuEnablePageBuyBait();
			final boolean enableActionBack = mainConfig.isMenuEnableActionBack();
			
			if (enablePageBuyBait) {
				final String permission = mainConfig.getMenuPermissionBuyBait();
				
				if (!SenderUtil.hasPermission(player, permission)) {
					final MessageBuild message = Language.PERMISSION_LACK.getMessage(player);
					
					message.sendMessage(player, "permission", permission);
					SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final String id = "DreamFish Shop_Bait_Buy";
					final String currency = mainConfig.getUtilityCurrency();
					final Collection<BaitProperties> allBaitProperties = baitManager.getAllBaitProperties();
					final HashMap<String, Integer> mapBait = new HashMap<String, Integer>();
					final HashMap<String, List<BaitProperties>> mapListBait = new HashMap<String, List<BaitProperties>>();
					final List<BaitProperties> listBaitUnlocked = new ArrayList<BaitProperties>();
					final List<BaitProperties> listBaitLocked = new ArrayList<BaitProperties>();
					
					for (BaitProperties baitProperties : allBaitProperties) {
						if (baitProperties.isBuyable()) {
							final ItemRequirement requirement = baitProperties.getRequirement();
							final ItemStack item = baitProperties.getItem();
							final String serialize = "Type: " + item.getType() + ", Data: "+ EquipmentUtil.getData(item);
							final String bait = baitProperties.getId();
							final int level = requirement.getLevel();
							
							mapBait.put(bait, level);
							
							if (mapListBait.containsKey(serialize)) {
								mapListBait.get(serialize).add(baitProperties);
							} else {
								final List<BaitProperties> listBait = new ArrayList<BaitProperties>();
								
								listBait.add(baitProperties);
								mapListBait.put(serialize, listBait);
							}
						}
					}
					
					for (List<BaitProperties> listBait : mapListBait.values()) {
						for (BaitProperties baitProperties : listBait) {
							final ItemRequirement requirement = baitProperties.getRequirement();
							
							if (requirement.isAllowed(player)) {
								listBaitUnlocked.add(baitProperties);
							}
						}
					}
					
					for (String bait : SortUtil.sortByValue(mapBait).keySet()) {
						final BaitProperties baitProperties = baitManager.getBaitProperties(bait);
						final ItemRequirement requirement = baitProperties.getRequirement();
						
						if (!requirement.isAllowed(player)) {
							listBaitLocked.add(baitProperties);
						}
					}
					
					final int row = 6;
					final int size = listBaitUnlocked.size() + listBaitLocked.size();
					final int slots = 28;
					final int maxPage = Math.max(1, size % slots > 0 ? (size/slots) + 1 : size/slots);
					final String prefix = placeholderManager.getPlaceholder("prefix");
					final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
					final String headerPageNext = Language.MENU_ITEM_HEADER_PAGE_NEXT.getText(player);
					final String headerPagePrevious = Language.MENU_ITEM_HEADER_PAGE_PREVIOUS.getText(player);
					final int[] arraySlotPane = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 51, 52, 53};
					final int[] arraySlotBack = new int[] {48, 49, 50};
					final int[] arraySlotNext = new int[] {26, 35};
					final int[] arraySlotPrevious = new int[] {18, 27};
					final int[] arraySlotComponent = new int[] {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
					final HashMap<Integer, MenuSlot> mapSlot = new HashMap<Integer, MenuSlot>();
					final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
					
					page = MathUtil.limitInteger(page, 1, maxPage);
					
					String textTitle = Language.MENU_PAGE_TITLE_SHOP_BAIT_BUY.getText(player);
					
					List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
					List<String> lorePageNext = Language.MENU_ITEM_LORES_PAGE_NEXT.getListText(player);
					List<String> lorePagePrevious = Language.MENU_ITEM_LORES_PAGE_PREVIOUS.getListText(player);
					
					mapPlaceholder.put("page", String.valueOf(page));
					mapPlaceholder.put("max_page", String.valueOf(maxPage));
					
					textTitle = TextUtil.placeholder(mapPlaceholder, textTitle);
					
					final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
					final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
					final ItemStack itemPageNext = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPageNext, 1, lorePageNext);
					final ItemStack itemPagePrevious = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPagePrevious, 1, lorePagePrevious);
					
					for (int slot : arraySlotPane) {
						final MenuSlot menuSlot = new MenuSlot(slot);
						
						menuSlot.setItem(itemPaneBlack);
						mapSlot.put(slot, menuSlot);
					}
					
					if (enableActionBack) {
						for (int slot : arraySlotBack) {
							final MenuSlot menuSlot = new MenuSlot(slot);
							
							menuSlot.setItem(itemPageBack);
							menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop");
							mapSlot.put(slot, menuSlot);
						}
					}
						
					if (page < maxPage) {
						for (int slot : arraySlotNext) {
							final MenuSlot menuSlot = new MenuSlot(slot);
							
							menuSlot.setItem(itemPageNext);
							menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop_Bait_Buy " + (page+1));
							mapSlot.put(slot, menuSlot);
						} 
					}
					
					if (page > 1) {
						for (int slot : arraySlotPrevious) {
							final MenuSlot menuSlot = new MenuSlot(slot);
							
							menuSlot.setItem(itemPagePrevious);
							menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop_Bait_Buy " + (page-1));
							mapSlot.put(slot, menuSlot);
						} 
					}
					
					for (int index = 0; index < arraySlotComponent.length; index++) {
						final int slot = arraySlotComponent[index];
						final int indexRegister = index + (slots*(page-1));
						
						if (indexRegister < size) {
							final MenuSlot menuSlot = new MenuSlot(slot);
							
							if (indexRegister < listBaitUnlocked.size()) {
								final BaitProperties baitProperties = listBaitUnlocked.get(indexRegister);
								final String bait = baitProperties.getId();
								final ItemStack item = baitProperties.getItem().clone();
								final double price = baitProperties.getPrice();
								final List<String> lores = EquipmentUtil.getLores(item);
								
								List<String> loreComponent = Language.MENU_ITEM_FORMAT_BAIT_BUY_COMPONENT.getListText(player);
								
								mapPlaceholder.clear();
								mapPlaceholder.put("bait", bait);
								mapPlaceholder.put("price", String.valueOf(price));
								mapPlaceholder.put("symbol_currency", currency);
								
								loreComponent = TextUtil.placeholder(mapPlaceholder, loreComponent);
								
								lores.addAll(loreComponent);
								EquipmentUtil.setLores(item, lores);;
								
								menuSlot.setItem(item);
								menuSlot.setActionArguments(ActionType.LEFT_CLICK, "DreamFish Action Bait Buy " + bait + " 1");
								menuSlot.setActionArguments(ActionType.RIGHT_CLICK, "DreamFish Action Bait Buy " + bait + " 64");
								mapSlot.put(slot, menuSlot);
							} else {
								final BaitProperties baitProperties = listBaitLocked.get(indexRegister - listBaitUnlocked.size());
								final ItemStack item = baitProperties.getItem().clone();
								final String itemName = EquipmentUtil.getDisplayName(item);
								
								String headerLocked = Language.MENU_ITEM_HEADER_SHOP_LOCKED.getText(player);
								List<String> loreLocked = Language.MENU_ITEM_LORES_SHOP_LOCKED.getListText(player);
								
								mapPlaceholder.clear();
								mapPlaceholder.put("item_name", itemName);
								
								headerLocked = TextUtil.placeholder(mapPlaceholder, headerLocked);
								loreLocked = TextUtil.placeholder(mapPlaceholder, loreLocked);
								
								EquipmentUtil.setMaterial(item, MaterialEnum.RED_STAINED_GLASS_PANE);
								EquipmentUtil.setDisplayName(item, headerLocked);
								EquipmentUtil.setLores(item, loreLocked);
								
								menuSlot.setItem(item);
								mapSlot.put(slot, menuSlot);
							}
						} else {
							break;
						}
					}
					
					final Text title = new TextLine(textTitle);
					final MenuGUI menuGUI = new MenuGUI(player, id, row, title, executor, false, mapSlot);
					
					Menu.openMenu(player, menuGUI);
					return;
				}
			}
		}
	}
	
	public final void openMenuBaitSell(Player player) {
		openMenuBaitSell(player, 1);
	}
	
	public final void openMenuBaitSell(Player player, int page) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final BaitManager baitManager = gameManager.getBaitManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		
		if (player != null) {
			final boolean enablePageSellBait = mainConfig.isMenuEnablePageSellBait();
			final boolean enableActionBack = mainConfig.isMenuEnableActionBack();
			
			if (enablePageSellBait) {
				final String permission = mainConfig.getMenuPermissionSellBait();
				
				if (!SenderUtil.hasPermission(player, permission)) {
					final MessageBuild message = Language.PERMISSION_LACK.getMessage(player);
					
					message.sendMessage(player, "permission", permission);
					SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final String id = "DreamFish Shop_Bait_Sell";
					final String currency = mainConfig.getUtilityCurrency();
					final Collection<BaitProperties> allBaitProperties = baitManager.getAllBaitProperties();
					final List<BaitProperties> allowedBait = new ArrayList<BaitProperties>();
					final HashMap<String, List<BaitProperties>> mapListBait = new HashMap<String, List<BaitProperties>>();
					
					for (BaitProperties baitProperties : allBaitProperties) {
						final ItemStack item = baitProperties.getItem();
						final String serialize = "Type: " + item.getType() + ", Data: "+ EquipmentUtil.getData(item);
						
						if (mapListBait.containsKey(serialize)) {
							mapListBait.get(serialize).add(baitProperties);
						} else {
							final List<BaitProperties> listBait = new ArrayList<BaitProperties>();
							
							listBait.add(baitProperties);
							mapListBait.put(serialize, listBait);
						}
					}
					
					for (List<BaitProperties> listBait : mapListBait.values()) {
						for (BaitProperties baitProperties : listBait) {
							allowedBait.add(baitProperties);
						}
					}
					
					final int row = 6;
					final int size = allowedBait.size();
					final int slots = 28;
					final int maxPage = Math.max(1, size % slots > 0 ? (size/slots) + 1 : size/slots);
					final String prefix = placeholderManager.getPlaceholder("prefix");
					final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
					final String headerPageNext = Language.MENU_ITEM_HEADER_PAGE_NEXT.getText(player);
					final String headerPagePrevious = Language.MENU_ITEM_HEADER_PAGE_PREVIOUS.getText(player);
					final int[] arraySlotPane = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
					final int[] arraySlotBack = new int[] {48, 49, 50};
					final int[] arraySlotNext = new int[] {26, 35};
					final int[] arraySlotPrevious = new int[] {18, 27};
					final int[] arraySlotComponent = new int[] {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
					
					final HashMap<Integer, MenuSlot> mapSlot = new HashMap<Integer, MenuSlot>();
					final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
					
					page = MathUtil.limitInteger(page, 1, maxPage);
					
					String textTitle = Language.MENU_PAGE_TITLE_SHOP_BAIT_SELL.getText(player);
					
					List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
					List<String> lorePageNext = Language.MENU_ITEM_LORES_PAGE_NEXT.getListText(player);
					List<String> lorePagePrevious = Language.MENU_ITEM_LORES_PAGE_PREVIOUS.getListText(player);
					
					mapPlaceholder.put("page", String.valueOf(page));
					mapPlaceholder.put("max_page", String.valueOf(maxPage));
					
					textTitle = TextUtil.placeholder(mapPlaceholder, textTitle);
					
					final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
					final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
					final ItemStack itemPageNext = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPageNext, 1, lorePageNext);
					final ItemStack itemPagePrevious = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPagePrevious, 1, lorePagePrevious);
					
					for (int slot : arraySlotPane) {
						final MenuSlot menuSlot = new MenuSlot(slot);
						
						menuSlot.setItem(itemPaneBlack);
						mapSlot.put(slot, menuSlot);
					}
					
					if (enableActionBack) {
						for (int slot : arraySlotBack) {
							final MenuSlot menuSlot = new MenuSlot(slot);
							
							menuSlot.setItem(itemPageBack);
							menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop");
							mapSlot.put(slot, menuSlot);
						}
					}
					
					if (page < maxPage) {
						for (int slot : arraySlotNext) {
							final MenuSlot menuSlot = new MenuSlot(slot);
							
							menuSlot.setItem(itemPageNext);
							menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop_Bait_Sell " + (page+1));
							mapSlot.put(slot, menuSlot);
						} 
					}
					
					if (page > 1) {
						for (int slot : arraySlotPrevious) {
							final MenuSlot menuSlot = new MenuSlot(slot);
							
							menuSlot.setItem(itemPagePrevious);
							menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop_Bait_Sell " + (page-1));
							mapSlot.put(slot, menuSlot);
						} 
					}
					
					for (int index = 0; index < arraySlotComponent.length; index++) {
						final int slot = arraySlotComponent[index];
						final int indexRegister = index + (slots*(page-1));
						
						if (indexRegister < size) {
							final MenuSlot menuSlot = new MenuSlot(slot);
							final BaitProperties baitProperties = allowedBait.get(indexRegister);
							final String bait = baitProperties.getId();
							final ItemStack item = baitProperties.getItem().clone();
							final double price = baitProperties.getPrice();
							final List<String> lores = EquipmentUtil.getLores(item);
							
							List<String> loreComponent = Language.MENU_ITEM_FORMAT_BAIT_SELL_COMPONENT.getListText(player);
							
							mapPlaceholder.clear();
							mapPlaceholder.put("bait", bait);
							mapPlaceholder.put("price", String.valueOf(price));
							mapPlaceholder.put("symbol_currency", currency);
							
							loreComponent = TextUtil.placeholder(mapPlaceholder, loreComponent);
							
							lores.addAll(loreComponent);
							EquipmentUtil.setLores(item, lores);
							
							menuSlot.setItem(item);
							menuSlot.setActionArguments(ActionType.LEFT_CLICK, "DreamFish Action Bait Sell " + bait + " 1");
							menuSlot.setActionArguments(ActionType.RIGHT_CLICK, "DreamFish Action Bait Sell " + bait + " 64");
							mapSlot.put(slot, menuSlot);
						} else {
							break;
						}
					}
					
					final Text title = new TextLine(textTitle);
					final MenuGUI menuGUI = new MenuGUI(player, id, row, title, executor, false, mapSlot);
					
					Menu.openMenu(player, menuGUI);
					return;
				}
			}
		}
	}
	
	public final void openMenuFishSell(Player player) {
		openMenuFishSell(player, 1);
	}
	
	public final void openMenuFishSell(Player player, int page) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final FishManager fishManager = gameManager.getFishManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		
		if (player != null) {
			final boolean enablePageSellFish = mainConfig.isMenuEnablePageSellFish();
			final boolean enableActionBack = mainConfig.isMenuEnableActionBack();
			
			if (enablePageSellFish) {
				final String permission = mainConfig.getMenuPermissionSellFish();
				
				if (!SenderUtil.hasPermission(player, permission)) {
					final MessageBuild message = Language.PERMISSION_LACK.getMessage(player);
					
					message.sendMessage(player, "permission", permission);
					SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final String id = "DreamFish Shop_Fish_Sell";
					final String currency = mainConfig.getUtilityCurrency();
					final Collection<FishProperties> allFishProperties = fishManager.getAllFishProperties();
					final List<FishProperties> allowedFish = new ArrayList<FishProperties>();
					final HashMap<String, List<FishProperties>> mapListFish = new HashMap<String, List<FishProperties>>();
					
					for (FishProperties fishProperties : allFishProperties) {
						final ItemStack item = fishProperties.getItem();
						final String serialize = "Type: " + item.getType() + ", Data: "+ EquipmentUtil.getData(item);
						
						if (mapListFish.containsKey(serialize)) {
							mapListFish.get(serialize).add(fishProperties);
						} else {
							final List<FishProperties> listFish = new ArrayList<FishProperties>();
							
							listFish.add(fishProperties);
							mapListFish.put(serialize, listFish);
						}
					}
					
					for (List<FishProperties> listFish : mapListFish.values()) {
						for (FishProperties fishProperties : listFish) {
							allowedFish.add(fishProperties);
						}
					}
					
					final int row = 6;
					final int size = allowedFish.size();
					final int slots = 28;
					final int maxPage = Math.max(1, size % slots > 0 ? (size/slots) + 1 : size/slots);
					final String prefix = placeholderManager.getPlaceholder("prefix");
					final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
					final String headerPageNext = Language.MENU_ITEM_HEADER_PAGE_NEXT.getText(player);
					final String headerPagePrevious = Language.MENU_ITEM_HEADER_PAGE_PREVIOUS.getText(player);
					final int[] arraySlotPane = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};
					final int[] arraySlotBack = new int[] {48, 49, 50};
					final int[] arraySlotNext = new int[] {26, 35};
					final int[] arraySlotPrevious = new int[] {18, 27};
					final int[] arraySlotComponent = new int[] {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
					final HashMap<Integer, MenuSlot> mapSlot = new HashMap<Integer, MenuSlot>();
					final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
					
					page = MathUtil.limitInteger(page, 1, maxPage);
					
					String textTitle = Language.MENU_PAGE_TITLE_SHOP_FISH_SELL.getText(player);
					
					List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
					List<String> lorePageNext = Language.MENU_ITEM_LORES_PAGE_NEXT.getListText(player);
					List<String> lorePagePrevious = Language.MENU_ITEM_LORES_PAGE_PREVIOUS.getListText(player);
					
					mapPlaceholder.put("page", String.valueOf(page));
					mapPlaceholder.put("max_page", String.valueOf(maxPage));
					
					textTitle = TextUtil.placeholder(mapPlaceholder, textTitle);
					
					final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
					final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
					final ItemStack itemPageNext = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPageNext, 1, lorePageNext);
					final ItemStack itemPagePrevious = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPagePrevious, 1, lorePagePrevious);
					
					for (int slot : arraySlotPane) {
						final MenuSlot menuSlot = new MenuSlot(slot);
						
						menuSlot.setItem(itemPaneBlack);
						mapSlot.put(slot, menuSlot);
					}
					
					if (enableActionBack) {
						for (int slot : arraySlotBack) {
							final MenuSlot menuSlot = new MenuSlot(slot);
							
							menuSlot.setItem(itemPageBack);
							menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop");
							mapSlot.put(slot, menuSlot);
						} 
					}
					
					if (page < maxPage) {
						for (int slot : arraySlotNext) {
							final MenuSlot menuSlot = new MenuSlot(slot);
							
							menuSlot.setItem(itemPageNext);
							menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop_Fish_Sell " + (page+1));
							mapSlot.put(slot, menuSlot);
						} 
					}
					
					if (page > 1) {
						for (int slot : arraySlotPrevious) {
							final MenuSlot menuSlot = new MenuSlot(slot);
							
							menuSlot.setItem(itemPagePrevious);
							menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Shop_Fish_Sell " + (page-1));
							mapSlot.put(slot, menuSlot);
						} 
					}
					
					for (int index = 0; index < arraySlotComponent.length; index++) {
						final int slot = arraySlotComponent[index];
						final int indexRegister = index + (slots*(page-1));
						
						if (indexRegister < size) {
							final MenuSlot menuSlot = new MenuSlot(slot);
							final FishProperties fishProperties = allowedFish.get(indexRegister);
							final String fish = fishProperties.getId();
							final ItemStack item = fishProperties.getItem().clone();
							final double price = fishProperties.getPrice();
							final List<String> lores = EquipmentUtil.getLores(item);
							
							List<String> loreComponent = Language.MENU_ITEM_FORMAT_FISH_SELL_COMPONENT.getListText(player);
							
							mapPlaceholder.clear();
							mapPlaceholder.put("fish", fish);
							mapPlaceholder.put("price", String.valueOf(price));
							mapPlaceholder.put("symbol_currency", currency);
							
							loreComponent = TextUtil.placeholder(mapPlaceholder, loreComponent);
							
							lores.addAll(loreComponent);
							EquipmentUtil.setLores(item, lores);;
							
							menuSlot.setItem(item);
							menuSlot.setActionArguments(ActionType.LEFT_CLICK, "DreamFish Action Fish Sell " + fish + " 1");
							menuSlot.setActionArguments(ActionType.RIGHT_CLICK, "DreamFish Action Fish Sell " + fish + " 64");
							mapSlot.put(slot, menuSlot);
						} else {
							break;
						}
					}
					
					final Text title = new TextLine(textTitle);
					final MenuGUI menuGUI = new MenuGUI(player, id, row, title, executor, false, mapSlot);
					
					Menu.openMenu(player, menuGUI);
					return;
				}
			}
		}
	}
	
	public final void openMenuGuide(Player player) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final DreamFishConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		
		if (player != null) {
			final String permission = mainConfig.getMenuPermissionGuide();
			final boolean enableActionBack = mainConfig.isMenuEnableActionBack();
			
			if (!SenderUtil.hasPermission(player, permission)) {
				final MessageBuild message = Language.PERMISSION_LACK.getMessage(player);
				
				message.sendMessage(player, "permission", permission);
				SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final String id = "DreamFish Guide";
				final String textTitle = Language.MENU_PAGE_TITLE_GUIDE.getText(player);
				
				final int row = 4;
				final Text title = new TextLine(textTitle);
				final String prefix = placeholderManager.getPlaceholder("prefix");
				final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
				final String headerGuideBait = Language.MENU_ITEM_HEADER_GUIDE_BAIT.getText(player);
				final String headerGuideFish = Language.MENU_ITEM_HEADER_GUIDE_FISH.getText(player);
				final String headerGuideFishPower = Language.MENU_ITEM_HEADER_GUIDE_FISH_POWER.getText(player);
				final String headerGuideFishingMode = Language.MENU_ITEM_HEADER_GUIDE_FISHING_MODE.getText(player);
				final String headerGuideTension = Language.MENU_ITEM_HEADER_GUIDE_TENSION.getText(player);
				final String headerGuideDistance = Language.MENU_ITEM_HEADER_GUIDE_DISTANCE.getText(player);
				final String headerGuideBiome = Language.MENU_ITEM_HEADER_GUIDE_BIOME.getText(player);
				final int[] arraySlotPaneBlack = new int[] {1, 2, 3, 4, 5, 6, 7, 10, 16, 19, 21, 22, 23, 25, 28, 29, 30, 31, 32, 33, 34};
				final int[] arraySlotPaneWhite = new int[] {0, 8, 9, 17, 18, 26, 27, 35};
				final MenuSlot menuSlotPageBack = new MenuSlot(31);
				final MenuSlot menuSlotGuideBait = new MenuSlot(11);
				final MenuSlot menuSlotGuideFish = new MenuSlot(12);
				final MenuSlot menuSlotGuideFishPower = new MenuSlot(13);
				final MenuSlot menuSlotGuideFishingMode = new MenuSlot(14);
				final MenuSlot menuSlotGuideTension = new MenuSlot(15);
				final MenuSlot menuSlotGuideDistance = new MenuSlot(20);
				final MenuSlot menuSlotGuideBiome = new MenuSlot(24);
				final HashMap<Integer, MenuSlot> mapSlot = new HashMap<Integer, MenuSlot>();
				
				List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
				List<String> loreGuideBait = Language.MENU_ITEM_LORES_GUIDE_BAIT.getListText(player);
				List<String> loreGuideFish = Language.MENU_ITEM_LORES_GUIDE_FISH.getListText(player);
				List<String> loreGuideFishPower = Language.MENU_ITEM_LORES_GUIDE_FISH_POWER.getListText(player);
				List<String> loreGuideFishingMode = Language.MENU_ITEM_LORES_GUIDE_FISHING_MODE.getListText(player);
				List<String> loreGuideTension = Language.MENU_ITEM_LORES_GUIDE_TENSION.getListText(player);
				List<String> loreGuideDistance = Language.MENU_ITEM_LORES_GUIDE_DISTANCE.getListText(player);
				List<String> loreGuideBiome = Language.MENU_ITEM_LORES_GUIDE_BIOME.getListText(player);
				
				final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
				final ItemStack itemPaneWhite = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, prefix, 1);
				final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPageBack, 1, lorePageBack);
				final ItemStack itemGuideBait = EquipmentUtil.createItem(MaterialEnum.BOOK, headerGuideBait, 1, loreGuideBait);
				final ItemStack itemGuideFish = EquipmentUtil.createItem(MaterialEnum.BOOK, headerGuideFish, 1, loreGuideFish);
				final ItemStack itemGuideFishPower = EquipmentUtil.createItem(MaterialEnum.BOOK, headerGuideFishPower, 1, loreGuideFishPower);
				final ItemStack itemGuideFishingMode = EquipmentUtil.createItem(MaterialEnum.BOOK, headerGuideFishingMode, 1, loreGuideFishingMode);
				final ItemStack itemGuideTension = EquipmentUtil.createItem(MaterialEnum.BOOK, headerGuideTension, 1, loreGuideTension);
				final ItemStack itemGuideDistance = EquipmentUtil.createItem(MaterialEnum.BOOK, headerGuideDistance, 1, loreGuideDistance);
				final ItemStack itemGuideBiome = EquipmentUtil.createItem(MaterialEnum.BOOK, headerGuideBiome, 1, loreGuideBiome);
				
				menuSlotGuideBait.setItem(itemGuideBait);
				menuSlotGuideFish.setItem(itemGuideFish);
				menuSlotGuideFishPower.setItem(itemGuideFishPower);
				menuSlotGuideFishingMode.setItem(itemGuideFishingMode);
				menuSlotGuideTension.setItem(itemGuideTension);
				menuSlotGuideDistance.setItem(itemGuideDistance);
				menuSlotGuideBiome.setItem(itemGuideBiome);
				
				mapSlot.put(menuSlotGuideBait.getSlot(), menuSlotGuideBait);
				mapSlot.put(menuSlotGuideFish.getSlot(), menuSlotGuideFish);
				mapSlot.put(menuSlotGuideFishPower.getSlot(), menuSlotGuideFishPower);
				mapSlot.put(menuSlotGuideFishingMode.getSlot(), menuSlotGuideFishingMode);
				mapSlot.put(menuSlotGuideTension.getSlot(), menuSlotGuideTension);
				mapSlot.put(menuSlotGuideDistance.getSlot(), menuSlotGuideDistance);
				mapSlot.put(menuSlotGuideBiome.getSlot(), menuSlotGuideBiome);
				
				for (int slot : arraySlotPaneBlack) {
					final MenuSlot menuSlot = new MenuSlot(slot);
					
					menuSlot.setItem(itemPaneBlack);
					mapSlot.put(slot, menuSlot);
				}
				
				for (int slot : arraySlotPaneWhite) {
					final MenuSlot menuSlot = new MenuSlot(slot);
					
					menuSlot.setItem(itemPaneWhite);
					mapSlot.put(slot, menuSlot);
				}
				
				if (enableActionBack) {
					menuSlotPageBack.setItem(itemPageBack);
					menuSlotPageBack.setActionArguments(ActionCategory.ALL_CLICK, "DreamFish Menu Home");
					mapSlot.put(menuSlotPageBack.getSlot(), menuSlotPageBack);
				}
				
				final MenuGUI menuGUI = new MenuGUI(null, id, row, title, executor, false, mapSlot);
				
				Menu.openMenu(player, menuGUI);
				return;
			}
		}
	}
}
