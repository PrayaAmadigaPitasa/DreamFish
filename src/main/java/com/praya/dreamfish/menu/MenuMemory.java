package com.praya.dreamfish.menu;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.game.MenuManager;
import com.praya.dreamfish.menu.MenuDreamFish;
import core.praya.agarthalib.builder.menu.MenuExecutor;

public final class MenuMemory extends MenuManager {

	private final MenuDreamFish menuDreamFish;
	
	private MenuMemory(DreamFish plugin) {
		super(plugin);
		
		final MenuExecutor menuExecutorDreamFish = new MenuDreamFishExecutor(plugin);
		
		this.menuDreamFish = new MenuDreamFish(plugin, menuExecutorDreamFish);
	}
	
	private static class MenuMemorySingleton {
		private static final MenuMemory instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			instance = new MenuMemory(plugin);
		}
	}
	
	public static final MenuMemory getInstance() {
		return MenuMemorySingleton.instance;
	}
	
	@Override
	public final MenuDreamFish getMenuDreamFish() {
		return this.menuDreamFish;
	}
}