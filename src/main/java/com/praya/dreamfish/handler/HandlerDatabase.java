package com.praya.dreamfish.handler;

import java.util.ArrayList;
import java.util.Collection;

import com.praya.agarthalib.database.Database;
import com.praya.dreamfish.DreamFish;

public abstract class HandlerDatabase extends Handler {
	
	protected HandlerDatabase(DreamFish plugin) {
		super(plugin);
	}
	
	protected abstract Database getDatabase();
	
	public final void closeConnection() {
		final Database database = getDatabase();
		
		database.close();
	}
	
	public static final void closeAllConnection() {
		for (HandlerDatabase handlerDatabase : getAllHandlerDatabase()) {
			handlerDatabase.closeConnection();
		}
	}
	
	public static Collection<HandlerDatabase> getAllHandlerDatabase() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerDatabase> allHandlerDatabase = new ArrayList<HandlerDatabase>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerDatabase) {
				final HandlerDatabase handlerDatabase = (HandlerDatabase) handler;
				
				allHandlerDatabase.add(handlerDatabase);
			}
		}
		
		return allHandlerDatabase;
	}
}
