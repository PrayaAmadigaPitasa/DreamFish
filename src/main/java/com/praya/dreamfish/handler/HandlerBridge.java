package com.praya.dreamfish.handler;

import java.util.ArrayList;
import java.util.Collection;

import com.praya.dreamfish.DreamFish;

public class HandlerBridge extends Handler {
	
	protected HandlerBridge(DreamFish plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerBridge> getAllHandlerBridge() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerBridge> allHandlerBridge = new ArrayList<HandlerBridge>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerBridge) {
				final HandlerBridge handlerBridge = (HandlerBridge) handler;
				
				allHandlerBridge.add(handlerBridge);
			}
		}
		
		return allHandlerBridge;
	}
}
