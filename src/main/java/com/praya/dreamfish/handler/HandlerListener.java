package com.praya.dreamfish.handler;

import java.util.ArrayList;
import java.util.Collection;

import com.praya.dreamfish.DreamFish;

public abstract class HandlerListener extends Handler {
	
	protected HandlerListener(DreamFish plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerListener> getAllHandlerListener() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerListener> allHandlerListener = new ArrayList<HandlerListener>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerListener) {
				final HandlerListener handlerListener = (HandlerListener) handler;
				
				allHandlerListener.add(handlerListener);
			}
		}
		
		return allHandlerListener;
	}
}