package com.praya.dreamfish.handler;

import java.util.Collection;
import java.util.HashMap;

import com.praya.dreamfish.DreamFish;

public abstract class Handler {

	private static final HashMap<Class<?>, Handler> mapHandler = new HashMap<Class<?>, Handler>();
	
	protected final DreamFish plugin;
	
	protected Handler(DreamFish plugin) {
		if (plugin != null) {
			final Class<?> clazz = this.getClass();
			
			mapHandler.put(clazz, this);
		}
		
		this.plugin = plugin;
	}
	
	public static final Collection<Handler> getAllHandler() {
		return mapHandler.values();
	}
	
	public static final Handler getProvidingHandler(Class<?> clazz) {
		return clazz != null ? mapHandler.get(clazz) : null;
	}
	
	public static final <T extends Handler> T getHandler(Class<T> clazz) {
		if (clazz != null) {
			if (Handler.class.isAssignableFrom(clazz)) {
				final Handler handler = mapHandler.get(clazz);
				
				if (handler != null) {
					return clazz.cast(handler);
				}
			}
		}
		
		return null;
	}
}
