package com.praya.dreamfish.handler;

import java.util.ArrayList;
import java.util.Collection;

import com.praya.dreamfish.DreamFish;

public abstract class HandlerConfig extends Handler {
	
	protected HandlerConfig(DreamFish plugin) {
		super(plugin);
	}
	
	public abstract void setup();
	
	public static Collection<HandlerConfig> getAllHandlerConfig() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerConfig> allHandlerConfig = new ArrayList<HandlerConfig>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerConfig) {
				final HandlerConfig handlerConfig = (HandlerConfig) handler;
				
				allHandlerConfig.add(handlerConfig);
			}
		}
		
		return allHandlerConfig;
	}
}