package com.praya.dreamfish.handler;

import java.util.ArrayList;
import java.util.Collection;

import com.praya.dreamfish.DreamFish;

import core.praya.agarthalib.builder.menu.MenuExecutor;

public abstract class HandlerMenu extends Handler {
	
	private final MenuExecutor menuExecutor;
	
	protected HandlerMenu(DreamFish plugin, MenuExecutor menuExecutor) {
		super(plugin);
		
		this.menuExecutor = menuExecutor;
	}
	
	public final MenuExecutor getMenuExecutor() {
		return this.menuExecutor;
	}
	
	public static Collection<HandlerMenu> getAllHandlerMenu() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerMenu> allHandlerMenu = new ArrayList<HandlerMenu>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerMenu) {
				final HandlerMenu handlerMenu = (HandlerMenu) handler;
				
				allHandlerMenu.add(handlerMenu);
			}
		}
		
		return allHandlerMenu;
	}
}
