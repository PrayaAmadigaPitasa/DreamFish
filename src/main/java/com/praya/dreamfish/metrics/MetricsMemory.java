package com.praya.dreamfish.metrics;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.plugin.MetricsManager;
import com.praya.dreamfish.metrics.service.BStats;

public final class MetricsMemory extends MetricsManager {

	private BStats metricsBStats;
	
	private MetricsMemory(DreamFish plugin) {
		super(plugin);
		
		this.metricsBStats = new BStats(plugin);
	}
	
	private static class MetricsMemorySingleton {
		private static final MetricsMemory instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			instance = new MetricsMemory(plugin);
		}
	}
	
	public static final MetricsMemory getInstance() {
		return MetricsMemorySingleton.instance;
	}
	
	@Override
	public final BStats getMetricsBStats() {
		return this.metricsBStats;
	}
}
