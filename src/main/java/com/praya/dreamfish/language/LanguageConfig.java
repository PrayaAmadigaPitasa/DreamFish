package com.praya.dreamfish.language;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import org.bukkit.configuration.file.FileConfiguration;

import com.praya.agarthalib.utility.FileUtil;
import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.handler.HandlerConfig;
import com.praya.dreamfish.manager.plugin.PlaceholderManager;
import com.praya.dreamfish.placeholder.PlaceholderMemory;
import core.praya.agarthalib.builder.main.LanguageBuild;
import core.praya.agarthalib.builder.message.MessageBuild;

public final class LanguageConfig extends HandlerConfig {
	
	private static final String PATH_FOLDER = "Language";
	private static final List<String> LIST_PATH_FILES = new ArrayList<String>();
	
	protected final HashMap<String, LanguageBuild> mapLanguageBuild = new HashMap<String, LanguageBuild>();
		
	protected LanguageConfig(DreamFish plugin) {
		super(plugin);
		
		setup();
	}
	
	static {
		LIST_PATH_FILES.add("Language/lang_en.yml");
		LIST_PATH_FILES.add("Language/lang_de.yml");
		LIST_PATH_FILES.add("Language/lang_id.yml");
		LIST_PATH_FILES.add("Language/lang_ru.yml");
	}
	
	
	public final void setup() {
		moveOldFile();
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapLanguageBuild.clear();
	}
	
	private final void loadConfig() {
		final File folder = FileUtil.getFile(plugin, PATH_FOLDER);
		
		for (String pathFile : LIST_PATH_FILES) {
			final File file = FileUtil.getFile(plugin, pathFile);
			final String name = file.getName().toLowerCase();
			final String id = name.split(Pattern.quote("."))[0];
			final String locale = (id.startsWith("lang_") ? id.replaceFirst("lang_", "") : "en").toLowerCase();
			
			if (!file.exists()) {
				FileUtil.saveResource(plugin, pathFile);
			}
			
			final FileConfiguration config = FileUtil.getFileConfigurationResource(plugin, pathFile);
			final LanguageBuild language = loadLanguage(locale, config);
			
			this.mapLanguageBuild.put(locale, language);
		}
		
		for (File file : folder.listFiles()) {
			final String name = file.getName().toLowerCase();
			final String id = name.split(Pattern.quote("."))[0];
			final String locale = (id.startsWith("lang_") ? id.replaceFirst("lang_", "") : "en").toLowerCase();
			final FileConfiguration config = FileUtil.getFileConfiguration(file);
			final LanguageBuild language = loadLanguage(locale, config);
			final LanguageBuild localeLang = this.mapLanguageBuild.get(locale);
			
			if (localeLang != null) {
				localeLang.mergeLanguage(language);
			} else {
				this.mapLanguageBuild.put(locale, language);
			}
		}
	}
	
	private final LanguageBuild loadLanguage(String locale, FileConfiguration config) {
		final PlaceholderManager placeholderManager = PlaceholderMemory.getInstance();
		final HashMap<String, MessageBuild> mapLanguage = new HashMap<String, MessageBuild>();
		
		for (String path : config.getKeys(true)) {
			final String key = path.replace(".", "_").toUpperCase();
			
			if (config.isString(path)) {
				final String text = config.getString(path);
				final List<String> list = new ArrayList<String>();
				
				list.add(text);
				
				final List<String> listPlaceholder = placeholderManager.localPlaceholder(list);
				final MessageBuild messages = new MessageBuild(listPlaceholder);
				
				mapLanguage.put(key, messages);
			} else if (config.isList(path)) {
				final List<String> list = config.getStringList(path);
				final List<String> listPlaceholder = placeholderManager.localPlaceholder(list);
				final MessageBuild messages = new MessageBuild(listPlaceholder);
				
				mapLanguage.put(key, messages);
			}
		}
		
		return new LanguageBuild(locale, mapLanguage);
	}
	
	private final void moveOldFile() {
		final String pathOld = "Language/lang.yml";
		final String pathNew = "Language/lang_en.yml";
		final File fileOld = FileUtil.getFile(plugin, pathOld);
		final File fileNew = FileUtil.getFile(plugin, pathNew);
		
		if (fileOld.exists()) {
			FileUtil.moveFileSilent(fileOld, fileNew);
		}
	}
}