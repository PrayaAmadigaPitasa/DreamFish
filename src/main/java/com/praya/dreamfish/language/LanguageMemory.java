package com.praya.dreamfish.language;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.language.LanguageConfig;
import com.praya.dreamfish.manager.plugin.LanguageManager;

import core.praya.agarthalib.builder.main.LanguageBuild;
import core.praya.agarthalib.builder.message.MessageBuild;

public class LanguageMemory extends LanguageManager {

	private final LanguageConfig languageConfig;
	
	private LanguageMemory(DreamFish plugin) {
		super(plugin);
		
		this.languageConfig = new LanguageConfig(plugin);
	};
	
	private static class LanguageMemorySingleton {
		private static final LanguageMemory instance;
		
		static {
			final DreamFish plugin = JavaPlugin.getPlugin(DreamFish.class);
			
			instance = new LanguageMemory(plugin);
		}
	}
	
	public static final LanguageMemory getInstance() {
		return LanguageMemorySingleton.instance;
	}
	
	public final LanguageConfig getLanguageConfig() {
		return this.languageConfig;
	}
	
	@Override
	public final Collection<String> getLanguageIds() {
		return getLanguageConfig().mapLanguageBuild.keySet();
	}
	
	protected final Collection<String> getLanguageIds(boolean clone) {
		final Collection<String> languageIds = getLanguageConfig().mapLanguageBuild.keySet();
		
		return clone ? new ArrayList<String>(languageIds) : languageIds;
	}
	
	@Override
	public final Collection<LanguageBuild> getAllLanguageBuild() {
		return getLanguageConfig().mapLanguageBuild.values();
	}
	
	protected final Collection<LanguageBuild> getAllLanguageBuild(boolean clone) {
		final Collection<LanguageBuild> allLanguageBuild = getLanguageConfig().mapLanguageBuild.values();
		
		return clone ? new ArrayList<LanguageBuild>(allLanguageBuild) : allLanguageBuild;
	}
	
	@Override
	public final LanguageBuild getLocaleLanguage(String id) {
		if (id != null) {
			for (String key : getLanguageIds(false)) {
				if (key.equalsIgnoreCase(id)) {
					return getLanguageConfig().mapLanguageBuild.get(key);
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final MessageBuild getLocaleMessage(String id, String key) {
		if (id != null && key != null) {
			final LanguageBuild languageBuild = getLocaleLanguage(id);
			
			if (languageBuild != null) {
				return languageBuild.getMessage(key);
			}
		}
		
		return new MessageBuild();
	}
}