package com.praya.dreamfish;

import com.praya.dreamfish.bait.BaitMemory;
import com.praya.dreamfish.command.CommandTreeMemory;
import com.praya.dreamfish.event.EventMemory;
import com.praya.dreamfish.fish.FishMemory;
import com.praya.dreamfish.manager.game.*;
import com.praya.dreamfish.menu.MenuMemory;
import com.praya.dreamfish.tabcompleter.TabCompleterTreeMemory;
import com.praya.dreamfish.treasure.TreasureMemory;

public final class DreamFishGameMemory extends GameManager {

	private final BaitManager baitManager;
	private final FishManager fishManager;
	private final EventManager eventManager;
	private final MenuManager menuManager;
	private final CommandTreeManager commandTreeManager;
	private final TabCompleterTreeManager tabCompleterTreeManager;
	private final TreasureManager treasureManager;
	
	protected DreamFishGameMemory(DreamFish plugin) {
		super(plugin);
		
		this.baitManager = BaitMemory.getInstance();
		this.fishManager = FishMemory.getInstance();
		this.eventManager = EventMemory.getInstance();
		this.menuManager = MenuMemory.getInstance();
		this.commandTreeManager = CommandTreeMemory.getInstance();
		this.tabCompleterTreeManager = TabCompleterTreeMemory.getInstance();
		this.treasureManager = TreasureMemory.getInstance();
	}
	
	public final BaitManager getBaitManager() {
		return this.baitManager;
	}
	
	public final FishManager getFishManager() {
		return this.fishManager;
	}
	
	public final EventManager getEventManager() {
		return this.eventManager;
	}
	
	public final MenuManager getMenuManager() {
		return this.menuManager;
	}
	
	public final CommandTreeManager getCommandTreeManager() {
		return this.commandTreeManager;
	}
	
	public final TabCompleterTreeManager getTabCompleterTreeManager() {
		return this.tabCompleterTreeManager;
	}

	public final TreasureManager getTreasureManager() {
		return this.treasureManager;
	}
}
