package com.praya.dreamfish;

import com.praya.dreamfish.DreamFish;
import com.praya.dreamfish.manager.player.PlayerBaitManager;
import com.praya.dreamfish.manager.player.PlayerFishingManager;
import com.praya.dreamfish.manager.player.PlayerFishingModeManager;
import com.praya.dreamfish.manager.player.PlayerManager;
import com.praya.dreamfish.player.PlayerBaitMemory;
import com.praya.dreamfish.player.PlayerFishingMemory;
import com.praya.dreamfish.player.PlayerFishingModeMemory;

public final class DreamFishPlayerMemory extends PlayerManager {

	private final PlayerBaitManager playerBaitManager;
	private final PlayerFishingManager playerFishingManager;
	private final PlayerFishingModeManager playerFishingModeManager;
	
	protected DreamFishPlayerMemory(DreamFish plugin) {
		super(plugin);
		
		this.playerBaitManager = PlayerBaitMemory.getInstance();
		this.playerFishingManager = PlayerFishingMemory.getInstance();
		this.playerFishingModeManager = PlayerFishingModeMemory.getInstance();
	};
	
	public final PlayerBaitManager getPlayerBaitManager() {
		return this.playerBaitManager;
	}
	
	public final PlayerFishingManager getPlayerFishingManager() {
		return this.playerFishingManager;
	}
	
	public final PlayerFishingModeManager getPlayerFishingModeManager() {
		return this.playerFishingModeManager;
	}
}
